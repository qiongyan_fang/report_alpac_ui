/**
 * External plugins added through the server-side FieldFactory are automatically registered.
 * Other external plugins (e.g. client-only) may still be registered here (and subsequently added via config.extraPlugins).
 *
 * e.g. if your plugin resides in src/main/resources/VAADIN/js:
 * CKEDITOR.plugins.addExternal("abbr", CKEDITOR.vaadinDirUrl + "js/abbr/");
 */




CKEDITOR.plugins.addExternal("magnolialink", CKEDITOR.vaadinDirUrl + "js/magnolialink/");
//CKEDITOR.plugins.addExternal("magnoliaFileBrowser", CKEDITOR.vaadinDirUrl + "js/magnoliaFileBrowser/");

/* this is absolute path, so check magnolia root path */
CKEDITOR.plugins.addExternal("eqneditor", "/.resources/Alpac-report/ckeditor/eqneditor/"); // plugin to create formula
/*
CKEDITOR.plugins.addExternal("clipboard","/docroot/js/ckeditor/clipboard/");
CKEDITOR.plugins.addExternal("lineutils","/docroot/js/ckeditor/lineutils/");
CKEDITOR.plugins.addExternal("widget","/docroot/js/ckeditor/widget/");
CKEDITOR.plugins.addExternal("mathjax","/docroot/js/ckeditor/mathjax/");*/

/*
 * 	config.mathJaxLib = '//cdn.mathjax.org/mathjax/2.6-latest/MathJax.js?config=TeX-AMS_HTML';
	config.mathJaxClass = 'my-math';

 */
//CKEDITOR.plugins.addExternal("wsc", "/docroot/js/ckeditor/wsc/");
CKEDITOR.editorConfig = function( config ) {
//	config.disableNativeSpellChecker = false;
	// MIRROR info.magnolia.ui.form.field.definition.RichTextFieldDefinition
	definition = {
		alignment: true,
		images: true,
		lists: true,
		source: true,
		tables: true,

		colors: 'ffffff,4CAF50,9e9e9e,C94412,7d7d7d,008542',
		fonts: null,
		fontSizes: 'small,large',
	}

	// MIRROR info.magnolia.ui.form.field.factory.RichTextFieldFactory
	removePlugins = [];

	// CONFIGURATION FROM DEFINITION
	
	
	if (definition.fonts == null && definition.fontSizes == null) {
		removePlugins.push("font");
		removePlugins.push("fontSize");
	}

	// DEFAULT CONFIGURATION FROM FIELD FACTORY

	config.baseFloatZIndex = 10000;
	config.resize_enabled = false;
	config.toolbar = "Magnolia";
	// mathjax starts since 4.4
	
	
	config.toolbar_Magnolia = [
		{ name: "basicstyles",   items: [ "Bold", "Italic", "Superscript"] },	

		{ name: "styles",        items: ['Format'] },
	
	
	];
	
};
/* version 6.1, the option lists won't show due to smaller z-index comparing to the dialog windows,
so manually set it to be larger than the dialog windows */
