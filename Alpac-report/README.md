# Navigation - Dynamic and managed options for your main navigation in Magnolia CMS.

> Light design and CSS ready for your custom theme the navigation light module gives you the option to use dynamic navigation based on page structure or completely managed link and dropdown items.

## Features
> Dynamic Navigation: Displays all children pages and subpages under the homepage. One component all automatic.
> Managed Navigation: Give authors full control over menu order and content. Each nav item and menu is completely managed with fallbacks from the page content.
> Both navigation's use very light styling allowing you to make your own custom theme.


![Navigation](https://bytebucket.org/trillitech/Alpac-report/raw/6109134bcd5b3ef7153162a8777f5318f45eb5d8/_dev/screens/nav.png)

### Dynamic Navigation
> Dynamic navigation builds the navigation based on the page structure of the site starting from the homepage and displays the direct children of each page.
![Dynamic Navigation](https://bytebucket.org/trillitech/Alpac-report/raw/6109134bcd5b3ef7153162a8777f5318f45eb5d8/_dev/screens/dynamic-nav.png)

### Managed Navigation
> Managed navigation creates a navigation area and allows content admins to add "navLinks". Each navLink has an optional title that can  be used to override the destinations page title and a list of submenu links that may be added.
![Managed Navigation](https://bytebucket.org/trillitech/Alpac-report/raw/6109134bcd5b3ef7153162a8777f5318f45eb5d8/_dev/screens/managed-nav.png)
![Managed Navigation Dialog](https://bytebucket.org/trillitech/Alpac-report/raw/179d2c1c419a585b6bfd21166ad240d78d4a208f/_dev/screens/nav-link-dialog.png)


## Usage
> Assuming you have a navigation area available on your homepage template you can copy and paste below and remove the component you do not want to use.
~~~~
areas:
    navigation:
        inheritance:
            enabled: true
            components: all
        availableComponents:
            navigationManaged:
                id: Alpac-report:components/navigation-managed
            navigationDynamic:
                id: Alpac-report:components/navigation-dynamic
~~~~

## Demo

This module ships with a decoration adding the navigation components to the MTK basic template. You will need to add the css files within the webresources folder to see this.

Alternatively you can remove .example from default.yaml.example and default.ftl.example within the templates/pages/ folder and import one of the xml files within _dev/demos/import to the root of the pages app.


## Information on Magnolia CMS
This directory is a Magnolia 'light module'.

https://docs.magnolia-cms.com

Search the docs for `sharing light modules` for details on how to share and use light modules on npm and github.


## Contribute to the Magnolia component ecosystem
It's easy to create components for Magnolia and share them on github and npm. I invite you to do so and join the community. Let's stop wasting time by developing the same thing again and again, rather let's help each other out by sharing our work and create a rich library of components.

Just add `magnolia-light-module` as a keyword to npm's package.json to make them easy to find and use on npm.

## License

MIT

## Contributors
http://crescendocollective.com/
