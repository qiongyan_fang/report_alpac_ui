<!DOCTYPE html>
<html xml:lang="${cmsfn.language()}" lang="${cmsfn.language()}">
  [@cms.page /]    
  [#include "/Alpac-report/templates/content-includes/html-head.ftl"]
  [#include "/Alpac-report/templates/content-includes/header-menu.ftl"]
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.css" rel="stylesheet" />
  <link href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css" rel="stylesheet" />
  
  <script src="${ctx.contextPath}/.resources/Alpac-report/webresources/js/org-main.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.js"></script> 
  <body class="default ${cmsfn.language()}">
  
  [#include "/Alpac-report/webresources/img/sprite.svg"]
  <div class="wrapper">
    <div class="scrollMore">	
      <span class="scroll-btn">
        <a href="#">
          <span class="mouse">
            <span>
            </span>
          </span>
        </a>
      </span>
      <p>Scroll for more</p>
    </div>
    <main class="content">
      [@cms.area name="banner" /]
      <div class="center" id="common-task"></div>
      [@cms.area name="main" /]
      [#include "/Alpac-report/templates/content-includes/html-foot.ftl"]
    </main>    
  </div>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.3/handlebars.min.js"></script>
  <script src="${ctx.contextPath}/.resources/Alpac-report/webresources/js/custom.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.js"></script>   
   <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
  <script src="${ctx.contextPath}/.resources/Alpac-report/webresources/js/custom-datatable.js"></script>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/highcharts-more.js"></script>
  <script src="https://code.highcharts.com/modules/broken-axis.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="${ctx.contextPath}/.resources/Alpac-report/webresources/js/hf-charts.js"></script>
  <script src="${ctx.contextPath}/.resources/Alpac-report/webresources/js/hf-map.js"></script>
  <script src="${ctx.contextPath}/.resources/Alpac-report/webresources/js/plotly-chart.js"></script>

  
  </body>
</html>


