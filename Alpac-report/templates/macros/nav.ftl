[#macro link linkPage title="" class="" showLink=true icon=""]
    [#compress]
        [#assign linkClass = class /]
    
        [#if navfn.isActive(model.root.content, linkPage)]
            [#assign linkClass = linkClass + " active" /]
        [/#if]
        [#if navfn.isOpen(model.root.content, linkPage)]
            [#assign linkClass = linkClass + " navitem-open" /]
        [/#if]       
       

        [#if linkPage.section?has_content]
            [#assign chapterNo =  (linkPage.chapter!"") + "." + (linkPage.section!"") /]            
        [#else]
            [#assign chapterNo =  (linkPage.chapter!"") /]
        [/#if]
						
        [#if title == ""]
            [#assign displayTitle= linkPage.navTitle!linkPage.title!linkPage.nodeName!'- No page title -' /]            
        [#else]
            [#assign displayTitle= title /]
        [/#if]
        
        [#if showLink == true]
            [#assign hrefLink = "href='" + cmsfn.link(linkPage) + "'" /]
        [#else]
            [#assign hrefLink = "" /]
        [/#if]
        <a ${hrefLink!} class="${linkClass!}">[#if icon?has_content]<span class="${icon}"></span>[/#if] <span>${chapterNo!}</span> ${displayTitle!}</a>
    [/#compress]
[/#macro]
