	<div id="breadcrumb">
		<ul>
      <li><a href="/">Home</a></li>
      <li class="seperator">/</li>
       [#assign currentPage = cmsfn.page(content)!"unknown"]
        [#list cmsfn.ancestors(content, "mgnl:page") as ancestor ]        
        
      
          [#if !navfn.isHiddenInNav(ancestor) && ancestor?index > 0]
            [#if !navfn.isActive(currentPage, ancestor)]
              [#assign link = cmsfn.link(ancestor) ] 
            [/#if] 
            <li><a href="${link!'#'}">          
                [#if ancestor.section?has_content]
                    Section ${ancestor.chapter!}.${ancestor.section!} 
                [#else]
                    Chapter ${ancestor.chapter!}
                [/#if]
            </a></li>
            [#if ancestor?has_next]
              <li class="seperator">/</li>
            [/#if]
          [/#if]        
        [/#list]
		</ul>
	</div>
