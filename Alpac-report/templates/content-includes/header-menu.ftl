[#import "/Alpac-report/templates/macros/nav.ftl" as nav /]

	<header class="header clear" role="banner">

		<div class="bookmark">
			<a href="/">
				<div class="logo">
					<img src="${ctx.contextPath}/.resources/Alpac-report/webresources/img/alpac-logo-top-01.svg" id="logo-top">
					<img src="${ctx.contextPath}/.resources/Alpac-report/webresources/img/alpac-logo-bottom-01.svg" id="logo-bottom">
				</div>
			</a>
			<div class="menuActivator">
				<button class="hamburger hamburger--spin" type="button">
				  <span class="hamburger-box">
					<span class="hamburger-inner"></span>
				  </span>
				</button>
			</div>

			<div class="siteTitle">
				<div class="titleCenter">
					<p>
						<span class="reportTitle">Status of Biodiversity in the </span> <span class="subTitle">Al-PAC Forest Management Agreement Area</span>
					</p>
				</div>
			</div>
		</div>
		  

		<div class="slideNav">
			<nav class="navigation">
				<div class="homeLink">
					<a href="/home">
						<svg id="e6be7e63-2b2a-4c82-b357-3891b28c0757" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 347.971 347.971"><polygon points="340.114 140.296 173.994 0 7.857 140.296 7.857 171.313 33.531 171.27 33.531 347.971 135.409 347.971 135.409 248.525 212.548 248.525 212.548 347.971 314.428 347.971 314.428 171.27 340.114 171.313 340.114 140.296"/></svg>
						Report at a Glance
					</a>
				</div>
				<ul id="supportNav">
					<li>
						<a href="/home">Executive Summary</a>
					</li>
				</ul>
				<h2>Chapters:</h2>
				[#assign homepage = navfn.rootPage(content)!]
				[#assign navItems = navfn.navItems(homepage)]			
				[#assign pageList = [homepage] ]
				[#assign currentPageIndex = 0]
				<ul id="mainNav">
					[#list navItems as navItem]
						[#if !navfn.isHiddenInNav(navItem)]
							[#assign pageList = pageList + [navItem] ]
							[#if navfn.isActive(content, navItem) ]
							 		[#assign currentPageIndex = pageList?size - 1]
							[/#if]
							[#assign subnavItems = navfn.navItems(navItem) /]
							[#if subnavItems?? && subnavItems?size > 0]
								[#if navfn.isActive(content, navItem) || navfn.isOpen(content, navItem)]
									[#assign chapterClass = " active toggled" /]
								[#else]
									[#assign chapterClass = "" /]
								[/#if]

								
								<li class="hasChildren ${chapterClass!}"> 								
									[@nav.link linkPage=navItem! /] 
																
									<div class="subToggle">
										<svg class="icon">
										<use xlink:href="#icon-keyboard_arrow_down" />
										</svg>
									</div>
									
									<div class="subNav">
										<ul>
											[#list subnavItems as subnavItem]
												[#if !navfn.isHiddenInNav(subnavItem)]
													[#assign pageList = pageList + [subnavItem] ]
												 	[#if navfn.isActive(content, subnavItem)]
														[#assign sectionClass = " active" /]
														[#assign currentPageIndex = pageList?size - 1]
													[#else]
														[#assign sectionClass = "" /]								
													[/#if]    
													<li class="${sectionClass!}">[@nav.link linkPage=subnavItem /]</li>
												[/#if]
											[/#list]
										</ul>
									</div>
								</li>
							[#else]
								<li>[@nav.link linkPage=navItem class="navigation-mainLink" /]</li>
							[/#if]
						[/#if] 
					[/#list]			

				</ul>
			</nav>

			<div class="menuFooter">
				<form role="search" action="" id="searchform">
					<input type="text" class="searchInput input" name="s" placeholder="Search"/>
					<button type="submit" class="form-control searchSubmit" alt="Search">
						<svg class="icon">
						  <use xlink:href="#icon-search" />
						</svg>
					</button>

				</form>
				<div class="termsCond">
					<p>General Terms and Conditions  /  All Rights Reserved</p>
				</div>
			</div>


		</div>
	</header>



	<div class="arrowNavigation">
		<div class="leftArrow-abmi">	
			[#if currentPageIndex >0  && (pageList)?size > currentPageIndex ]
			<a title="${pageList[currentPageIndex -1].navTitle!}" href="${cmsfn.link(pageList[currentPageIndex -1]!)}">
				<svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M1.86947e-07 15.6771L7.8125 8.33333L1.18007e-08 0.989583L1.09375 1.06206e-07L10 8.33333L1.09375 16.6667L1.86947e-07 15.6771Z" fill="#EAEBE9"/>
				</svg>
				<span>Previous Page:<br>${pageList[currentPageIndex -1].navTitle!pageList[currentPageIndex -1].title!}</span>
			</a>
			[/#if]
		</div>
		<div class="rightArrow-abmi">
			[#if (currentPageIndex + 1) < pageList!?size ]
				<a title="${pageList[currentPageIndex +1].navTitle!}" href="${cmsfn.link(pageList[currentPageIndex +1])}">				
					<svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M1.86947e-07 15.6771L7.8125 8.33333L1.18007e-08 0.989583L1.09375 1.06206e-07L10 8.33333L1.09375 16.6667L1.86947e-07 15.6771Z" fill="#EAEBE9"/>
					</svg>
					<span>Next Page: <br>${pageList[currentPageIndex + 1].navTitle!pageList[currentPageIndex + 1].title!}</span>
				</a>
			[/#if]
		</div>
	</div>
