
<footer>
	<div class="container large">
		<div class="row">
			<div class="col s12 m4 l3">
				<ul class="footerNav">
					[@cms.area name="footer-link" /]
				</ul>
			</div>
			<div class="col s12 m8 l6 contactInfo">
				<h3>Alberta Biodiversity Monitoring Institute</h3>
				<div class="row">
					<div class="col s12 m6 l6 xl5">
						
							[@cms.area name="address" /]
						
					</div>
					<div class="col s12 m6 l6 xl7">
						
						[@cms.area name="contact-infos" /]
						
					</div>
				</div>
			</div>
			<div class="col s12 m12 l3 copyright">
				<p> [@cms.area name="copyright" /]<br>
					[@cms.area name="privacy-link" /]<br>
				</p>
			</div>
			<#--  <div class="col s12 m12 l4">
				<form action="//abmi.us4.list-manage.com/subscribe?u=de3236a0b48dda3b348d3943b&amp;id=560ef11599" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank">

						<input type="email" value="" name="EMAIL" class="newslettersignup email newsletter_email" id="mce-EMAIL" placeholder="Join our mailing list" title="email address" required="">
						<input type="submit" class="newslettersignup btn" value="Sign Up">

				</form>
				<p class="learnMoreAbmi"><a href="https://abmi.ca">Learn more about ABMI »</a></p>
			</div>  -->
		</div>
		<hr>
		<#--  <div class="row">
			<div class="col s12 m6 l6 social">
				<p><a href="#"><span class="ion-social-instagram"></span></a> <a href="#"><span class="ion-social-facebook"></span></a></p>
			</div>
			<div class="col s12 m6 l6 copyright">
				<p> [@cms.area name="copyright" /]<br>
					[@cms.area name="privacy-link" /]<br>
				</p>
			</div> 
		</div>  -->
	</div>

</footer>



<!-- script src="${ctx.contextPath}/.resources/Alpac-report/webresources/js/scripts-min.js?v=1.2"></script -->       
