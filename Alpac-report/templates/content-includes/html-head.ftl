<head>
	<meta charset="utf-8">
	<title>${content.title!'ALPAC Report'}</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Domine:400,700|Montserrat:400,600,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="${ctx.contextPath}/.resources/Alpac-report/webresources/css/core.css">
	<link rel="stylesheet" href="${ctx.contextPath}/.resources/Alpac-report/webresources/css/custom.css">
</head>