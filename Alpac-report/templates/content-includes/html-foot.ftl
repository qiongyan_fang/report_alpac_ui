	
		<section class="section partnerLogos ${(content.greySponsor!false)?then('grey', '')}">
			<div class="container has-text-centered">
				<h3>This report is in partnership with</h3>

				<div class="columns is-mobile is-multiline is-centered">
					[@cms.area name="partner-area" /]					
				</div>
				
			</div>
		</section>

		<footer class="has-text-centered-mobile">
			<div class="container is-fluid">
				<div class="columns is-multiline">
				  <div class="column is-one-quarter">
						<div class="logo">
							<img src="${ctx.contextPath}/.resources/Alpac-report/webresources/img/alpac-logo-top-01.svg" id="logo-top">
							<img src="${ctx.contextPath}/.resources/Alpac-report/webresources/img/alpac-logo-bottom-01.svg" id="logo-bottom">
						</div>
				  </div>
				  <div class="column is-one-quarter rightPad">
				  		[@cms.area name="foot-note-area" /]					
				  </div>
				  <div class="column is-one-quarter rightPad">
				  		[@cms.area name="foot-abmi-area" /]							
				  </div>
				  <div class="column is-one-quarter">
						<ul class="footMenu">
							[@cms.area name="foot-menu" /]								
						</ul>
				  </div>
				  <div class="backtoTop">
				  	<a href="#">
				  		Back to top
				  		<svg class="icon">
							  <use xlink:href="#icon-keyboard_arrow_down" />
							</svg>
				  	</a>
				  </div>
				</div>
			</div>

			<div class="secondaryFooter">
				<div class="container is-fluid">
					<div class="columns">
						<div class="column is-narrow has-text-centered-mobile">
							[@cms.area name="foot-copyright-area" /]
						</div>
						<div class="column has-text-centered-mobile has-text-right">
							<p>Alberta-Pacific Forest Industries Inc.  /  Alberta Biodiversity Monitoring Instutute</p>
						</div>
					</div>
				</div>
			</div>
		</footer>
	