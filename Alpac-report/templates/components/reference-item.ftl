[#if content.reference?has_content]
  <div class="reference-item" id="reference${content.referenceId!}">  
    <span>${content.referenceId!}. </span> ${cmsfn.decode(content).reference!}
  </div>
  [/#if]
