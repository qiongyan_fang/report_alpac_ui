<a href="${content.url!}" class="${content.cssClass!}">
       [#if content.customIcon?hasContent || content.predefinedIcon?hasContent]
              <span class="${content.predefinedIcon!content.customIcon!''}"></span>
       [/#if]
       ${cmsfn.decode(content).title!}
</a>     
