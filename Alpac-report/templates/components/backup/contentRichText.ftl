[#if content.wrap!false == true || content.showMore!false == true ]
<div class="contentRichText">
  [#if content.richText?has_content]
    ${cmsfn.decode(content).richText!}
  [/#if]
</div>
[#else]
[#if content.richText?has_content]
    ${cmsfn.decode(content).richText!}
  [/#if]
[/#if]
