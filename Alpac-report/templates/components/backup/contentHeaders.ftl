
  [#if content.tag?has_content]
    [#assign styleText = "" /]
    [#if content.top?has_content && content.top != '0']
      [#assign styleText = styleText + "padding-top:" + content.top + "px;" /]
    [/#if]

    [#if content.bottom?has_content && content.bottom != '0']
      [#assign styleText = styleText + "padding-bottom:" + content.bottom + "px;" /]
    [/#if]


    [#if content.left?has_content && content.left != '0']
      [#assign styleText = styleText + "padding-left:" + content.left + "px;" /]
    [/#if]


    [#if content.right?has_content && content.right != '0']
      [#assign styleText = styleText + "padding-right:" + content.right + "px;" /]
    [/#if]

    [#if styleText!= '']
      [#assign styleText = 'style="' + styleText + '"' /]
    [/#if]
    
    [#if content.cssClass?has_content]
      [#assign styleClass = "class='" + content.cssClass + "'" /]
    [/#if]

    [#if content.tag == 'p']
      <p ${styleClass!} ${styleText!} >${cmsfn.decode(content).text!}</p>
    [#else]
      <h${content.tag!} ${styleClass!} ${styleText!}>${cmsfn.decode(content).text!}</h${content.tag!}>
    [/#if]
  [/#if]

