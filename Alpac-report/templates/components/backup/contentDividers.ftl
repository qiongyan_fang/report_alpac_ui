[#if content.type?has_content]
  [#if content.type == 'hr']
  <hr class="${content.cssClass!}" />
  [#else]
    <div style="clear:both"></div>
  [/#if]
[/#if]