<div class="featureBlock ${(content.textOnRight!true)?then('', 'flip')}">
  <div class="featureImage ${content.imageClass!''}">
    [#if content.image?has_content]
      [#assign image = damfn.getAsset(content.image!)!]
      [#assign imageLink = image.link!]
      <img 
        src='${imageLink!}'  >
    [/#if]						
  </div>
  <div class="featureContent">
    <p><span>${cmsfn.decode(content).description!}</span></p>
  </div>
</div>