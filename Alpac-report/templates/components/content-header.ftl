
  [#if content.tag?has_content]
    [#if content.tag == 'figure']
      <figure><figcaption  [#if content.anchorId?has_content] id="${content.anchorId!}" [/#if] >${cmsfn.decode(content).text!}
      </figcaption></figure>
    [#else]
      [#assign styleText = "" /]
      [#if content.top?has_content && content.top != '0']
        [#assign styleText = styleText + "padding-top:" + content.top + "px;" /]
      [/#if]

      [#if content.bottom?has_content && content.bottom != '0']
        [#assign styleText = styleText + "padding-bottom:" + content.bottom + "px;" /]
      [/#if]


      [#if content.left?has_content && content.left != '0']
        [#assign styleText = styleText + "padding-left:" + content.left + "px;" /]
      [/#if]


      [#if content.right?has_content && content.right != '0']
        [#assign styleText = styleText + "padding-right:" + content.right + "px;" /]
      [/#if]

      [#if styleText!= '']
        [#assign styleText = 'style="' + styleText + '"' /]
      [/#if]    

      [#if content.tag == 'p']
        [#assign tagName = content.tag /]
      [#else]
        [#assign tagName = "h" + content.tag /]
      [/#if]
              
      [#if content.wrapInColumn!false]      
          <div class="columns">
            <div class="column is-one-half-desktop">
      [/#if]
      

      <${tagName!}  [#if content.anchorId?has_content] id="${content.anchorId!}" [/#if] class="${content.cssClass!} ${content.additionalClass!?replace(',',' ')!''}" ${styleText!}>
      [#if content.image?has_content]
        [#assign image = damfn.getAsset(content.image)!]    
        [#if image??]
          [#assign imageLink = image.link!'']
          <img class="middle header-icon" src="${imageLink!}" />
        [/#if]
      [/#if]

      ${cmsfn.decode(content).text!}
      </${tagName!}>
      [#if content.wrapInColumn!false]      
            </div>
          </div>
      [/#if]    
    [/#if]
  [/#if]

