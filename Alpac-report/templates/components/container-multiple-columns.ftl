



<div class="column  ${cmsfn.isPreviewMode()?then('', 'bordercms')} ${((content.allSize!'0') == '0')?then('', content.allSize)} ${((content.mobile!'0') == '0')?then('', content.mobile)} ${((content.tablet!'0') == '0')?then('', content.tablet)} ${((content.desktop!'0') == '0')?then('', content.desktop)} ${((content.widescreen!'0') == '0')?then('', content.widescreen)} ${((content.fullhd!'0') == '0')?then('', content.fullhd)}   ${content.cssClass!} ${content.additionalClass!?replace(',',' ')!''}">  
  [@cms.area name="grid-columns" /]
</div>
