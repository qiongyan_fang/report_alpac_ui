<div class="col s12 m6 l3">
					<div class="resourceItem">
            [#if content.image?has_content]
              [#assign image = damfn.getAsset(content.image)]
              [#assign imageLink = image.link]
              <img
              src='${imageLink!}'
              class='img-responsive'
              alt='${content.header!"image"}'>
            [/#if]
          
              <h4> ${content.header!}</h4>
              ${cmsfn.decode(content).description!}
              [#if content.urlLink?has_content]
              [#assign target = cmsfn.contentById(content.urlLink, "website")!]
                <a href='${ctx.contextPath}/${cmsfn.link(target)!"#"}' >View</a>
              [#else]
                <p><a href="#">View »</a></p>
              [/#if]
						
					</div>
</div>