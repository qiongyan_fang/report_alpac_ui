
<div style="width:100%;position:relative" >
<div id="violin${cmsfn.asJCRNode(content).getUUID()}"></div>
<div id="hviolin${cmsfn.asJCRNode(content).getUUID()}" class="violin-hover"><span class="ion-close"></span><div class="content"></div></div>
</div>
[#if cmsfn.isEditMode()]		
<h1> violin chart will be rendered here</h1>
[/#if]
<script>
	$(document)
	.ready(
			function() {
				var year = ${(content.year!2017)}				
				var title = ''; //${content.graphTitle!}';
				var yLabel = '${content.yLabel!}';
				var version = '${content.version!}';
				var yMin = ${content.yMin!0};
				var yMax = ${content.yMax!100};
				var species = '${content.speciesGroup!}';
				
				var useSpecialLabel = ${(content.symetricYLabel!false)?c};
				var groupBySpp =  ${(content.groupSpecies!false)?c};
				var displayDots =  ${(content.displayDots!false)?c};
				var bHumanFootprint =  ${(content.isHumanFootprint!false)?c};
				var divId = 'violin${cmsfn.asJCRNode(content).getUUID()}';
				var rankedSpp = ${(content.rankedSpp!false)?c};
				var nRecord = '${content.nRecord!5}'; 
				var url;				
				if (bHumanFootprint) {
					url = API_ROOT + '/report/getViolinChartHFData';
				} else {
					url = API_ROOT + '/report/getViolinChartData';
				}			
				url += '?year=' + year + ' &version=' + version + '&bGroupByTaxon=' + groupBySpp;
				if (species && species != 'All') {
					url += '&sppFullName=' + species;
				}
				var region = "${content.region!'All'}";
				if (region && region != 'All') {
					url += '&subRegionFullName=' + region;
				}
				if (rankedSpp) {
					url += '&showSpecies=true&nRecord=' + nRecord;
				}
				
				var yRange = [yMin, yMax];				
				createViolinChart(url, divId, title, yLabel, yRange, displayDots, useSpecialLabel, rankedSpp, species);
				
			});

	</script>

