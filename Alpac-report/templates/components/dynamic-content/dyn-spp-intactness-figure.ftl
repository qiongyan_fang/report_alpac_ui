
<div style="width:100%;position:relative" >
<div id="si${cmsfn.asJCRNode(content).getUUID()}"></div>
</div>
[#if cmsfn.isEditMode()]		
<h1>  chart will be rendered here</h1>
[/#if]
<script>
	$(document)
	.ready(
			function() {
				var year = '${(content.version!2020)}';
				var title = ''; //${content.graphTitle!}';
				var yLabel = '${content.yLabel!}';		
				var speciesGroup = '${content.speciesGroup!}';
				var displayFieldName = "${content.displayFieldName!'scientificName'}";
				const divId = 'si${cmsfn.asJCRNode(content).getUUID()}';
				let range = [];
				try {
					[#if content.leftBreakMin?has_content && content.leftBreakMax?has_content]
						const value1 = -1* (100 - Math.abs(parseInt(${content.leftBreakMin})));
						const value2 = -1* (100 - Math.abs(parseInt(${content.leftBreakMax})));
						range.push({ to: Math.max(value1, value2), from: Math.min(value1, value2), breakSize: 2 });
					[/#if]
				}catch(e) {}
				try {
					[#if content.rightBreakMin?has_content && content.rightBreakMax?has_content]
					 	const value3 = 100 - parseInt(${content.rightBreakMin});
						const value4 = 100 - parseInt(${content.rightBreakMax});
						range.push({ to: Math.max(value3, value4), from: Math.min(value4, value3), breakSize: 2 });
					[/#if]
				} catch(e) {}
				const tickInterval = parseInt('${content.xUnit!25}');
				createSpeciesIntactnessFigure( divId, speciesGroup, displayFieldName, year, range, tickInterval);				
			});

	</script>

