<script>
$( document ).ready(function() {
	
	let startYear;

  try {
    startYear = parseInt("${content.startYear!1990}");
  } catch (e) {
    startYear = 1999;
  }
	const endYear = parseInt("${content.endYear!2016}");
	const publishYear = parseInt("${content.publishYear!2018}");
	const xLabel = "${content.xLabel!'Human Footprint (%)'}";
	const xBreak = parseFloat('${content.xBreak!40}');
	const version = 'data' + endYear + '-process' + publishYear ;
	const divId = 's${cmsfn.asJCRNode(content).getUUID()}';
  	const regionId = null;
	createStackBar(divId, regionId, startYear, endYear, version, xLabel, xBreak);
});
	
</script>

<div id="s${cmsfn.asJCRNode(content).getUUID()}">
	<div class="columns">
		<ul class="tabList chart" style="margin: 1rem!important;">
			<li class="tab column is-one-forth"><a href="#s${cmsfn.asJCRNode(content).getUUID()}graphTab2" class="btn is-default-tab is-active">View
					as Graph</a></li>
			<li class="tab column is-one-forth"><a href="#s${cmsfn.asJCRNode(content).getUUID()}tableTab2" class="btn">View as
					Table</a></li>		
		</ul>
	</div>
	<div id="s${cmsfn.asJCRNode(content).getUUID()}graphTab2" class="">
		<figure>
			<div class="stackBarChart"></div>
		</figure>
		<figcaption>${content.title!}</figcaption>
	</div>

	<div id="s${cmsfn.asJCRNode(content).getUUID()}tableTab2" class="columns is-centered " style="display: none;">
		<div class="column is-two-thirds-widescreen">

			<table class="table is-striped colored">
				<thead>
					<tr class="hfSumamryTableHeader">
						<th class="iconHead"></th>					
						<th class="nameHead">Type</th>
					</tr>
				</thead>

				<tbody class="hfSumamryTableBody">

				</tbody>
			</table>
		</div>
	</div>
</div>

