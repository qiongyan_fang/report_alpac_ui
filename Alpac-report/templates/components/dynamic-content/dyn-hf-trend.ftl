
<script>
$( document ).ready(function() {	
	var startYear;
  try {
    startYear = parseInt("${content.startYear!1990}");
  } catch (e) {
    startYear = 1999;
  }
	var endYear = parseInt("${content.endYear!2016}");
  var publishYear = parseInt("${content.publishYear!2018}");

	var version = 'data' + endYear + '-process' + publishYear ;
	var divId = 't${cmsfn.asJCRNode(content).getUUID()}';
  var regionId = '${content.regionId!1}';
	createTrendingCharts(divId, regionId, startYear, endYear, version);	
});
	
</script>


<div class="row tableToggle sliderContainer  hfTrend active">
<div id="t${cmsfn.asJCRNode(content).getUUID()}"  class="figureSlide slickSlider">
</div>
</div>	