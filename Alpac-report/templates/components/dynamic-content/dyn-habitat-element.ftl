<script>
$( document ).ready(function() {
	
	var year;

  try {
    version = parseInt('${content.version!2017}');
  } catch (e) {
    year = 2017;
  }
  var typeId = '${content.typeId!1}';
	var divId = 'nh${cmsfn.asJCRNode(content).getUUID()}';  
	var yAxis = '${cmsfn.decode(content).yLabel!}';
  var title = '${cmsfn.decode(content).title!}';

	createHabitataElement(divId, version, typeId, title, yAxis);

});
	
</script>

<div class="row tableToggle" id="nh${cmsfn.asJCRNode(content).getUUID()}">	
</div>

