<script>
$( document ).ready(function() {	
  //const mapVersion = "${content.mapVersion!'Human Footprint 2017'}";
	const divId = 'map${cmsfn.asJCRNode(content).getUUID()}';
  const layerCollection =  {
    hf: { layers: [3665, 3668, 3691, 3677, 3681, 3687], aoi: 'default', mask: 'default'}, // alpac + EIA
    nativeHabitat: { layers: [7781,7783,7784,7785], aoi: 'default', mask: 'default', useRadioButton: true},
    linear: {layers: [7774,7775,7776,7777,7778,7779,7780], aoi: 'default', mask: 'default'},
    SpectralRegeneration: {layers: [7791, 7792, 7793, 7794, 7795], aoi: 'default', mask: 'default'},
    boundary: {layers: ['7609', '7790']}, // alpac + EIA
    ecosystem: {layers: ['24', '36', '38', '39', '40', '42', '43', '25', '44', '26', '45'], aoi: ['7609', '7790'], mask: '7609' },
    wetland: {layers: ['3662', '3661', '3659', '3660', '3663', '3658'], aoi: ['7609', '7790'], mask: '7609'},
    file_history: {layers: ['7772'], aoi: ['7609', '7790'], mask: '7609'},
    parks: { layers: ['203', '202'], aoi: ['7609', '7790'], mask: '7609'},
    'Harvest Areas Analyzed': { layers: [7791], aoi: 'default', mask: 'default', zoom: '7790'},
    'Detected Year of Harvest': { layers: [7792], aoi: 'default', mask: 'default'},
    '5-year Post-harvest Spectral Regeneration': { layers: [7793], aoi: 'default', mask: 'default'},
    'Years to Reach 80% Spectral Regeneration': { layers: [7794], aoi: 'default', mask: 'default'},
    'Current Levels of Regeneration': { layers: [7795], aoi: 'default', mask: 'default'}  ,
    'Overall Intactness': { layers: [7802, 7801, 7799, 7798], aoi: 'default', mask: 'default', useRadioButton: true, isSingleColorScheme: {isReverseOrder:true}, }  ,
    'NNative Plant Richness': { layers: [7797], aoi: 'default', mask: 'default'}     
  };

  const defaultSetting = {
    aoi: ['7609' , '7790'],
    mask: '7609'
  };
	const mapDivId = 'map${cmsfn.asJCRNode(content).getUUID()!}';
  const containerDivId = mapDivId + '_container';
  const mapName = "${content.mapVersion!'hf'}";
  let currentLayers = layerCollection[mapName];
  
  if (currentLayers) {
    currentLayers.layers  = currentLayers.layers.map (x => x + '');
    if (currentLayers.aoi === 'default') {
      currentLayers.aoi = defaultSetting.aoi;
    }
    if (currentLayers.mask === 'default') {
      currentLayers.mask = defaultSetting.mask;
    }
    [#if  cmsfn.isPreviewMode()]
      initMap(mapDivId, currentLayers);    
    [/#if]
  }
});
</script>
<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.js'></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>    
<script src='https://npmcdn.com/@turf/turf/turf.min.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v1.1.0/mapbox-gl.css' rel='stylesheet' />

<div  id="map${cmsfn.asJCRNode(content).getUUID()}_container"  style="position: relative;" class="mapbox-container">
  <div id="map${cmsfn.asJCRNode(content).getUUID()}" class="hf-map">    
    [#if  !cmsfn.isPreviewMode()]
    <h3> render map here </h3>
    [/#if]
  </div>
  <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>  
  <div id="hf-legend" class="compress">
    <span class="icon"></span><Strong>Legend</Strong>
    <div class="legend-container"> 
      <div class="toggle-legend arrow-down"></div>
        <div class="normal"></div>
        <div class="aoi" ></div>
    </div>
  </div>
  <button id="resize-map" style="display:none"></button>
</div>
    

