
[#if cmsfn.isEditMode()]		
<h1>  chart will be rendered here</h1>
[/#if]

	<div class="centerSelect">
		<hr>

		<div class="dropdown">
			<div class="dropdown-trigger">
			<button class="button" aria-haspopup="true" aria-controls="dropdown-menu">
				<span id="selected-spp">Select a Species</span>
						<svg class="icon is-small">
							<use xlink:href="#icon-keyboard_arrow_down"></use>
						</svg>

			</button>
			</div>
			<div class="dropdown-menu" role="menu">
			<div class="dropdown-content ">
				<ul class="tabList" id="sector-effect-spp">							   
				</ul>
			</div>
			</div>
		</div>
	</div>		
	

	<div class="selectTab" id="selectTab1">		
		<div class="columns">
			<div class="column is-one-half-desktop">
				<div id="under${cmsfn.asJCRNode(content).getUUID()}"></div>
				<figure>
					<figcaption>					
					Figure: Species Local-footprint Sector Effects. Predicted percentage change in species' relative abundance inside areas that have been disturbed by each sector within the Al-Pac FMA area compared to the habitat it replaces. Sector effects values <0% indicate a negative response to a particular sector and >0% indicate a positive response.					
				</figure>
			</div>
			<div class="column is-one-half-desktop">
				<div id="se${cmsfn.asJCRNode(content).getUUID()}"></div>
				<figure>						
					<figcaption>
					Figure: Species Regional Sector Effects. Percentage change in bird species relative abundance due to the footprints of each industrial sector in the Al-Pac FMA area. Total effects values <0% indicate a predicted decrease in the regional population due to a particular sector and >0% indicate a predicted increase.
					</figcaption>
				</figure>
			</div>
		</div>
	</div>

<script>
	$(document)
	.ready(
			function() {
				const year = parseInt('${(content.year!2020)}');
				const title = ''; //${content.graphTitle!}';
				const yLabel = '${content.yLabel!}';		
				const sppGroup = '${content.speciesGroup!}';	
				const displayFieldName = "${content.displayFieldName!'scientificName'}";			
				const divId = '${cmsfn.asJCRNode(content).getUUID()}';				
				let url =  API_ROOT + 'report/getHFSectorChartData';				
				url += '?year=' + year + '&spp=';		
				const yMin = ${content.yMin!0};
				const yMax = ${content.yMax!100};		
				const yRange = [yMin, yMax];
				
				loadSectorEffectBarChart(url, (sppGroup=='All' ? null : sppGroup), displayFieldName, divId, year, title, yLabel, yRange);
						
			});

	</script>

