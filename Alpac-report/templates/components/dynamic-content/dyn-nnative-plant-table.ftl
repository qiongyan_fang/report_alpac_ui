<script>
$( document ).ready(function() {	
	var version = "${content.year!}";	
	var divId = 't${cmsfn.asJCRNode(content).getUUID()}';  
	createNNSpeciesTable('#' + divId + ' .NNP-table', version);
});
</script>
<div id="t${cmsfn.asJCRNode(content).getUUID()}"> 
	<table class="table is-striped NNP-table">
		<thead>
			<tr>
				<th class="nameHead img">Image</th>
				<th class="nameHead cname">Common Name</th>
				<th class="nameHead sname">Scientific Name</th>
				<th class="percentHead percentBod  value">Occurrence (%)</th>
				<th class="nameHead percentBod status">Status</th>
			</tr>
		</thead>				
		<tbody ></tbody>
	</table>
	
</div>