<script>
$( document ).ready(function() {	
	var version = "${content.year!}";	
	var divId = 't${cmsfn.asJCRNode(content).getUUID()}';  
	createCultureImportantSpeciesTable('#' + divId + ' .culture-table', version);
});
</script>
<div id="t${cmsfn.asJCRNode(content).getUUID()}"> 
	<table class="table is-striped culture-table">
		<thead>
			<tr>
				<th class="nameHead img">Image</th>
				<th class="nameHead">Taxonomic Group</th>
				<th class="nameHead cname">Common Name</th>
				<th class="nameHead sname">Scientific Name</th>
				<th class="percentHead value">Occurrence (%)</th>
				<th class="percentHead value">Intactness</th>
				<th class="nameHead status">Reference</th>
				<th class="nameHead status">Status</th>				
				<th class="nameHead status">Upland Forest</th>
			</tr>
		</thead>				
		<tbody ></tbody>
	</table>	
</div>