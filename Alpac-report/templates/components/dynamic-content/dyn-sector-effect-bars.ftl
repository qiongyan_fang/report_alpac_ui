<div style="width:100%;position:relative" >
<div id="s${cmsfn.asJCRNode(content).getUUID()}"></div>
<div id="hs${cmsfn.asJCRNode(content).getUUID()}" class="violin-hover"><span class="ion-close"></span><div class="content"></div></div>
</div>
[#if cmsfn.isEditMode()]		
	<h1> Sector chart will be rendered here</h1>
[/#if]
	<script>
	$(document)
	.ready(
			function() {				
				var year = ${(content.year!2016)}
				var title = ''; //${content.graphTitle!}';
				var yLabel = '${content.yLabel!}';				
				var yMin = ${content.yMin!0};
				var yMax = ${content.yMax!100};				
				var spp = '${content.spp!}';		

			//	var region = "${content.region!'All'}";
				var divId = 's${cmsfn.asJCRNode(content).getUUID()}';				
				var url =  API_ROOT + 'report/getHFSectorChartData';				
				url += '?year=' + year + '&spp=' + spp;
			
				var yRange = [yMin, yMax];
				
				createSectorEffectBarChart(url, divId, title, yLabel, yRange);			
			});

	</script>

