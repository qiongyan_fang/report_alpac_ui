<script>
$( document ).ready(function() {	
	var endYear = parseInt("${content.endYear!2016}");
  	var publishYear = parseInt("${content.publishYear!2018}");

	var version = ''; //data' + endYear + '-process' + publishYear ;
	var divId = 'ld${cmsfn.asJCRNode(content).getUUID()}';
  	var regionId = null;
	createLinearStackBar(divId, regionId, endYear, version);
});
	
</script>

<div class="columns" id="ld${cmsfn.asJCRNode(content).getUUID()}">
	<div class="column">
		<div class="columns is-centered">		
			<ul class="tabList chart" style="margin: 1rem!important;"> <!-- use unique anchor id for different component -->
				<li class="tab column is-one-forth"><a href="#g${cmsfn.asJCRNode(content).getUUID()}" class="btn is-active">View
						as Graph</a></li>
				<li class="tab column is-one-forth"><a href="#t${cmsfn.asJCRNode(content).getUUID()}" class="btn">View as
						Table</a></li>		
			</ul>
		</div>
		<div id="g${cmsfn.asJCRNode(content).getUUID()}" class="is-active" style="display: none;" >
			<figure>
				<div class="stackBarChart"></div>
			</figure>
			<figcaption>${content.title!}</figcaption>
		</div>
		<div id="t${cmsfn.asJCRNode(content).getUUID()}" style="display: none;">			
			<table class="table is-striped">
				<thead>
					<tr class="hfSumamryTableHeader">
						<th class="nameHead">Type</th>
					</tr>
				</thead>
				<tbody id="hfSumamryTableBody">
				</tbody>
			</table>			
		</div>
	</div>
</div>

