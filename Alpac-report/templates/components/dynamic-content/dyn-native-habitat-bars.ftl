<script>
$( document ).ready(function() {
	
	var year;

  try {
    year = parseInt('${content.year!2016}');
  } catch (e) {
    year = 2016;
  }
	var divId = 'nh${cmsfn.asJCRNode(content).getUUID()}';  
	var yAxis = '${content.yLabel!}'

	createNativeHabitatGroupedBars(divId, year, yAxis);

});
	
</script>

<div class="row tableToggle" id="nh${cmsfn.asJCRNode(content).getUUID()}">	
</div>

