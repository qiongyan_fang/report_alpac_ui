<script>
$( document ).ready(function() {	
	var year = "${content.year!}";	
	var divId = 't${cmsfn.asJCRNode(content).getUUID()}';  
	createSpeciesAtRiskTable('#' + divId + ' #species-at-risk-List', year);
});
</script>
<div id="t${cmsfn.asJCRNode(content).getUUID()}"> 
	<table class="table  " id="species-at-risk-List">
		<thead>
			<tr>
				<th class="nameHead img">Image</th>
				<th class="nameHead taxon">Taxon</th>
				<th class="nameHead cname">Common Name</th>
				<th class="nameHead sname">Scientific Name</th>
			
				<th class="percentHead occurrence">Occurrence (%)</th>
				<th class="percentHead intactness">Biodiversity Intactness</th>
				<th class="percentHead reference">Habitat Suitability Increase or Decrease</th>			
				<th class="nameHead status">Status</th>
			</tr>
		</thead>				
		<tbody ></tbody>
	</table>
</div>