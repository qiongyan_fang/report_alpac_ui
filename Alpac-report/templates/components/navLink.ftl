[#import "/Alpac-report/templates/macros/nav.ftl" as nav /]
<li ${content.subLinks?has_content?then('class=hasChildren','')}> 
[#if content.subLinks?has_content ]
    <div class="sub-toggle"> </div>
[/#if]
[#if content.link?has_content ]     
      [#if (cmsfn.contentById(content.link)??)]
           
           [#--  nav.link linkPage=cmsfn.contentById(content.link!) title=content.title class="navigation-mainLink" showLink=!content.subLinks?has_content  --]
           [@nav.link linkPage=cmsfn.contentById(content.link!) title=content.title class="navigation-mainLink" showLink=true /] 
            [#if content.subLinks?? && cmsfn.isPreviewMode()]
              <div class="sub-menu-wrap">
                <ul class="sub-menu">
                    [#list cmsfn.children(content.subLinks) as link]
                    [#assign subLink = cmsfn.contentById(link.link!)! /]
                    [#if subLink?has_content]
                        <li>[@nav.link linkPage=subLink title=link.title /]</li>
                    [/#if]
                [/#list]
            </ul>
            </div>
            [/#if]
        [/#if]
       
    [#else]
        [#if content.title?has_content]
            <a>${content.title!}</a>
        [#else]
          <a>Page removed</a>
        [/#if]
    [/#if]
</li>
