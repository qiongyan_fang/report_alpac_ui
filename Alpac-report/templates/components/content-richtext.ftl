[#if content.wrap!false == true || content.showMore!false == true ]
<div id="${content.anchorId!}" class="contentRichText richtext ${content.cssClass!} ${content.additionalClass!?replace(',',' ')!''}">
  [#if content.richText?has_content]
    ${cmsfn.decode(content).richText!}
  [/#if]
</div>
[#else]
  [#if content.richText?has_content]
  <div class="contentRichText richtext ${content.cssClass!} ${content.additionalClass!?replace(',',' ')!''}">
    ${cmsfn.decode(content).richText!}
  </div>
  [/#if]
[/#if]
