[#import "/Alpac-report/templates/macros/nav.ftl" as nav /]
[#--  [#assign homepage = navfn.rootPage(content)!]  --]

[#list 1..3 as i]
    [#assign navigationItem = navfn.ancestorPageAtLevel(content, i)]
    
    [#--  check if the current  page (not content)  is same as nav itme, true when it is parent node. --]
    [#if !navfn.isOpen(model.root.content, navigationItem)]
        [#break]
    [/#if]
    
    [#if navigationItem?has_content && !navfn.isHiddenInNav(navnavigationItemItem)]
        <a>[@nav.link linkPage=navigationItem title=navigationItem.title!'no title' /]</a>/        
    [/#if]
    
[/#list]

