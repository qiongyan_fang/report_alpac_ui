
  [#if cmsfn.isEditMode()]<div cms:edit>[Tab Header] <span class="${content.predefinedIcon!content.customIcon!''}"></span> [#if content.title?has_content]${content.title!} [/#if]</div>
  [Tab Content-> edit here]
  [/#if]

  
    [@cms.area name="single-tab-content-area" /]
  
