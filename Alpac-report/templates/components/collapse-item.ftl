<li>
    <div class="collapsible-header" tabindex="0">${content.header!}<span class="ion-arrow-down-b"></span></div>
    <div class="collapsible-body">
        [@cms.area name="collapsible-body" /]
    </div>
</li>