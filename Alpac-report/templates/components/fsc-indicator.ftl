[#if cmsfn.isEditMode()]
<div>
[/#if]
<div class=" columns fscLockup">
   <div class="column is-3 has-text-centered">
      [#assign bgImage = damfn.getAsset(content.bgImage)!]    
      [#if bgImage??]
         [#assign bgImageLink = bgImage.link!'']   
      [/#if]
      <div class="fscBg" style="background-image: url(${bgImageLink!!});">
         [#assign image = damfn.getAsset(content.image)!]    
         [#if image??]
            [#assign imageLink = image.link!'']
            <img src="${imageLink!}">
         [/#if]
      </div>
   </div>
   <div class="column is-9 fscContent ${content.bgClass!}">
      <p class='fscTitle'>${content.header!}</p>
      <div> ${cmsfn.decode(content).description!} </div>
   </div>
</div>
[#if cmsfn.isEditMode()]
</div>
[/#if]