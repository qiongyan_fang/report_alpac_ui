[#import "/Alpac-report/templates/macros/nav.ftl" as nav /]
[#assign homepage = navfn.rootPage(content)!]
[#assign navItems = navfn.navItems(homepage)]
<div class="mobileWrap">
    <ul class="mainNav">
        [#list navItems as navItem]
            [#if !navfn.isHiddenInNav(navItem)]
                [#assign subnavItems = navfn.navItems(navItem) /]
                [#if subnavItems?? && subnavItems?size > 0]
                    <li class="hasChildren"> 
                        <div class="sub-toggle"> </div>
                        [@nav.link linkPage=navItem class="navigation-mainLink" /]
                    
                        <div class="sub-menu-wrap">
                            <ul class="submenu">
                                [#list subnavItems as subnavItem]
                                    [#if !navfn.isHiddenInNav(navItem)]
                                        <li>[@nav.link linkPage=subnavItem /]</li>
                                    [/#if]
                                [/#list]
                            </ul>
                        </div>
                    </li>
                [#else]
                    <li>[@nav.link linkPage=navItem class="navigation-mainLink" /]</li>
                [/#if]
            [/#if] 
        [/#list]
     
        <li class="signUp" id="userInfo-4">
            <authentication v-cloak>               
                <template slot="secured">
                    <a class="button" href="/home/projects.html"  slot="secured">My Projects</a>
                </template>        
            </authentication>                     
        </li>
    </ul>
</div>