 <section [#if content.anchorId?has_content] id="${content.anchorId!}" [/#if] class="section ${content.background!} ${content.bgImage!} ">
    <div class="${(content.addLeftRightMargin!false)?then('container','')} ${content.cssClass!} ${content.additionalClass!?replace(',',' ')!''}">
        [@cms.area name="container" /]        
    </div>
</section>
