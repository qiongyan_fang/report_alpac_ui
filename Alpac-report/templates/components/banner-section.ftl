<section class="pageHead inner section">

	[#include "/Alpac-report/templates/content-includes/bread-crumb.ftl"]



	<div class="columns is-multiline">



		<!-- Left Image Column -->
		<div class="column is-full-tablet is-two-fifths-widescreen is-one-third-fullhd imgBackground">
      [#if content.bgImage?has_content]
        [#assign image = damfn.getAsset(content.bgImage!)!]
        [#assign imageLink = image.link!]   
      [/#if]

	    <div class="headBg" style="background-image: url(${imageLink!});"></div>
			[#if content.creditText?has_content]
        		<span class="photo-credit ${content.creditColor!''} ${content.creditPosition!''} ${content.creditSize!''}">${content.creditText!''}</span>
      		[/#if]
			<div class="headContent innerHead">
				<div class="contentWrap container">
				  [#assign pageNode = cmsfn.page(content)!"unknown"]
            [#if pageNode.section?has_content]
              <p class="sectionNo">Section ${pageNode.chapter!}.${pageNode.section!}</p>
            [#else]
              <p class="sectionNo">Chapter ${pageNode.chapter!}</p>
            [/#if]
						<h1>${pageNode.navTitle!pageNode.title}</h1>
            <div>${cmsfn.decode(content).description!}</div>		
				</div>
			</div>
		</div>
		<!-- Left Image Column -->


		<!-- Right Dashboard Column -->
		<div class="column is-three-fifths-widescreen is-two-thirds-fullhd dashboard">
			<div class="contentWrap container">


				<!-- configurable area - will change per page -->
				<div class="columns is-multiline">
            		[@cms.area name="banner-content" /]
				</div>
				<!-- end configurable area -->


			</div>
		</div>
		<!-- Right Dashboard Column -->


	</div>


	<div class="headSvg"></div>

</section>