[#if content.fileUrl?has_content]
       [#assign image = damfn.getAsset(content.fileUrl)!]           
       [#if image??]      
              [#assign link = image.link!'']
       [/#if]
      
[#else]
       [#assign link = content.url!]    
[/#if]


<a href="${link!}" class="${content.cssClass!} ${content.additionalClass!?replace(',',' ')!''}">
       [#if content.customIcon?has_content || content.predefinedIcon?has_content]
              <span class="${content.predefinedIcon!content.customIcon!''}"></span>
       [/#if]
       ${cmsfn.decode(content).title!}
</a>     
