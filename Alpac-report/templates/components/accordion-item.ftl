<div class="accordion-header">${content.header!}</div>
<div class="accordion-content">${cmsfn.decode(content).desc!}
<@cms.area name="free-content"/>
</div>
