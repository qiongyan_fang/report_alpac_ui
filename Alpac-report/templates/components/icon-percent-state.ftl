[#if content.image?has_content]
        [#assign image = damfn.getAsset(content.image)!]
        [#assign imageLink = image.link!]
    [/#if]	
[#if !content.textOnSide!false]
  <div class="column has-text-centered statistics ${content.imageClass!''}" style="padding: 0px;">
      
          <img
            src='${imageLink!}'
            class='icon is-large'>
          
        <p class="percent">${content.percent!}</p>
        <p class="stat">${cmsfn.decode(content).description!}</p>
  </div>
[#else]
    <div class="column ">
          <p class="percent">
            <span class="icon is-large">
              <img   src='${imageLink!}'>
            </span>
            ${cmsfn.decode(content).description!} ${content.percent!}
          </p>
        </div>
[/#if]