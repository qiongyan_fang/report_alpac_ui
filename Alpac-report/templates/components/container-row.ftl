<div class="${cmsfn.isPreviewMode()?then('columns', 'bordercms')} ${(content.screenSize == '0')?then('', content.screenSize)} ${content.cssClass!} ${content.additionalClass!?replace(',',' ')!''}">
  [@cms.area name="side-by-side-column" /]
</div>
