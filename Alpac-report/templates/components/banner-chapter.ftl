<section class="pageHead chapter section">

  [#include "/Alpac-report/templates/content-includes/bread-crumb.ftl"]

  [#if content.bgImage?has_content]
    [#assign image = damfn.getAsset(content.bgImage)]
    [#assign imageLink = image.link]   
  [/#if]

	<div class="headBg" style="background-image: url(${imageLink!});"></div>
      [#if content.creditText?has_content]
        <span class="photo-credit ${content.creditColor!''} ${content.creditPosition!''} ${content.creditSize!''}">${content.creditText!''}</span>
      [/#if]

	<div class="headContent">
		<div class="contentWrap container">

			<div class="columns is-multiline">

				<div class="column is-full-tablet is-two-fifths-widescreen is-half-fullhd introduction">
					<div>
            [#assign pageNode = cmsfn.page(content)!"unknown"]
            [#if pageNode.section?has_content]
              <p class="sectionNo">Section ${pageNode.chapter!}.${pageNode.section!}</p>
            [#elseif pageNode.chapter?has_content]
              <p class="sectionNo">Chapter ${pageNode.chapter!}</p>
            [/#if]
						<h1>${pageNode.navTitle!}</h1>
            <div>${cmsfn.decode(content).description!}</div>						
					</div>
				</div>

				<div class="column is-three-fifths-widescreen is-half-fullhd sections">
          [#if pageNode.chapter?has_content]
					  <h2>${content.chapter!'What you’ll find in this chapter'}</h2>
          [/#if]

					<div class="columns is-multiline">          

            [#list navfn.navItems(pageNode) as navItem ]
              [#if !navfn.isHiddenInNav(navItem)]
                <div class="column is-half-tablet is-full-widescreen">
                  <h3><a href="${cmsfn.link(navItem)!}">${navItem.chapter!}.${navItem.section!} ${navItem.navTitle!navItem.title!navItem.@name}</a></h3>							
                  <p>${navItem.navigationDescription!}</p>
                </div>             
              [/#if]
            [/#list]
					</div>
				</div>

		</div>
	</div>


	<div class="headSvg"></div>

</div></section>