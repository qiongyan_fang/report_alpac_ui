[#assign imageBoxType = ctx.imageBoxType!'xxx' ]
[#if content.fileUrl?has_content]
    [#assign fileUrl = damfn.getAsset(content.fileUrl)!]    
    [#if fileUrl??]
      [#assign targetUrl = fileUrl.link!'']    
    [/#if]
[#else]
  [#if content.urlLink?has_content]
    [#assign targetUrl = content.urlLink!]
    [#if content.urlLink?starts_with("http")]
       [#assign target = '_blank']
     [#-- 
      [ assign targetUrl = ctx.contextPath + cmsfn.link (cmsfn.contentByPath(content.urlLink, "website")!)!'#' --]
    [/#if]
  [/#if]
[/#if]  
     
   
[#if imageBoxType == 'redTop flexRow']
  <div class="col s12 m12 l6">
    <div class="boxed ${imageBoxType}">      
      <div class="relWrap padded60" id="${content.anchorId!}">
        <div class="bordered"></div>
        <div class="container center-align">
          <h3 class="center-align">${cmsfn.decode(content).header!}</h3>
          <div class="center-align">${cmsfn.decode(content).description!}</div>
          [@cms.area name="box-content" /]								
        </div>
      </div>      
      <div class="relWrap padded40">
        [@cms.area name="out-box-content" /]
      </div>
    </div>
  </div>
[#elseif imageBoxType == 'sponsor flexRow']
  <div class="col s12 m12 l${content.largeColWidth!'3'} sponsor" id="${content.anchorId!}">  
    [#if content.image?has_content]
      <div class="image-container">
        [#assign image = damfn.getAsset(content.image)]
        [#assign imageLink = image.link!]
        [#if targetUrl?has_content]
          <a  href='${targetUrl!}'  target="${target!}"> 
          <img
            src='${imageLink!}'
            class='icon'
            alt='${content.header!"image"}'></a>
        [#else]  
            <img
            src='${imageLink!}'
            class='icon'
            alt='${content.header!"image"}'>
        [/#if]
        [#if content.creditText?has_content]
          <span class="photo-credit ${content.creditColor!''} ${content.creditPosition!''} ${content.creditSize!''}">${content.creditText!''}</span>
        [/#if]
      </div>
    [/#if]
  </div>  
[#elseif imageBoxType == 'resourcesFeed']
  <div class="col s12 m6 l6 xl3">
    <div class="resourceItem" id="${content.anchorId!}">    
      [#if content.image?has_content]
        <div class="image-container">
          [#assign image = damfn.getAsset(content.image)]
          [#assign imageLink = image.link!]
          [#if targetUrl?has_content]
            <a  href='${targetUrl!}'  target="${target!}">
            <img
              src='${imageLink!}'
              class='icon'
              alt='${content.header!"image"}'></a>
          [#else]  
              <img
              src='${imageLink!}'
              class='icon'
              alt='${content.header!"image"}'>
          [/#if]
          [#if content.creditText?has_content]
            <span class="photo-credit ${content.creditColor!''} ${content.creditPosition!''} ${content.creditSize!''}">${content.creditText!''}</span>
          [/#if]
        </div>
      [/#if]
      [#if targetUrl?has_content]
        <h4><a href='${targetUrl!}'  target="${target!}">${cmsfn.decode(content).header!}</a></h4>
      [#else]  
        <h4>${cmsfn.decode(content).header!}</h4>
      [/#if]
      [#if content.description?has_content]
          <div class="${(content.left!false)?then('left-align','')}"> ${cmsfn.decode(content).description!} </div>
      [/#if]
      [#if targetUrl?has_content]    
        <p><a href='${targetUrl!}'  target="${target!}">${content.urlText!'View'} »</a></p>
      [/#if]
    </div>
  </div>
[#elseif imageBoxType == 'col6Box flexRow']
  <div class="col s12 m12 l6">
    <div class="container overflow boxed padded40" id="${content.anchorId!}">
      [#if content.image?has_content]
        <div class="image-container">
          [#assign image = damfn.getAsset(content.image)]
          [#assign imageLink = image.link!]
          <img
          src='${imageLink!}'
          class='icon'
          alt='${content.header!"image"}'>
          [#if content.creditText?has_content]
              <span class="photo-credit ${content.creditColor!''} ${content.creditPosition!''} ${content.creditSize!''}">${content.creditText!''}</span>
          [/#if]
        </div>
      [/#if]
      <h5>${cmsfn.decode(content).header!}</h5>
      [#if content.description?has_content]
        <div class="${(content.left!false)?then('left-align','')}"> ${cmsfn.decode(content).description!} </div>
      [/#if]    
      [#if targetUrl?has_content]
        <p><a class='btn' href='${targetUrl!}'  target="${target!}">${content.urlText!'Read More'}</a></p>  
      [/#if]  
    </div>
  </div> 
  [#elseif imageBoxType == 'credits']
    <div class="col s12 m6 center-align" id="${content.anchorId!}">  
      <h4>${cmsfn.decode(content).header!}</h4>
      [#if content.description?has_content]
        <div class="${(content.left!false)?then('left-align','')}"> ${cmsfn.decode(content).description!} </div>
      [/#if]    
      <div class="sponsor">
        <a href='${targetUrl!}'  target="${target!}">
          [#if content.image?has_content]
            <div class="image-container">
              [#assign image = damfn.getAsset(content.image)]
              [#assign imageLink = image.link!]
              <img
              src='${imageLink!}'
              class='icon'
              alt='${content.header!"image"}'>
              [#if content.creditText?has_content]
                <span class="photo-credit ${content.creditColor!''} ${content.creditPosition!''} ${content.creditSize!''}">${content.creditText!''}</span>
              [/#if]
            </div>
          [#else]
            ${content.urlText!'Read More'}
          [/#if]      
        </a>
      </div>           
    </div>
  [/#if]