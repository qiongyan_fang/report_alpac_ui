[#if content.images?has_content]
    [#if cmsfn.children(content.images, 'mgnl:contentNode')?size <= 1]
        [#list cmsfn.children(content.images, 'mgnl:contentNode') as item ] 
            <figure>
                <div class="treeWrap ${item.imageClass!'treWrap'}">
                [#if item.image?has_content]
                        [#assign image = damfn.getAsset(item.image)]
                        [#assign imageLink = image.link!]
                        <img src="${imageLink!}">
                [/#if]
                
                </div>
                <figcaption> ${cmsfn.decode(item).caption!}</figcaption>
            </figure>
        [/#list]
    [#else]
     
                <div class="columns">
                    [#list cmsfn.children(content.images, 'mgnl:contentNode') as item ] 
                        <div class="column ">
                            <figure>
                                <div class="treeWrap ${item.imageClass!''}">
                                [#if item.image?has_content]
                                        [#assign image = damfn.getAsset(item.image)]
                                        [#assign imageLink = image.link!]
                                        <img src="${imageLink!}">
                                [/#if]
                                
                                </div>
                                <figcaption> ${cmsfn.decode(item).caption!}</figcaption>
                            </figure>
                        </div>
                    [/#list]
                </div>
           
    [/#if] 
[/#if]