[#if !(content.isTwoColumn!false)]
    [#if content.highlight?has_content || content.briefSummary?has_content]
        <h2 class="dashStat"><span>${cmsfn.decode(content).highlight!}</span>${cmsfn.decode(content).briefSummary!}</h2>
    [/#if]
    [#if content.detailedSummary?has_content]
        <h5>${cmsfn.decode(content).detailedSummary!}</h5>
    [/#if]
    ${cmsfn.decode(content).description!}
[#else]
    <div class="columns" style="margin-top: 1.75rem">
       
        <div class="column is-one-third">
            <h2 class="dashStat"><span>${content.highlight!}</span>${cmsfn.decode(content).briefSummary!}</h2>
        </div>
        <div class="column is-two-thirds" style="border-left: 1px solid #9b9ba2;   padding: 0 1.75rem;">                        
            [#if content.detailedSummary?has_content]
                <h4>${cmsfn.decode(content).detailedSummary!}</h4>
            [/#if]
            ${cmsfn.decode(content).description!}            
        </div>
    </div>
[/#if]