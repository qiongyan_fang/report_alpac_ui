<iframe width="${content.width!'100%'}" height="${content.height!'315'}" 
src="${content.url!}" 
frameborder="0" 
allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
allowfullscreen></iframe>