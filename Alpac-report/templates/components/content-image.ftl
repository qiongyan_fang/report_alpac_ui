<figure>

  [#if content.image?has_content]
    <div class="centered">  
      [#assign image = damfn.getAsset(content.image)!]    
      [#if image??]
        [#assign imageLink = image.link!'']
        [#if content.largeImage?has_content]
          [#assign largeImage = damfn.getAsset(content.largeImage)!]   
          [#assign largeImageLink = largeImage.link]   
          <a href="#" data-featherlight="${largeImageLink!}">
        [/#if]      		
        <img
          src="${imageLink!}"
          class="mg-responsive"
          alt="${content.caption!'image'}">
          [#if content.creditText?has_content]
            <span class="photo-credit ${content.creditColor!''} ${content.creditPosition!''} ${content.creditSize!''}">${content.creditText!''}</span>
          [/#if]
        [#if largeImageLink?has_content]
          </a>
        [/#if]
      [/#if]
      </div>
  [/#if]
 
  [#if content.internalLink?has_content]
    [#assign target = cmsfn.contentById(content.internalLink, "website")!]
    <a href='${ctx.contextPath}/${cmsfn.link(target)!}' class='btn btn-link'>${target.title!target.@name}</a>
  [/#if]

  [#if content.caption?has_content]
    <figcaption>
      ${cmsfn.decode(content).caption}
    </figcaption>
  [/#if]
</figure>
