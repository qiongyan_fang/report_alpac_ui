<!-- Header - Home -->
<section class="pageHead home section">

	<div id="reporting">
		<p><a href="https://www.abmi.ca">Reporting By <img src="${ctx.contextPath}/.resources/Alpac-report/webresources/img/abmi-logo.svg"></a></p>
	</div>

	[#if content.image?has_content]   
    [#assign image = damfn.getAsset(content.image)]
    [#assign imageLink = image.link]
    <div class="headBg" style="background-image:url('${imageLink!}');" >
      [#if content.creditText?has_content]
        <span class="photo-credit ${content.creditColor!''} ${content.creditPosition!''} ${content.creditSize!''}">${content.creditText!''}</span>
      [/#if]
    </div>
  [/#if]
	

	<div class="headContent">
		<div class="contentWrap container">
    [#if content.header?has_content]
      <h1 class="${content.headerFont!}">
      	[#if content.subHeader?has_content]
          <span class="${content.subHeader!}">${cmsfn.decode(content).subHeader!}</span>
        [/#if]
      ${cmsfn.decode(content).header!}</h1>
    [/#if]
  
    [#if content.description?has_content]
    <div class="desc">${cmsfn.decode(content).description!}</div>
    [/#if]
		</div>
	</div>

	<div class="headSvg"></div>

</section>
<!-- Header - Home -->