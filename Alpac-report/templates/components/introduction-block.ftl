[#if content.hasBorder!false]
   [#if !(content.isStandAlone!false)]
      <section>
   [/#if]
   <div id="${content.anchorId!}" class="container overflow boxed padded60 moveBack">
      <div class="bordered"></div>
      <div class="container ${(content.isNarrowWidth!false)?then('small','')} ${(content.isLeftAlign!false)?then('left-align', 'center-align')} ${content.cssClass!} ${content.additionalClass!?replace(',',' ')!''}">
         [@cms.area name="breadcrumb-content" /]
         [#if content.header?has_content]
            <h2>${cmsfn.decode(content).header!}</h2>
         [/#if]      
         [#if content.subHeader?has_content]
            <div class="lead">${cmsfn.decode(content).subHeader!}</div>
         [/#if]
         [#if content.description?has_content]
            <div>${cmsfn.decode(content).description!}</div>
         [/#if]
         [@cms.area name="block-content" /]
      </div>
   </div>
   [#if !(content.isStandAlone!false)]
      </section>
   [/#if]
[#else]
<section>
		<div id="${content.anchorId!}" class="container ${(content.isLeftAlign!false)?then('left-align', 'center-align')} ${content.cssClass!} ${content.additionalClass!?replace(',',' ')!''}">
         [@cms.area name="breadcrumb-content" /]
			[#if content.header?has_content]
            <h2>${cmsfn.decode(content).header!}</h2>
         [/#if]    
			[#if content.subHeader?has_content]
            <div class="lead">${cmsfn.decode(content).subHeader!}</div>
         [/#if]
          [#if content.description?has_content]
            <div>${cmsfn.decode(content).description!}</div>
         [/#if]
         [@cms.area name="block-content" /]
			
		</div>
	</section>
   [/#if]