
[#-- make each components in a table row, used in custom quote table --]
[#assign formType =cmsfn.parent(cmsfn.parent(content, "mgnl:component"), "mgnl:component").formType!'' ]
<tr>
    <th class="checkCell"></th>										
    <th class="service">Service</th>
    <th class="unit">Unit</th>
    [#if formType =='ARU']
        <th class="profit">0 - 20 Stations</th>
        <th class="nonProfit">21 - 40 Stations</th>
        <th class="nonProfit">41 - 60 Stations</th>
        <th class="nonProfit">61 - 80 Stations</th>
        <th class="nonProfit">80+ Stations</th>
    [#else]
        <th class="profit">0 - 25 Stations</th>
        <th class="nonProfit">26 - 50 Stations</th>
        <th class="nonProfit">51 - 100 Stations</th>
        <th class="nonProfit">101 - 150 Stations</th>
        <th class="nonProfit">150+ Stations</th>
    [/#if]
</tr>
[#list components as component]    
    [@cms.component content=component contextAttributes={"formType":formType} /]
[/#list]