[#if cmsfn.editMode]
    [#list components as component]
        <div style="border: yellow solid 20px; margin-bottom: 20px;">
            [@cms.component content=component /]
        </div>
    [/#list]
[#else]
    <div>
     <!-- important, use api.mapbox.com link library. api.tile.mapbox doesn't work, has bugs on move layers -->
        <script src='https://api.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.js'></script>
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>    
        <script src='https://npmcdn.com/@turf/turf/turf.min.js'></script>
        <link href='https://api.mapbox.com/mapbox-gl-js/v0.52.0/mapbox-gl.css' rel='stylesheet' />
        <ul id="ul${cmsfn.asJCRNode(content).getUUID()!}" class="abmi context-map-tab custom-tabList${cmsfn.editMode?then('edit','')}">
            [#list components as component]           
                <li><a href="#tab-${cmsfn.asJCRNode(component).getUUID()!}" data-map-id="${component.mapId!}">${component.name!}</a></li>
            [/#list]	    
        </ul>
        [#list components as component]
            [#if navfn.hasTemplate(component, "Alpac-report:components/tab-context-map-item")]                  
                <div class="tab-content tab-${cmsfn.asJCRNode(component).getUUID()!}">
                    <div> ${component.introduction!}
                    </div>
                </div>                    
            [/#if]    
        [/#list]

        [#assign tabSize = components?size ]
        <div class="columns">
            <div class="column is-one-half-desktop">
                [#list components as component]
                    [#if navfn.hasTemplate(component, "Alpac-report:components/tab-context-map-item")] 
                        [#if component?index == 0 ]    
                            [#assign firstMap = component.mapId!'']
                        [/#if]             
                        <div class="tab-${cmsfn.asJCRNode(component).getUUID()!}">
                            [@cms.component content=component /]        
                        </div>                    
                    [/#if]    
                [/#list]
            </div>
            <div class="column is-one-half-desktop">
                <div id="map${cmsfn.asJCRNode(content).getUUID()}_container" style="position:relative">
                    <div id="map${cmsfn.asJCRNode(content).getUUID()}" class="hf-map">
                        <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>                
                    </div>    
                    <div id="hf-legend" class="compress"><span class="icon"></span><Strong>Legend</Strong>
                        <div class="legend-container">
                            <div class="toggle-legend arrow-down"></div>
                            <div class="normal"></div>
                            <div class="aoi" ></div>
                        </div>
                    </div>
                </div>
                [#list components as component]
                    [#if navfn.hasTemplate(component, "Alpac-report:components/tab-context-map-item")]                  
                       <figure class="tab-content tab-${cmsfn.asJCRNode(component).getUUID()!}">
                            <figcaption> ${cmsfn.decode(component).mapTitle!}</figcaption>
                        </figure>             
                    [/#if]    
                [/#list] 
                
            </div>
        </div>   
        [#list components as component]
            [#if navfn.hasTemplate(component, "Alpac-report:components/tab-context-map-item")]                  
                <div class="tab-content tab-${cmsfn.asJCRNode(component).getUUID()!}">
                    <div> ${component.bottomTable!}
                    </div>
                </div>                    
            [/#if]    
        [/#list]    
    </div>
    <script>
        $( document ).ready(async function() {	
           
             const layerList = {
                boundary: {layers: ['7609', '7790']}, // alpac + EIA
                ecosystem: {layers: ['24', '36', '38', '39', '40', '42', '43', '25', '44', '26', '45'], aoi: ['7609', '7790'], mask: '7609' },
                wetland: {layers: ['3662', '3661', '3659', '3660', '3663', '3658'], aoi: ['7609', '7790'], mask: '7609'},
                file_history: {layers: ['7772'], aoi: ['7609', '7790'], mask: '7609'},
                parks: { layers: ['203', '202'], aoi: ['7609', '7790'], mask: '7609'},
                'Harvest Areas Analyzed': { layers: [7791], aoi: 'default', mask: 'default', zoom: '7790'},
                'Detected Year of Harvest': { layers: [7792], aoi: 'default', mask: 'default'},
                '5-year Post-harvest Spectral Regeneration': { layers: [7793], aoi: 'default', mask: 'default'},
                'Years to Reach 80% Spectral Regeneration': { layers: [7794], aoi: 'default', mask: 'default'},
                'Current Levels of Regeneration': { layers: [7795], aoi: 'default', mask: 'default'},         
            };
            const defaultSetting = {
                aoi: ['7609' , '7790'],
                mask: '7609'
            };           
            Object.keys(layerList).forEach( layerId => {   
                let currentLayers = layerList[layerId];
                currentLayers.layers  = currentLayers.layers.map (x => x + '');
                if (currentLayers.aoi === 'default') {
                    currentLayers.aoi = defaultSetting.aoi;
                }
                if (currentLayers.mask === 'default') {
                    currentLayers.mask = defaultSetting.mask;
                }            
            })
            const mapDivId = 'map${cmsfn.asJCRNode(content).getUUID()!}';
            const containerDivId = mapDivId + '_container';
            const defaultMapId ="${firstMap!'boundary'}";
            data = await initMap(mapDivId, layerList[defaultMapId] || {});
                    
            /* click tab to switch map layers */            
           $('#ul${cmsfn.asJCRNode(content).getUUID()!}.context-map-tab a').click((event)=>{
               event.preventDefault();
               if ($(event.target).hasClass('is-active')) {
                   return; // do nothing when it is already active.
               }
                const mapName = $(event.target).data('map-id');
                if (mapName && layerList[mapName]) {                                    
                        switchLayers(containerDivId, layerList[mapName], data.map, data.mapLayers, data.headers);
                    
                } else {
                    alert('No map information provided');
                }
                /* toggle content */
                
               $('#ul${cmsfn.asJCRNode(content).getUUID()!}.context-map-tab a').each((index, button) => {
                    $('.' + $(button).attr('href').substring(1)).hide();
                    $(button).removeClass('is-active');
                })
                $('.' + $(event.target).attr('href').substring(1)).show();
                $(event.target).addClass('is-active');               
            }); 
            
            const firstButton = $('#ul${cmsfn.asJCRNode(content).getUUID()!}.context-map-tab a')[0];
            $('#ul${cmsfn.asJCRNode(content).getUUID()!}.context-map-tab a').each((index, button) => {
                $('.' + $(button).attr('href').substring(1)).hide();
                $(button).removeClass('is-active');
            })
            $('.' + $(firstButton).attr('href').substring(1)).show();
            $(firstButton).addClass('is-active');      
          
           
        });
    </script>
[/#if]