[#if components?size > 1]
    [#assign isCenterAligned =cmsfn.parent(content, "mgnl:component").isCenterAligned!false ]
    <ul class="content tabList${cmsfn.editMode?then('edit','')} ${isCenterAligned?then('center-align','')}">
        [#list components as component]           
            <li class="content tab" id="${component.anchorId!}"><a class="${(component?index == 0)?then('active', '')}" 
            href="#tab-${cmsfn.asJCRNode(component).getUUID()!}"><span class="${component.predefinedIcon!component.customIcon!''}"></span> ${component.title!}</a></li>
        [/#list]
	    <li class="indicator" style="left: 0px; right: 1448px;"></li>
    </ul>
[/#if]

[#assign tabSize = components?size ]
[#list components as component]    
    [#if cmsfn.isEditMode()]
        <div style="border: yellow solid 20px;">
    [/#if]
    <div id="tab-${cmsfn.asJCRNode(component).getUUID()!}" 
    class="tab ${(tabSize > 1)?then('section-tab tabContent','')} ${cmsfn.editMode?then('edit','')}"
     [#if component?index > 0 && cmsfn.isPreviewMode()]  
      style="display:none;"
     [/#if]
    >
        [@cms.component content=component /]        
    </div>
    [#if cmsfn.isEditMode()]
        </div>
    [/#if]
[/#list]
