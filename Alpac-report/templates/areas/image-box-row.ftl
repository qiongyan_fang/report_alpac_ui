[#assign imageBoxType =cmsfn.parent(content, "mgnl:component").imageBoxType!'' ]

[#list components as component]    
    [@cms.component content=component contextAttributes={"imageBoxType":imageBoxType} /]    
[/#list]


