
function commonDataTable(divId, dataDef, speciesListUrl, styles) {
    var ServletPath = 'http://abmi.ca/FileDownloadServlet?dir=WEB_GRAPH&filename=';
    var gDetailedUrlParam = 'http://abmi.ca/home/data-analytics/biobrowser-home/species-profile';
    var speciesTable = $(divId)
        .on(
            'preXhr.dt',
            function (e, settings, data) {
                for (var i = 0; i < data.columns.length; i++) {
                    column = data.columns[i];
                    column.searchRegex = column.search.regex;
                    column.searchValue = column.search.value;
                    delete (column.search);
                }

            })
        .DataTable(
            {
                processing: true,

                serverSide: true,
                scroller: {

                    loadingIndicator: true

                },

                scrollY: 700,
                searching: false,
                deferRender: true, // render node
                // only when
                // needed
                paging: false,


                ajax: speciesListUrl ,
                columns: dataDef,

                bAutoWidth: false,

                language: {

                    infoEmpty: 'No data available for this filter selection; please select another filter option.',
                    zeroRecords: 'No data available for this filter selection; please select another filter option.',
                    info: 'Showing _START_ - _END_ of _TOTAL_ species'

                },
                'createdRow': function (row, data,
                    index) {
                    if (data.image) {
                        $('td:eq(0)', row)
                            .html(
                                '<div ><img class="circle-img" alt="" src="' +
                                ServletPath + data.image
                                + '" onerror ="this.src=\'/.resources/Alpac-report/webresources/img/species-list-standard.jpg\'"></div>');
                    } else { // use default if no
                        // image is
                        // provided.
                        $('td:eq(0)', row)
                            .html(
                                '<div><img alt="" class="circle-img" src="/.resources/Alpac-report/webresources/img/species-list-standard.jpg"></div>');
                    }

                    // add link and pointer
                    $(row).css('cursor', 'pointer');
                    styles.forEach(function(item, index) {
                        $('td:eq(' + index +')', row).addClass(item);
                      });                 
                    $(row).unbind().bind('click',
                        function () {
                            window.open(gDetailedUrlParam + '?tsn=' + data.tsn);

                        });

                }

            });


    /* hide header, but somethinge is there */
    $(divId).on(
        'draw.dt',
        function () {
            $('.dataTables_scrollBody thead tr')
                .addClass('hidden');

        });

    $(window).resize(function () {
        speciesTable.draw('page'); // updated fro 1.10.8
    });

    return speciesTable;
}
function createNNSpeciesTable(divId, version) {

    var gIsInitLoad = true; // update species count at initial    
    var speciesListUrl = API_ROOT + 'report/getNNPSppList' + '?version=' + version;


    // names are case sensitive
    var dataDef = [{
        'data':  null,
        'orderable': false,
        'searchable': false
    }, {
        'data': 'common_name',
        'searchable': true
    }, {
        'data': 'scientific_name',
        'searchable': true
    },
    {
        'data': 'occurent_percent',
        'visible': true,
        'orderable': true
    },
    {
        'data': 'status',
        'visible': true,
        'orderable': true
    },
    {
        'data': 'tsn',
        'visible': false,
        'searchable': false
    }
    ];

    var styles = ['datatableimg img_col img',
    'cname' , 'italic sname', 'percentBod status center','percentBod value center'];
    var speciesTable = commonDataTable(divId, dataDef, speciesListUrl, styles);
    speciesTable.order([3, 'desc']).draw(); // sort by occurrence percentage
}

/**-------------create Species at Risk Table -----------------------*/
function createSpeciesAtRiskTable(divId) {
    
    var speciesListUrl = API_ROOT + 'report/getSpeciesAtRiskList';
    
    var dataDef = [{
        'data': null,
        'orderable': false,
        'searchable': false
    }, {
        'data': 'spp_group',
        'searchable': false,
        'orderable': true
    }, {
        'data': 'common_name',
        'searchable': true,
        'orderable': true
    }, {
        'data': 'scientific_name',
        'searchable': true,
        'orderable': true
    },

    {
        'data': 'occurent_percent',
        'visible': true,
        'orderable': true
    },

    {
        'data': 'intactness',
        'visible': true,
        'orderable': true
    },

    {
        'data': 'reference',
        'visible': true,
        'orderable': true
    },

    {
        'data': 'status',
        'visible': true,
        'searchable': true
    },
    {
        'data': 'tsn',
        'visible': false,
        'searchable': false
    }
    ];

    var styles = ['datatableimg img_col img', 'sppgroup center',
    'cname' , 'italic sname', 'percentBod occurrence center','percentBod intactness center', 'center reference', 'status'];

    var speciesTable = commonDataTable(divId, dataDef, speciesListUrl, styles);
    speciesTable.order([1, 'asc'], [5, 'asc']).draw();

}

function createCultureImportantSpeciesTable(divId, version) {

    var gIsInitLoad = true; // update species count at initial    
    var speciesListUrl = API_ROOT + 'report/getCultureImportantSppList' + '?version=' + version;


    // names are case sensitive
    var dataDef = [{
        'data': null,
        'orderable': false,
        'searchable': false
    }, {
        'data': 'spp_group',
        'searchable': false,
        'orderable': true
    }, {
        'data': 'common_name',
        'searchable': true
    }, {
        'data': 'scientific_name',
        'searchable': true
    },
    {
        'data': 'occurent_percent',
        'visible': true,
        'orderable': true
    },
    {
        'data': 'intactness',
        'visible': true,
        'orderable': true
    },
    {
        'data': 'reference',
        'visible': true,
        'orderable': true
    },
    {
        'data': 'status',
        'visible': true,
        'orderable': true
    },
    
    {
        'data': 'upland_forest',
        'visible': true,
        'orderable': true
    },
    {
        'data': 'tsn',
        'visible': false,
        'searchable': false
    }
    ];

    var styles = ['datatableimg img_col img', '',
    'cname' , 'italic sname', 'percentBod status center','percentBod value center'];
    var speciesTable = commonDataTable(divId, dataDef, speciesListUrl, styles);
    speciesTable.order([[1, 'asc'], [5, 'desc']]).draw(); // sort by occurrence percentage
}