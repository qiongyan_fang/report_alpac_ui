	/* custom */
    const API_ROOT = 'https://dev-api-alpac.abmi.ca/';		 
    // const API_ROOT = 'http://localhost:8089/';
    // find references and save it to webpage. all reference are <sup> tag
    $(document).ready(function(){
        if ($('sup').length){
            $.each($('sup'), function(indx, item){
                if ($(this).text().indexOf('[') != -1){					
                    const idString = $(this).text().replace(/[\[\]]/g, '');
                    const idList = idString.split(',');
                    const tab = $(this).parents('.section-tab.tabContent');
                
                    let toastContent ='<h3>Reference</h3>';
                    idList.forEach( function(id) {
                        const refId = id.trim();
                        // get tab id if available
                        let refObj;
                        if (tab.length) {
                            refObj = tab.find('#reference' + refId);
                        } else {
                            refObj = $('#reference' + refId);
                        }
                    
                        if (refObj.length) {
                            toastContent += '<div>' + refObj.html() + '</div>' ;                   
                            
                        }
                    });
                    $(this).wrap( '<a class="tooltip"></a>' );
                    $(this).append('<span class="tooltiptext reference-item">' + toastContent.replace(/\"/g, '\'') + '</span>');
                }
            });
            
        }

        if ($('.showMore').length) {
            showReadMore('.showMore', 300);
        } 

        $(".tabList a").click(function(event) {
            event.preventDefault();
            const tabDetailAreaId = $(event.target).attr('href') + '-simple-tabs';

            $(tabDetailAreaId + ' #resize-map').click();


        });        
    });

    function getDynHtml(htmStr, values) {
       
        // Compile the template data into a function  
        var templateScript = Handlebars.compile(htmStr);        
        // Pass Data to template script.  
        var html = templateScript(values);  
        // Insert the HTML code into the page  

        return html;
    }

    if (!Array.prototype.last){
        Array.prototype.last = function(){
            return this[this.length - 1];
        };
    };