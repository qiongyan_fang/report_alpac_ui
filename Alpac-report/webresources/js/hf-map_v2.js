function generateConfig(layer, config) {
    let {id, settings, geom} = layer;

    if (!config) {

      try {
        geom = JSON.parse(geom);
      } catch (e) {
      }

      config = {
        'id': id + '',
        'type': 'fill',
        'source': {
          'type': 'geojson',
          'data': {
            'type': 'Feature',
            'geometry': geom
          }
        },
        'layout': {},
        'paint': {
          'fill-color': '#088',
          'fill-opacity': 0.8,
          'fill-outline-color': 'rgba(169,169,169,1)'
        }
      };
    }

    Object.keys(settings || {})
      .forEach(key => {
        switch(key) {
          case 'color':
            config.paint['fill-color'] = '#' + settings[key];
            break;
          case 'opacity':
          case 'fill_opacity':
            config.paint['fill-opacity'] = (parseInt(settings[key]) / 100);
            break;
          default:
            return null;
        }
      });

    Object.keys(settings.appearance || {})
      .forEach(key => {
        const enabled = settings.appearance[key];
        switch(key) {
          case 'fill':
            if (!enabled) {
              config.paint['fill-opacity'] = 0; // to hide fill
            }
            break;
          case 'border':
            config.paint['fill-outline-color'] = enabled ? 'rgba(169,169,169,0)' : 'rgba(169,169,169,1)';
            break;
          default:
            return null;
        }
      });

    return config;
}

function drawMask(map, layers) {    
    console.log('draw mask', layers);
    return new Promise((resolve) => {
        
        let geoms = Object.values(layers).map(layer => ({ type: 'Feature', geometry: layer.geom }));

        let combined = turf.union(...geoms);
        const world = turf.bboxPolygon([-180, -90, 180, 90]);
        const diff = turf.difference(world, combined);

        let config = {
            'id': 'world',
            'source': {
                'type': 'geojson',
                'data': diff
            },
            'type': 'fill',
            'visibility': 'visible',
            'paint': {
                'fill-color': 'white',
                'fill-opacity': 0.9               
            }
        };

        map.addLayer(config);
    })
}

// function switchToLayer(divId, map, layerIds, id) {
//     $('#' + divId + ' .lds-spinner').show();
//     layerIds.forEach(x => {
//         map.setLayoutProperty(x, 'visibility', 'none');
//     });
    
//      map.setLayoutProperty(id, 'visibility', 'visible');
// }
function removeLayer(divId, map, id) {
    
    changeLayerVisibility(divId, map, id, false);
}

function showLayer(divId, map, id) {    
    changeLayerVisibility(divId, map, id, true);
}

function changeLayerVisibility(divId, map, id, toVisible) {
    const visibility = map.getLayoutProperty(id, 'visibility'); // initial is is undefined
    if (visibility) {
        if (visibility === 'visible' && toVisible == false) {
            $('#' + divId + ' .lds-spinner').show();
            map.setLayoutProperty(id, 'visibility', 'none');        
        } else if (visibility !== 'visible' && toVisible == true) {        
            $('#' + divId + ' .lds-spinner').show();
            map.setLayoutProperty(id, 'visibility', 'visible');
        }
    } else { // initally visibility is undefined, so can't use that to check if layer is visible or not, just follow the instruction to hide/show it.
        $('#' + divId + ' .lds-spinner').show();
        map.setLayoutProperty(id, 'visibility', (toVisible ? 'visible': 'none'));
    }
}
function makeStepSegments(fillColorArr)  {
    let segments = [];
    /* skip first two elements since they describe the type of expression and its input
    * while the rest are pairs of a color and a respective step threshold
    * https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step */
    for (let i = 2; i < fillColorArr.length; i += 2) {
        let color = fillColorArr[i];
        let prevStep = fillColorArr[i - 1];
        let nextStep = fillColorArr[i + 1];

        let min = prevStep;
        if (i === 2 && fillColorArr.length > 5) {
            let increment = fillColorArr[i + 3] - nextStep;
            min = (nextStep - 1) - increment;
        }

        let max = nextStep - 1;
        if (i === fillColorArr.length - 1) {
            let increment = prevStep - fillColorArr[i - 3];
            max = (prevStep - 1) + increment;
        }

        segments.push({ color, min, max });
    }

    return segments;
};

function makeMatchSegments (fillColorArr)  {
    let segments = [];
    /* skip first two elements since they describe the type of expression and its input
    * skip the last element since it's just a fallback color
    * https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-match */
    for (let i = 2; i < fillColorArr.length - 1; i += 2) {
        let color = fillColorArr[i + 1];
        let category = fillColorArr[i];

        segments.push({ category, color });
    }

    return segments;
}

function createLegend(divId, map, name, id, fill) {
    
    if (Array.isArray(fill)) {
      //   $('#' + divId + ' #hf-legend .legend-container').append('<div class="legends" id="hf-legend' + id + '"><h5>' + name + '</h5></div></div>'); //$('#hf-legend').append('<div class="legend-container"><div class="toggle-legend arrow-down"></div><div class="legends" id="hf-legend' + id + '"><h5>' + name + '</h5><button id="hf-hide' + id + '">Hide</button><button id="hf-show' + id + '">Show</button></div></div>');
   
        
        if(fill[0] == 'match') {                   

            for (let i = 2; i < fill.length - 1; i += 2) {

                $('#' + divId + ' #hf-legend' + id).append('<div class="legend-row"> <div class="legend" style="background-color:' + fill[i + 1] + ';" ></div><span>' + fill[i] + '</div></div>')
            }
        } else if (fill[0] == 'step') {
            const segment = makeStepSegments (fill);
            
            for (let i = 0; i < segment.length ; i ++) {
                $('#' + divId + ' #hf-legend' + id).append('<div class="legend-row"> <div class="legend" style="background-color:' + segment[i].color + ';" ></div><span>' + segment[i].min + '-' + segment[i].max + '</div></div>')
            }
        } else if (fill[0].type == 'fill') {// multiple layer
            if ($('#legendRow'+ id).length > 0) {
                $("#legendRow" + id ).append('<div class="legend" style="background-color:' + fill + ';" ></div><span>' + name + '</span>');
            } else {    
                $('#' + divId + ' #hf-legend .legend-container').append('<div><div class="legend-row1" id="legendRow' + id + '"> </div></div>');
            }

            for (let i = 0; i < fill.length - 1; i ++) {
               
                $('#' + divId + " #legendRow" + id ).append('<div><div class="legend" style="background-color:' + fill[i].paint['fill-color'] + ';" ></div><span>' + fill[i].id + '</span></div>');
            }
        }

    } else {
        if ($('#legendRow'+ id).length == 0) {          
            $('#' + divId + ' #hf-legend .legend-container').append('<div><div class="legend-row" id="legendRow' + id + '"> <label class="checkbox-label"><input type="checkbox" id="ch_legend' + id + '" checked><span class="checkbox-custom rectangular"></span></label></div></div>');
        }
        if (typeof fill == 'string') {
            $('#' + divId + ' #legendRow' + id ).append('<div class="legend" style="background-color:' + fill + ';" ></div><span>' + name + '</span>')
        } else {
            let opacity = '';
            if (fill.opacity) {
                opacity = ';opacity: ' + (fill.opacity/100);
            }
            $('#' + divId + ' #legendRow' + id ).append('<div class="legend" style="background-color:#' + fill.color + opacity + ';" ></div><span>' + name + '</span>');
        }      

        
    }

  
    $('#' + divId + ' #ch_legend' + id).change(function () {
        if ($(this).is(":checked")) {
            showLayer(divId, map, id);
        } else {
            removeLayer(divId, map, id);
        }
    })
}

function loadLayerFromUrl(divId, map, headers,  mapLayers, id, base, isMask) {
    const datasetPrefix = 'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/';
    const datasetSuffix = '?type=' + (base?'aoi': 'dataset') + '&gridId=1&id=';   
    $('#' + divId + ' .lds-spinner').show();
    return new Promise((resolve, reject) => {
        let mapLayer = {};
        const url = datasetPrefix + id + datasetSuffix + id;
        axios.get(url, { headers }).then(function (response) {
            // handle success        
            const baseLayer = response.data.data;
            let layersToAdd = [];
            // map.on('load', function () {                
            mapLayer.full = baseLayer;
            if (base) {
                mapLayer.type = 'aoi';
                const newLayer = generateConfig(baseLayer);
                layersToAdd.push(newLayer.id);
                mapLayer.layer = newLayer;
                const layerIds = Object.keys(mapLayers);
                mapLayers[newLayer.id] = { id: newLayer.id, type: 'aoi', layer: [baseLayer] };
                console.log('current layers ', layerIds);
                createLegend(divId, map, baseLayer.name, baseLayer.id, baseLayer.settings);
                if (layerIds.length > 0) {
                    map.addLayer(newLayer, layerIds[layerIds.length-1]);
                } else {
                    console.log();
                    map.addLayer(newLayer);
                }
                if (isMask) {                         
                    drawMask(map, [baseLayer], layerIds[layerIds.length-1]);
                    zoomToLayer(map, [baseLayer]);  
                }
                
            
            } else {
                mapLayer.type = 'data';
                let layer = baseLayer.style.layers[0];
                let source = baseLayer.style.sources.dataset
                mapLayer.layer = layer;
                mapLayer.source = source;
                mapLayer.id = baseLayer.id + '';
                layer.source = baseLayer.id + '';
                 
            
                if ( baseLayer.style.layers.length <= 1) {
                    map.addSource(baseLayer.id, source);
                    map.addLayer(layer);
                    mapLayers[layer.source] = { source: source, layer: layer };        
                    createLegend(divId, map, baseLayer.name, layer.source, layer['paint']['fill-color']);
                    layersToAdd.push(baseLayer.id);
                } else { // multiple layers
                    if (baseLayer.style.layers) {
                        createLegend(divId, map, baseLayer.name, layer.source, baseLayer.style.layers);
                    }
                 
                    map.addSource(baseLayer.id, source);
                    baseLayer.style.layers.forEach( x => {
                        x.source = baseLayer.id + '';  
                        map.addLayer(x);  
                        mapLayers[x.id] = { source: x.source, layer: x }; 
                        layersToAdd.push(x.id); 
                    });            
                    mapLayers[baseLayer.id + ''] = { linkedLayer: layersToAdd }; 
                }
            }
            let loadTasks = [];
            layersToAdd.forEach(x => { console.log('checking ',x); loadTasks.push(checkSourceLoaded (x, map)); })
            Promise.all(loadTasks).then( x=>{console.log('check done',x);  resolve(x)} );
        })
        .catch(function (error) {
            // handle error
            console.log(error);
           
            reject(error);
           
        })
    });
    
}

function zoomToLayer(map, layers) {
    return new Promise((resolve) => {
        
        let geoms = Object.values(layers).map(layer => ({ type: 'Feature', geometry: layer.geom }));
        
        const bbox = turf.bbox(geoms[0]);
        
        map.fitBounds(bbox);

    })
}

function checkSourceLoaded (id, map) {
    return new Promise((resolve, reject) => {
      let time = 0;
      let timer = setInterval(() => {
        try {
          if (map.getSource(id) && map.isSourceLoaded(id)) {
            clearInterval(timer)
            resolve(id)
          }

          time += 500;

          // timeout
          if (!map.getSource(id) || time > 30000) {
            clearInterval(timer)
            resolve(id);
          }
        } catch (e) {
          clearInterval(timer)
          reject(e)
        }
      }, 500)
    })
  }

  function createLegendRow(divId, layerIds) {
     if (layerIds.length  == 0 ) {
         return;
     }
     layerIds.forEach( hfLayer => {        //<div class="legend" style="background-color:' + fill + ';" ></div><span>' + name + '           
        $('#' + divId + ' #hf-legend .legend-container').append(
            getDynHtml(
            '<div><div class="legend-row" id="legendRow{{hfLayer}}"> <label class="checkbox-label"><input type="checkbox" id="ch_legend{{hfLayer}}"' +
            ' checked><span class="checkbox-custom rectangular"></span></label></div></div>', {hfLayer}));
    });    
}

function initMap (mapDivId, layersToLoad) {	 
    let mapLayers = {};     
    const containerDivId = mapDivId + '_container'    ;
    const bounds = [
        [-130, 40], // Southwest coordinates
        [-90, 70]  // Northeast coordinates
       ];

    let headers = {};
    headers['x-api-key'] = 'NSgyFqN2n31UQPd3uLA8o2w0A7TJ0Gk29JPVK041';

    mapboxgl.accessToken = 'pk.eyJ1IjoicWZhbmciLCJhIjoiY2p5NjJ3OHNqMGUydDNibXpvZzdodzNsaCJ9.HAK1XLZht400oO5Q_19LsQ';
     /* add legend expand compress action */
     $('#' + containerDivId + '#hf-legend .icon').click(() => {
        const legendObj = $('#' + containerDivId + ' #hf-legend');
        legendObj.toggleClass( 'compress', !legendObj.hasClass('compress') );
        legendObj.toggleClass( 'expand', !legendObj.hasClass('expand') );
    });

    return new Promise((resolve) => {
        let map = new mapboxgl.Map({
            container: mapDivId,
            // style: 'mapbox://styles/mapbox/light-v10',
            zoom: 6,
            
            center: [-114.6309516, 56.6293613],
            maxBounds: bounds,
            // style: 'mapbox://styles/mapbox/satellite-v9'
        });
        map.addControl(new mapboxgl.FullscreenControl());
        map.addControl(new mapboxgl.NavigationControl());
        
        map.on('data', function (data) {  
            if (data.dataType === 'source' && data.isSourceLoaded) {        
                    $('#' + mapDivId + ' .lds-spinner').hide();
            }
        })
        
        
        const topographUrl = 'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3601?type=basemap&id=3601&grid_id=1';
       
        axios.get(topographUrl, { headers }).then(function (styleResponse) {
            const style = styleResponse.data.data.style;
            map.setStyle(style);
            
            let loadingLayerTasks = [];
            if (layersToLoad.layers && layersToLoad.layers.length  > 0 ) {
             
                createLegendRow(containerDivId, layersToLoad.layers);
                layersToLoad.layers.forEach((id) => loadingLayerTasks.push(loadLayerFromUrl(containerDivId, map,  headers, mapLayers, id))); 
                Promise.all(loadingLayerTasks).then(()	=> {					
                    layersToLoad.aoi.forEach((id) => loadLayerFromUrl(containerDivId, map,  headers, mapLayers, id, true, id ==layersToLoad.mask));   
                    resolve({mapLayers, map});                   
                });	 
            } else {
                if (layersToLoad.aoi && layersToLoad.aoi.length > 0) {
                    layersToLoad.aoi.forEach((id) => loadLayerFromUrl(containerDivId, map,  headers, mapLayers, id, true, id == layersToLoad.mask)); 
                }  
                resolve({mapLayers, map, headers});                    
            }
        }); 
    });   
}

function switchLayers(mapDivId, newLayers, map,  mapLayers, headers) { 
    // hide current layers.
    const divId = mapDivId + '_container';
    const allNewLayers = (newLayers.layers || []).concat(newLayers.aoi || []);
    // hide existing layer not to be used
    console.log(mapLayers);
    Object.keys(mapLayers).forEach((layerId) => {
        if (allNewLayers && !allNewLayers.includes(layerId)) {
            if (mapLayers[layerId].layer ) {
                removeLayer(divId, map, layerId);
            }
            
            $('#' + divId + ' #legendRow' + layerId ).hide();
        }       
    });
    // first check if layer is already loaded, if so, skip them
    let loadingLayerTasks = [];
    if (newLayers.layers && newLayers.layers.length > 0) {

        newLayers.layers.forEach(id => {
            if ( map.getSource(id) && map.isSourceLoaded(id)) {
            // already loaded, show layer, show legend
                if (mapLayers[id].linkedLayer) {
                    mapLayers[id].linkedLayer.forEach(x=> {
                        showLayer(divId , map, x);
                        $('#' + divId + ' #legendRow' + x ).show();
                    });
                } else {
                    showLayer(divId , map, id);
                    $('#' + divId + ' #legendRow' + id ).show();
                }
                
            } else {
                createLegendRow(divId, [id])
                loadingLayerTasks.push( loadLayerFromUrl(divId, map,  headers, mapLayers, id));
            }

        });
    }

    if (newLayers.aoi && newLayers.aoi.length > 0) {       
        Promise.all(loadingLayerTasks).then( (layersLoaded)	=> {	
            console.log(layersLoaded);
            let loadingAOITask = [];			              ;
            newLayers.aoi.forEach((id) => {
                if (map.getSource(id) && map.isSourceLoaded(id)) {
                    // already loaded, show layer, show legend
                        if (mapLayers[id].linkedLayer) {
                            mapLayers[id].linkedLayer.forEach(x=> {
                                showLayer(divId , map, x);
                                $('#' + divId + ' #legendRow' + x ).show();
                            });
                        } else {
                            showLayer(divId , map, id);
                            $('#' + divId + ' #legendRow' + id ).show();
                        }
                  
                } else {
                    createLegendRow(divId, [id]);
                    loadingAOITask.push(loadLayerFromUrl(divId, map,  headers, mapLayers, id, true, id == newLayers.mask));
                }
                // move AOI to top
                if ( newLayers.layers && newLayers.layers.length > 0) {
                    map.moveLayer(id + '',  map.getStyle().layers.last().id + '');
                }
            });
            // if no map yet 
            console.log(loadingAOITask);
            Promise.all(loadingAOITask).then((x) => {
                console.log(loadingAOITask, x);
                if (newLayers.mask) {
                    if (map.getSource('world')) {
                        console.log(' move layer to front', layersLoaded.last(), map.getStyle().layers.last().id);
                        map.moveLayer('world', (layersLoaded.last() ?   layersLoaded.last() : map.getStyle().layers.last().id) + '');                    
                    } else {
                         drawMask(map, mapLayers[newLayers.mask].layer);     
                    }          
                }
            });
            console.log(map, map.style._layers, map.getStyle().layers)              ;
        });	 
    }
    // if not loaded, load them
}

