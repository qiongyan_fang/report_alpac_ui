const AOIColor = {'7790aoi': '#000', '7609aoi': '#555'};
function generateAOIConfig(layer, borderColor) {
    let {id, settings, geom} = layer;
    try {
    geom = JSON.parse(geom);
    } catch (e) {
    }

    const config = {
    'id': id + 'aoi',
    'type': 'line',
    'source': {
        'type': 'geojson',
        'data': {
        'type': 'Feature',
        'geometry': geom
        }
    },
    'layout': {},
    'paint': {
        'line-color': AOIColor[id + 'aoi'], //'#000', // + settings.color,
        'line-width': 3,
        'line-offset': 0

    //   'fill-color':  '#' + settings.color,
    // //   'fill-opacity': 0.1,
    //   'fill-opacity': 0.2,
    // //   'fill-outline-color': 'rgba(169,169,169,1)'
    //    'fill-outline-color': '#' + settings.color
    }
    };  
    

    return config;
}

function generateConfig(layer, config) {
    let {id, settings, geom} = layer;

    if (!config) {

      try {
        geom = JSON.parse(geom);
      } catch (e) {
      }

      config = {
        'id': id + '',
        'type': 'fill',
        'source': {
          'type': 'geojson',
          'data': {
            'type': 'Feature',
            'geometry': geom
          }
        },
        'layout': {},
        'paint': {
          'fill-color': '#088',
          'fill-opacity': 0.8,
          'fill-outline-color': 'rgba(169,169,169,1)'
        }
      };
    }

    Object.keys(settings || {})
      .forEach(key => {
        switch(key) {
          case 'color':
            config.paint['fill-color'] = '#' + settings[key];
            break;
          case 'opacity':
          case 'fill_opacity':
            config.paint['fill-opacity'] = (parseInt(settings[key]) / 100);
            break;
          default:
            return null;
        }
      });

    Object.keys(settings.appearance || {})
      .forEach(key => {
        const enabled = settings.appearance[key];
        switch(key) {
          case 'fill':
            if (!enabled) {
              config.paint['fill-opacity'] = 0; // to hide fill
            }
            break;
          case 'border':
            config.paint['fill-outline-color'] = enabled ? 'rgba(169,169,169,0)' : 'rgba(169,169,169,1)';
            break;
          default:
            return null;
        }
      });

    return config;
}

function drawMask(map, layers) {    
    console.log('draw mask', layers);
    return new Promise((resolve) => {
        
        let geoms = Object.values(layers).map(layer => ({ type: 'Feature', geometry: layer.geom }));

        let combined = turf.union(...geoms);
        const world = turf.bboxPolygon([-180, -90, 180, 90]);
        const diff = turf.difference(world, combined);

        let config = {
            'id': 'world',
            'source': {
                'type': 'geojson',
                'data': diff
            },
            'type': 'fill',
            'visibility': 'visible',
            'paint': {
                'fill-color': 'white',
                'fill-opacity': 0.9               
            }
        };

        map.addLayer(config);
    })
}

// function switchToLayer(containerDivId, map, layerIds, id) {
//     $('#' + containerDivId + ' .lds-spinner').show();
//     layerIds.forEach(x => {
//         map.setLayoutProperty(x, 'visibility', 'none');
//     });
    
//      map.setLayoutProperty(id, 'visibility', 'visible');
// }
function removeLayer(containerDivId, map, id) {
    
    changeLayerVisibility(containerDivId, map, id, false);
}

function showLayer(containerDivId, map, id) {    
    changeLayerVisibility(containerDivId, map, id, true);
}

function changeLayerVisibility(containerDivId, map, id, toVisible) {
    const visibility = map.getLayoutProperty(id, 'visibility'); // initial is is undefined
    $('#' + containerDivId + ' .lds-spinner').show();
    if (visibility) {
        if (visibility === 'visible' && toVisible == false) {
         
            map.setLayoutProperty(id, 'visibility', 'none');        
        } else if (visibility !== 'visible' && toVisible == true) {        
         
            map.setLayoutProperty(id, 'visibility', 'visible');
        }
    } else { // initally visibility is undefined, so can't use that to check if layer is visible or not, just follow the instruction to hide/show it.
        
        map.setLayoutProperty(id, 'visibility', (toVisible ? 'visible': 'none'));
    }
}
function makeStepSegments(fillColorArr)  {
    let segments = [];
    /* skip first two elements since they describe the type of expression and its input
    * while the rest are pairs of a color and a respective step threshold
    * https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step */
    for (let i = 2; i < fillColorArr.length; i += 2) {
        let color = fillColorArr[i];
        let prevStep = fillColorArr[i - 1];
        let nextStep = fillColorArr[i + 1];

        let min = prevStep;
        if (i === 2 && fillColorArr.length > 5) {
            let increment = fillColorArr[i + 3] - nextStep;
            min = (nextStep - 1) - increment;
        }

        let max = nextStep - 1;
        if (i === fillColorArr.length - 1) {
            let increment = prevStep - fillColorArr[i - 3];
            max = (prevStep - 1) + increment;
        }

        segments.push({ color, min, max });
    }

    return segments;
};

function makeMatchSegments (fillColorArr)  {
    let segments = [];
    /* skip first two elements since they describe the type of expression and its input
    * skip the last element since it's just a fallback color
    * https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-match */
    for (let i = 2; i < fillColorArr.length - 1; i += 2) {
        let color = fillColorArr[i + 1];
        let category = fillColorArr[i];

        segments.push({ category, color });
    }

    return segments;
}


function loadDataFromUrl(containerDivId, mapLayers, headers, id, base, isMask, isZoom) {
    const datasetPrefix = 'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/';
    const datasetSuffix = '?type=' + (base?'aoi': 'dataset') + '&gridId=1&id=';   
    
    $('#' + containerDivId + ' .lds-spinner').show();
    return new Promise((resolve, reject) => {
        if (mapLayers[id]) {
            resolve( {id, base, isMask, baseLayer: mapLayers[id].layer[0]});
        }
        const url = datasetPrefix + id + datasetSuffix + id;
      
        axios.get(url, { headers }).then(function (response) {
            console.log({id, base, isMask, baseLayer: response.data.data});
            resolve( {id, base, isMask, isZoom, baseLayer: response.data.data});
          }).catch(function (error) {
                // handle error
                console.log(error);
               
                reject(error);
               
            })
        });
}
function loadLayerFromData(containerDivId, data, map, mapLayers) {
   $('#' + containerDivId + ' .lds-spinner').show();
    return new Promise((resolve, reject) => {
        let mapLayer = {};  
        
        let layersToAdd = [];
        // map.on('load', function () {    
        const { baseLayer , isMask, isZoom, id, base} = data; 
        // const baseLayer = data.data;
        // const isMask = data.isMask;
        // const id = data.id;    
        // const base = data.base;
        
        if (!baseLayer) {
            resolve(id);
            return;
        }
       
        mapLayer.full = baseLayer;
        if (!baseLayer.style) {
            mapLayer.type = 'aoi';
            const newLayer = base? generateAOIConfig(baseLayer): generateConfig(baseLayer, null, base);
            layersToAdd.push(newLayer.id);
            mapLayer.layer = newLayer;
            // const lastLayerId = map.getStyle().layers.last().id + '';
            const newLayerId = newLayer.id;
            mapLayers[newLayerId] = { id: newLayerId, type: (base?'aoi':'dataset'), layer: [baseLayer] };
            if (!base) {
                createLegend(containerDivId, mapLayers, map, baseLayer.name, newLayerId, baseLayer.settings);
            } else {
                createLegend(containerDivId, mapLayers, map, baseLayer.name, newLayerId, {'fill-color': AOIColor[newLayerId]}, true);
            }
            map.addLayer(newLayer);
            
            if (isMask) {                         
                drawMask(map, [baseLayer]);               
            }
            if (isZoom || (!isZoom && isMask))  {
                zoomToLayer(map, mapLayers, newLayerId);  
            }
        
        } else {
            mapLayer.type = 'data';
            let layer = baseLayer.style.layers[0];
            let source = baseLayer.style.sources.dataset
            mapLayer.layer = layer;
            mapLayer.source = source;
            mapLayer.id = baseLayer.id + '';
            layer.source = baseLayer.id + '';
                
        
            if ( baseLayer.style.layers.length <= 1) {
                map.addSource(baseLayer.id, source);
                map.addLayer(layer);
                mapLayers[layer.source] = { source: source, layer: layer };        
                createLegend(containerDivId, mapLayers, map, baseLayer.name, layer.source, layer['paint']);
                layersToAdd.push(baseLayer.id);
            } else { // multiple layers
                if (baseLayer.style.layers) {
                    createLegend(containerDivId, mapLayers, map, baseLayer.name, layer.source, baseLayer.style.layers);
                }
                
                map.addSource(baseLayer.id, source);
                baseLayer.style.layers.forEach( x => {
                    x.source = baseLayer.id + '';  
                    map.addLayer(x);  
                    mapLayers[x.id] = { source: x.source, layer: x }; 
                    layersToAdd.push(x.id); 
                });            
                mapLayers[baseLayer.id + ''] = { linkedLayer: layersToAdd }; 
            }
        }
        let loadTasks = [];
        // layersToAdd.forEach(x => { console.log('checking ',x);
         loadTasks.push(checkSourceLoaded (id, map)); // })
        Promise.all(loadTasks).then( x=>{console.log('check done',x);  resolve(x)} );
       
    });
    
}

function zoomToLayer(map, mapLayers, layerId) {
    return new Promise((resolve) => {
        if (layerId && mapLayers[layerId]) {
            let bbox;
            if (mapLayers[layerId].bbox) {
                bbox = mapLayers[layerId].bbox;     
            } else {
                let geoms = Object.values(mapLayers[layerId].layer).map(layer => ({ type: 'Feature', geometry: layer.geom }));            
                bbox = turf.bbox(geoms[0]);  
                mapLayers[layerId].bbox = bbox;
            }         
            map.fitBounds(bbox);
        }

    })
}

function checkSourceLoaded (id, map) {
    return new Promise((resolve, reject) => {
      let time = 0;
      let timer = setInterval(() => {
        try {
          if (map.getSource(id) && map.isSourceLoaded(id)) {
            clearInterval(timer)
            resolve(id)
          }

          time += 500;

          // timeout
          if (!map.getSource(id) || time > 30000) {
            clearInterval(timer)
            resolve(id);
          }
        } catch (e) {
          clearInterval(timer)
          reject(e)
        }
      }, 500)
    })
  }
  function createLegend(containerDivId, mapLayers, map, name, id, paint, isAOI) {
    const fill = paint['fill-color'] || paint;
    const border = paint['fill-outline-color'];
    let defaultLegendDiv = '#' + containerDivId + " #legendRow" + id;
    if (Array.isArray(fill)) { // legend item add inside dive
        $(defaultLegendDiv + ' .legend-header').append('<b>' + name + '</b>');
      //   $('#' + containerDivId + ' #hf-legend .legend-container').append('<div class="legends" id="hf-legend' + id + '"><h5>' + name + '</h5></div></div>'); //$('#hf-legend').append('<div class="legend-container"><div class="toggle-legend arrow-down"></div><div class="legends" id="hf-legend' + id + '"><h5>' + name + '</h5><button id="hf-hide' + id + '">Hide</button><button id="hf-show' + id + '">Show</button></div></div>');
        if(fill[0] == 'match') {                   
          
            for (let i = 2; i < fill.length - 1; i += 2) {

                $(defaultLegendDiv).append('<div class="legend-row subRow"> <div class="legend" style="background-color:' + fill[i + 1] + ';" ></div><span>' + fill[i] + '</div></div>')
            }
        } else if (fill[0] == 'step') {
            const segment = makeStepSegments (fill);
            
            for (let i = 0; i < segment.length ; i ++) {
                $(defaultLegendDiv).append('<div class="legend-row subRow"> <div class="legend" style="background-color:' + segment[i].color + ';" ></div><span>' + segment[i].min + '-' + segment[i].max + '</div></div>')
            }
        } else if (fill[0].type == 'fill') {// multiple layer
            if ($(defaultLegendDiv).length > 0) {
              //  $(defaultLegendDiv).append('<b>' + name + '</b>');
            } else {    
                $('#' + containerDivId + ' #hf-legend .legend-container .normal').append('<div><div class="legend-row1" id="legendRow' + id + '"> </div></div>');
            }

            for (let i = 0; i < fill.length; i ++) {
               
                $(defaultLegendDiv ).append('<div class="legend-row subRow" id="legendRow' + fill[i].id + '"><label class="checkbox-label"><input type="checkbox"  data-id="' + fill[i].id + '" id="ch_legend' + id + '" checked><span class="checkbox-custom rectangular"></span></label><div class="legend" style="background-color:' + fill[i].paint['fill-color'] + ';" ></div><span>' + fill[i].id + '</span></div></div>');
            }
        }

    } else { // single legend item add next to checkbox
        if ($(defaultLegendDiv).length == 0) {          
            $('#' + containerDivId + ' #hf-legend .legend-container .normal').append('<div><div class="legend-row" id="legendRow' + id + '"><div class="legend-header">  <label class="checkbox-label"><input type="checkbox" data-id="' + id + '" id="ch_legend' + id + '" checked><span class="checkbox-custom rectangular"></span></label></div></div></div>');
        }
        const  headerDiv =  defaultLegendDiv + ' .legend-header';
        if (isAOI) { // create border
            $(headerDiv).append('<div class="legend" style="border:2px solid ' + AOIColor[id] + ';" ></div><span>' + name + '</span>')
        }
        else if (typeof fill == 'string') {
            $(headerDiv ).append('<div class="legend" style="background-color:' + fill + '; border: 2px solid ' + border + ';" ></div><span>' + name + '</span>')
        } else {
            let opacity = '';
            if (fill.opacity) {
                opacity = ';opacity: ' + (fill.opacity/100);
            }
            $(headerDiv).append('<div class="legend" style="background-color:#' + fill.color + opacity + ';" ></div><span>' + name + '</span>');
        }             
    }
    
    $(defaultLegendDiv).show();
    $('#' + containerDivId + ' input').unbind().change(function () {
        const mapId = $(this).data('id');
        const isChecked =  $(this).is(":checked");
        if (mapLayers[mapId].linkedLayer) {
            mapLayers[mapId].linkedLayer.forEach( x => {
                changeLayerVisibility(containerDivId, map, x, isChecked);
                $('#' + containerDivId + ' *[data-id="' +x + '"]').prop('checked', isChecked);;
            });
        } else {
            changeLayerVisibility(containerDivId, map,  $(this).data('id'), isChecked);
        }
        // if ($(this).is(":checked")) {
        //     showLayer(containerDivId, map, $(this).data('id'));
        // } else {
        //     removeLayer(containerDivId, map, $(this).data('id'));
        // }
    })
}
  function createLegendRow(containerDivId, layerIds, isAOI, isRadio) {
     if (layerIds.length  == 0 ) {
         return;
     }
     layerIds.forEach( id => {        //<div class="legend" style="background-color:' + fill + ';" ></div><span>' + name + '           
    //  console.log('#' + containerDivId + ' #hf-legend .legend-container ' + (isAOI? '.aoi': ' .normal'))
        const hfLayer  = id + (isAOI? 'aoi': '');
        
        $('#' + containerDivId + ' #hf-legend .legend-container ' + (isAOI? '.aoi': ' .normal')).append(
            getDynHtml(
            '<div><div style="display: none;" id="legendRow{{hfLayer}}"><div class="legend-header"> <label class="checkbox-label"><input type="checkbox" data-id="{{hfLayer}}" id="ch_legend{{hfLayer}}"' +
            ' checked><span class="checkbox-custom rectangular {{radio}} "></span></label></div></div></div>', {hfLayer, radio: (isRadio && !isAOI? 'radio': '')}));
    });    
}
/* Idea from Stack Overflow https://stackoverflow.com/a/51683226  */
class MapboxGLButtonControl {
    constructor({
    className = "",
    title = "",
    eventHandler = evtHndlr
    }) {
    this._className = className;
    this._title = title;
    this._eventHandler = eventHandler;
    }

    onAdd(map) {
    this._btn = document.createElement("button");
    this._btn.className = "mapboxgl-ctrl-icon" + " " + this._className;
    this._btn.type = "button";
    this._btn.title = this._title;
    this._btn.onclick = this._eventHandler;

    this._container = document.createElement("div");
    this._container.className = "mapboxgl-ctrl-group mapboxgl-ctrl";
    this._container.appendChild(this._btn);

    return this._container;
    }

    onRemove() {
    this._container.parentNode.removeChild(this._container);
    this._map = undefined;
    }
}

function initMap (mapDivId, layersToLoad) {	 
    let mapLayers = {};     
    const containerDivId = mapDivId + '_container'    ;
    const bounds = [
        [-130, 40], // Southwest coordinates
        [-90, 70]  // Northeast coordinates
       ];

    let headers = {};
    headers['x-api-key'] = 'NSgyFqN2n31UQPd3uLA8o2w0A7TJ0Gk29JPVK041';

    mapboxgl.accessToken = 'pk.eyJ1IjoicWZhbmciLCJhIjoiY2p5NjJ3OHNqMGUydDNibXpvZzdodzNsaCJ9.HAK1XLZht400oO5Q_19LsQ';
     /* add legend expand compress action */
     $('#' + containerDivId + ' #hf-legend .icon').click(() => {
        const legendObj = $('#' + containerDivId + ' #hf-legend');
        legendObj.toggleClass( 'compress', !legendObj.hasClass('compress') );
        legendObj.toggleClass( 'expand', !legendObj.hasClass('expand') );
    });
 
  
    return new Promise((resolve) => {
        let map = new mapboxgl.Map({
            container: mapDivId,
            // style: 'mapbox://styles/mapbox/light-v10',
            zoom: 6,
            
            center: [-114.6309516, 56.6293613],
            maxBounds: bounds,
            // style: 'mapbox://styles/mapbox/satellite-v9'
            trackResize: true
        });
        map.addControl(new mapboxgl.FullscreenControl());
        map.addControl(new mapboxgl.NavigationControl());
        map.addControl(new mapboxgl.ScaleControl({maxWidth: 80,
            unit: 'metric'}), 'bottom-right');

        const zoomToExtent = new MapboxGLButtonControl({
            className: "mapbox-gl-draw_polygon",
            title: "Zoom to Extent",
            eventHandler: function () {                     
                zoomToLayer(map, mapLayers, layersToLoad.zoom ? layersToLoad.zoom + 'aoi': (layersToLoad.mask ? layersToLoad.mask + 'aoi' : '7609aoi'));                 
            }
        });

        $('#' + containerDivId + ' button#resize-map').click(() => {
            map.resize();   
            zoomToLayer(map, mapLayers, layersToLoad.zoom ? layersToLoad.zoom + 'aoi': (layersToLoad.mask ? layersToLoad.mask + 'aoi' : '7609aoi'));          
        });
              
          
        /* Add Controls to the Map */
        map.addControl(zoomToExtent);

        map.on('data', function (data) {  
            if (data.dataType === 'source' && data.isSourceLoaded) {        
                    $('#' + containerDivId + ' .lds-spinner').hide();
            }
        })
        
        
        const topographUrl = 'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3602?type=basemap&id=3602&grid_id=1'; // grey scale
       
        axios.get(topographUrl, { headers }).then(function (styleResponse) {
            const style = styleResponse.data && styleResponse.data.data && styleResponse.data.data.style;
            if (style) {
                map.setStyle(style);
            } else {
                map.setStyle('mapbox://styles/mapbox/light-v10')
            }
            
            // let loadingLayerTasks = [];
          
            // createLegendRow(containerDivId, layersToLoad.layers || []);
            // createLegendRow(containerDivId, layersToLoad.aoi || []);
            // (layersToLoad.layers || []).forEach((id) => loadingLayerTasks.push(loadDataFromUrl(containerDivId, headers, id))); 
            // (layersToLoad.aoi || []).forEach((id) => loadingLayerTasks.push(loadDataFromUrl(containerDivId,  headers,  id, true, id ==layersToLoad.mask)));   
            // console.log(loadingLayerTasks);
            // Promise.all(loadingLayerTasks).then((loadedAPIData)	=> {	
            //     console.log('loaded data', loadedAPIData)				;
            //     (layersToLoad.layers || []).concat(layersToLoad.aoi || []).forEach((id) => {
            //         let data = loadedAPIData.find(x => x.id == id);
            //         loadLayerFromData(containerDivId, data, map,  mapLayers);
            //     });   

                switchLayers(containerDivId, layersToLoad, map,  mapLayers, headers)
                resolve({mapLayers, map, headers});                   
            });	 
           
        }); 
    }
/* bugs, when move layer to the end, the end layers are replaced by the moved layer */
function switchLayers(containerDivId, layersToLoad, map,  mapLayers, headers) { 
    $('#' + containerDivId + ' .lds-spinner').show();
    // hide current layers.
    
    const allNewLayers = (layersToLoad.layers || []).concat(layersToLoad.aoi || []);
    // hide existing layer not to be used
    console.log(mapLayers);
    Object.keys(mapLayers).forEach((layerId) => {
        if (!(layersToLoad.layers || []).includes(layerId)
            && !(layersToLoad.aoi || []).map(x => x + 'aoi').includes(layerId)
        ) {
            if (mapLayers[layerId] && mapLayers[layerId].layer ) {                
                removeLayer(containerDivId, map, layerId);
            }            
            $('#' + containerDivId + ' #legendRow' + layerId ).hide();
        }      
          
    });
    // first check if layer is already loaded, if so, skip them
    let loadingLayerTasks = [];
    (layersToLoad.layers || []).concat(layersToLoad.aoi || []).forEach((includeId) =>  { 
        const id = includeId + ((layersToLoad.aoi || []).includes(includeId) ? 'aoi': '') ;
        if ( map.getSource(id) && map.isSourceLoaded(id)) {
            // already loaded, show layer, show legend
                if (mapLayers[id] && mapLayers[id].linkedLayer) {
                    mapLayers[id].linkedLayer.forEach(x=> {
                        if ($('#' + containerDivId + ' #legendRow' + x + ' input').is(":checked")) {
                            showLayer(containerDivId , map, x);
                        }
                        $('#' + containerDivId + ' #legendRow' + x ).show();
                        
                    });
                } else {
                   
                    if ($('#' + containerDivId + ' #legendRow' + id + ' input').is(":checked")) {
                        showLayer(containerDivId , map, id);
                    }
                    $('#' + containerDivId + ' #legendRow' + id ).show();
                }
                
            } else {
                createLegendRow(containerDivId, [includeId],  (layersToLoad.aoi || []).includes(includeId), layersToLoad.useRadioButton, layersToLoad.customHeader);
                loadingLayerTasks.push(loadDataFromUrl(containerDivId, mapLayers, headers, includeId, (layersToLoad.aoi || []).includes(includeId), 
                layersToLoad.mask == includeId, layersToLoad.zoom == includeId));
            }
    }); 
    
  
    Promise.all(loadingLayerTasks).then((loadedAPIData)	=> {	
        
        let renderingTasks = [];
        allNewLayers.forEach((id) => {
            let data = loadedAPIData.find(x => x.id == id);
            if (data) { // already loaded layer, don't have api data here
                renderingTasks.push(loadLayerFromData(containerDivId, data, map,  mapLayers));
            }
        }); 
        // do masks when all layers loaded.
        Promise.all(renderingTasks).then((loadedLayers) => {
            console.log('loadedLayers', loadedLayers);
            // move aoi to front
            if (layersToLoad.layers && layersToLoad.layers.length > 0) {
                (layersToLoad.aoi || []).forEach( x => {                    
                    map.moveLayer(x + 'aoi'); // move to top, make sur all ids are string                     
                })
            };

            if (layersToLoad.mask) {
                if (map.getSource('world')) {
                    // const layerIds = Object.keys(mapLayers).filter(x => mapLayers[x].layer != null);
                    
                    if (layersToLoad.layers) { 
                        
                        map.moveLayer('world') ; //, map.getStyle().layers.last().id + '');                    
                        
                    }
                } else {
                        drawMask(map, mapLayers[layersToLoad.mask].layer);     
                }          
            } 
            
            // make layer group radio button.
            // when set as use radio button, only select the first checkbox
            if (layersToLoad.useRadioButton) { 
                (layersToLoad.layers || []).forEach( (layerId, index) => {
                    $('#' + containerDivId + ' #legendRow' + layerId + ' .checkbox-custom').addClass("radio");
                    if (index > 0) {
                        $('#' + containerDivId + ' #legendRow' + layerId + ' input').prop("checked", false);
                        removeLayer(containerDivId, map, layerId);
                        $('#' + containerDivId + ' #legendRow' + layerId + ' .subRow').hide(); // hide detailed legend
                     }
                });

                loadedLayers.flat().forEach( layerId => {
                    if (!(layersToLoad.layers || []).includes(layerId)) { // only work inside layers not in aoi
                        return;
                    }
                    $('#' + containerDivId + ' #legendRow' + layerId + ' input').change(function () {
                        const isChecked =  $(this).is(":checked");
                        if (!isChecked) {// when uncheck do nothing.
                            return; 
                        }
                        $('#' + containerDivId + ' #legendRow' + layerId + ' .subRow').show();
                        const mapId = $(this).data('id');                   
                        (layersToLoad.layers || []).forEach( otherLayerId => {
                            if (otherLayerId != mapId) {
                                $('#' + containerDivId + ' #legendRow' + otherLayerId + ' input').prop("checked", false);
                                $('#' + containerDivId + ' #legendRow' + otherLayerId + ' .subRow').hide(); // hide detailed legend
                                removeLayer(containerDivId, map, otherLayerId);
                            }
                        });
                    })

                });
            }
            // if all data layer share same color ramp, only show one on top
            if (layersToLoad.hasOwnProperty('isSingleColorScheme')) { 
                (layersToLoad.layers || []).forEach( (otherLayerId, index) => {          
                    if (index  == 0)  {
                        // copy legend to top
                        $('#' + containerDivId + ' .normal').prepend('<div class="common"></div>');
                        
                        $.each($(' #legendRow' + otherLayerId + ' .subRow'), (rowIndx, row) => {
                            if (layersToLoad.isSingleColorScheme && layersToLoad.isSingleColorScheme.isReverseOrder) {
                                $(row).clone().prependTo($('#' + containerDivId + ' .normal .common'));
                            } else {
                                $(row).clone().appendTo($('#' + containerDivId + ' .normal .common'));
                            }
                        }); 

                    }
                    $('#' + containerDivId + ' #legendRow' + otherLayerId + ' .subRow').remove(); // hide detailed legend

                    
                    
                });
                
            }
        });
                   
    });	 
    // if not loaded, load them
}

