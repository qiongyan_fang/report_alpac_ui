function createViolinChart(url, d3ObjId, title, yTitle, yRange, bFilterValue, bIntactessLabel, bShowRankedSpp, sppGroup) {
	var gColors = ['#336600', '#cc9900', '#66cc66', '#999900', '#748838', '#666600', '#6699cc', '#096C85', '#333399', '#006600',  '#666699', '#333333'];
	var myPlot = document.getElementById(d3ObjId);
	var width = myPlot.clientWidth;
	if (!width) { // if it is hidden and no width can't be get, a workaround here is to show the element, and get the width and then hide it.
		var parentHiddenDiv = $('#' + d3ObjId ).parents('.toolWrap');
		if (parentHiddenDiv.length > 0) {
			if ($(parentHiddenDiv[0]).is(":hidden")){
				$(parentHiddenDiv[0]).show();
				width = myPlot.clientWidth;
				$(parentHiddenDiv[0]).hide();
			}
		}
	}
	Plotly.d3.json(url, function(error, data) {
		if (!data || data.length == 0) { // don't draw anything if no data.
			return; 
		}
		var traceList = [];
		var yMax = 100;
		var yMin = -100;
		if (yRange) {
			yMax = yRange[1];
			yMin = yRange[0];
		}
		var anyExceedMax = false;
		$.each(data, function(index, row) {
			var filteredData = row.data;
			var exceedMax = false;
			if (bFilterValue) {
				filteredData = row.data.filter(function(value) {return value >= yMin && value <=yMax;});
				exceedMax = row.data.some(function(value) {return value > yMax;});
			}
			if (exceedMax) {
				anyExceedMax = true;
			}
			var valueMax = Math.max(...filteredData);
			var valueMin = Math.min(...filteredData);
			var color  = row.color;
			if (!color) {
				 color = gColors[index%gColors.length];
			}
			var customData = '';
			if (bIntactessLabel && filteredData.length > 0) { // calculate intactness mean values. convert 200-> 0, 150->50
				var sum = 0;
				filteredData.forEach(function(value) {
					sum +=  (value > 100? 200 - value: value);
				});
				var avg = parseInt(sum/filteredData.length);
				customData += '<div class="highlight">' + sppGroup + ' Intactness: <b>' + avg.toFixed(1) + '%</b> | ';
				customData += 'Number of species: <b>' + filteredData.length + '</b></div>';				
			}
			if (bShowRankedSpp) {
				var htmlStr = '';

				if (row.increaser) {
					var html = '<div class="column"><b>Top Increaser</b><ul class="increaser">';
					row.increaser.forEach(function(spp) {
						if (sppGroup.indexOf('Birds') != -1 || sppGroup.indexOf('Mammals') != -1 || sppGroup == 'Vascular Plants'){
							html +='<li class="spp-name">' + (spp.cname ? spp.cname : '<i>' + spp.sname + '</i>') + '</li>';
						}
						else if (sppGroup.indexOf('Bryo') != -1 || sppGroup.indexOf('Moss') != -1 || sppGroup.indexOf('Lichen') != -1){
							if (spp.cname) {
								html +='<li class="spp-name">' + (spp.cname?spp.cname:'') + ' (<i>' + spp.sname + '</i>) </li>';
							} else {
								html +='<li class="spp-name"><i>' + spp.sname + '</i></li>';	
							}
						}
						else {
							html +='<li class="spp-name"><i>' + spp.sname + '</i> </li>';
						}
					});
					htmlStr += html + '</ul></div>';
				}	
				if (row.decreaser) {
					var html = '<div class="column"><b>Top Decreaser</b><ul class="decreaser">';
					row.decreaser.forEach(function(spp) {
						if (sppGroup.indexOf('Birds') != -1 || sppGroup.indexOf('Mammals') != -1 || sppGroup == 'Vascular Plants'){
							html +='<li class="spp-name">' + (spp.cname?spp.cname:'') + '</li>';
						}
						else if (sppGroup.indexOf('Bryo') != -1 || sppGroup.indexOf('Moss') != -1 || sppGroup.indexOf('Lichen') != -1){
							if (spp.cname) {
								html +='<li class="spp-name">' + (spp.cname?spp.cname:'') + ' (<i>' + spp.sname + '</i>) </li>';
							} else {
								html +='<li class="spp-name"><i>' + spp.sname + '</i></li>';	
							}							
						}
						else {
							html +='<li class="spp-name"><i>' + spp.sname + '</i></li>';
						}
					});
					htmlStr += html + '</ul></div>';
				}
				customData += '<div class="columns no-margin">' + htmlStr + '</div>';
			}
			
			var trace = {
				text: row.name + '<br>spp1<br>spp2',
				customdata: row.name + customData,
				//hovertext: ['points+kde'],
				hoverinfo: 'none',
				hoveron: 'violins+fills',
				meanline: {
					visible: !bIntactessLabel // don't show mean line when display intactness values, as the intactness values >100 will be converted to 0-100
				},
				opacity: 2,
				legendgroup: row.name,
				scalegroup: row.name,									
				box: {
					visible: true
				},
				fillcolor: color+'cc',
				jitter: 0,
				scalemode: "count",
				marker: {
					color: color,
					line: {
						width: 2,
						color: color +'ff'
					},
					opacity: [1],
					symbol: "line-ns"
				},
				showlegend: false,
			//	span: yRange,
				span: [
						valueMin, valueMax
					],
				type: "violin",
				name: row.name,				
				line: {
					color: color
				},
				x0: row.name,
				y: filteredData
			};
			
			traceList.push(trace);
			// put a dot if there are > 100 values
			if (exceedMax) {
				var trace2 = {
					customdata: row.name + customData,
					x0: [row.name],
					y: [yRange[1]],
					mode: 'markers',
					hoverinfo: 'none',
					name: 'values exceed maximum values',
					showlegend: false,
					marker: {
						color: row.color,									
						symbol: 'circle',
						size: 12
				  }
				};
				traceList.push(trace2);
			}
			
			
		});
		if (anyExceedMax) {
			yRange[1] += 5; //leave room to display circle marker.
		}
		var layout = {
			font: {
				family: '"Open Sans",sans-serif',
    
			},
		//hovermode: "closest",
			//autoresize: true,
			width: width,
			height: (width > 700? 500: width*0.8),
			margin: {t: 40, b:80},
			yaxis: {
				showgrid: true,
				range: yRange,
				title: yTitle,
				tickvals: (bIntactessLabel?[0,  50, 100, 150, 200]:null),
				ticktext: (bIntactessLabel?[0, 50, 100, 50, 0]:null),
				//ticklen: 10,
				titlefont: {				
				  size: 18				  
				},
				showline: true,
				side: 'left+right'
			},
			xaxis: {
				showline: false
			},
			//title: title,			
			/*legend: {
				//tracegroupgap: 20,
				orientation: 'h' | 'v'
			},*/
			violingap: 20,
			violingroupgap: 20,
			//violinmode: "overlay",
			
		};
		
		if (bIntactessLabel) {
			layout.annotations =  [
			 {
				x: 1,
				y: 1,
				showarrow: false,
				text: "< Increaser",
				xref: "paper",
				yref: "paper",
				textangle: '90',
				font: {
					size: 14,
					color: '#8e6828'
				},
			 },
			 {
				x: 1,
				y: 0.02,
				showarrow: false,
				text: 'Decreaser >',
				font: {
					size: 14,
					color: '#8e6828'
				},
				xref: 'paper',
				yref: 'paper',
				textangle: '90',
			 }
			];
			
			layout.shapes = [
				{
					type: 'line',
					xref: 'paper',
					yref: 'y',
					x0: 0,
					y0: 100,
					x1: 1,
					y1: 100,
					fillcolor: 'rgb(80, 80, 80)',
					line:{
						color: 'rgb(80, 80, 80)',
						
						width: 2,
						//dash:'dash'
					},
					layer: 'below'
				}
				];
		
			
			
		}
		 var defaultPlotlyConfiguration = { modeBarButtonsToRemove: ['sendDataToCloud', 'autoScale2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'lasso2d', 'select2d'], displaylogo: false, showTips: true };
		// display the plot in myPlot HTML element
		
		Plotly.plot(d3ObjId, traceList, layout, defaultPlotlyConfiguration);
	   
		// tried using annotation boxes to display hoverover, however it will cause infinite loop, use custom defined div instead.
		if (bIntactessLabel ||  bShowRankedSpp) {
			var hoverObj = $('#h' + d3ObjId + " .content");
			myPlot.on('plotly_hover',
				function(data){
					var point = data.points[0];
					var text= point.data.customdata;		
					
					$('#h' + d3ObjId).css({ left: 0 + 'px'});
					hoverObj.html(text);
					$('#h' + d3ObjId).show();
								
					var contentWidth = $('#h' + d3ObjId).width();
					var centerPosition = point.xaxis.d2p(point.x);
					// console.log('width =' + width + ' contentWidth=' + contentWidth + ' centerPosition=' + centerPosition);
					if (centerPosition + 0.5*contentWidth > width){
						centerPosition = (width -0.5*contentWidth); 
						// console.log('move left centerPosition=' + centerPosition);
					}
					if (centerPosition - 0.5*contentWidth < 0){
						centerPosition = (0.5*contentWidth); 
						// console.log('move right centerPosition=' + centerPosition);
					}
					if (bShowRankedSpp) {
						$('#h' + d3ObjId).removeClass()
						$('#h' + d3ObjId).addClass('violin-hover');
						$('#h' + d3ObjId).addClass('sppPop');
						$('#h' + d3ObjId).addClass(point.x.replace('/', ''));
					}
					$('#h' + d3ObjId).css({// left: (centerPosition - 0.5*contentWidth) + 'px',
						'border-color': point.data.marker.color
						})
			});
			if (!bShowRankedSpp) { // spp list is long so don't hide when unhover. 
				$('#h' + d3ObjId + '  span').hide(); // don't show close button
				myPlot.on('plotly_unhover', function(data){ // hide un hover events, otherwise, you see hover boxes flicking when the boxes cover the graphs
					$('#h' + d3ObjId).hide();
					if (bShowRankedSpp) {
						$('#h' + d3ObjId).removeClass(data.points[0].x.replace('/', ''));	
					}					
				});
			}
			else {
				myPlot.on('plotly_unhover', function(data){ // hide un hover events, otherwise, you see hover boxes flicking when the boxes cover the graphs
					//$('#h' + d3ObjId).hide();
					//$('#h' + d3ObjId).removeClass(data.points[0].x.replace('/', ''));					
				});
				$('#h' + d3ObjId).css({ position:'relative',
						'width': '100%',
						'margin-bottom': '20px'
						})
			}
			$('#h' + d3ObjId + " span").click(function() {$('#h' + d3ObjId).hide();});
		}
	});
}


/* normal bars */

function createSectorEffectBarChart_NoUsed(url, d3ObjId, title, yTitle, yRange, bAddHighlitLine, highlitValue) {

	var myPlot = document.getElementById(d3ObjId);
	var width = myPlot.clientWidth;
	if (!width) { // if it is hidden and no width can't be get, a workaround here is to show the element, and get the width and then hide it.
		var parentHiddenDiv = $('#' + d3ObjId ).parents('.toolWrap');
		if (parentHiddenDiv.length > 0) {
			if ($(parentHiddenDiv[0]).is(":hidden")){
				$(parentHiddenDiv[0]).show();
				width = myPlot.clientWidth;
				$(parentHiddenDiv[0]).hide();
			}
		}
	}
	Plotly.d3.json(url, function(error, data) {
		if (!data || data.length == 0) { // don't draw anything if no data.
			return; 
		}
		
		data.value.forEach(function(value, index, arr) {
			arr[index] = value.toFixed(1);
		});
			
		var traceList = [];
		var yMax = 100;
		var yMin = -100;
		if (yRange) {
			yMax = yRange[1];
			yMin = yRange[0];
		}
	
		var trace = {
			//text: row.name + '<br>spp1<br>spp2',
			//hovertext: ['points+kde'],
			hoverinfo: 'none',
			hoveron: 'bars+fills',
			opacity: 2,
			x: data.category,
			y: data.value,

			text: data.value.map(x => (x > 0 ? '+' : '') + x  + '%'),
			textposition: 'outside',					
			marker:{
				color: (data.color!=null?data.color: '#44897a')
			},
			type: 'bar',			
			showlegend: false,
			orientation: 'v'			
		};	
		
		var layout = {
			font: {
				family: '"Open Sans",sans-serif',
    
			},
			width: width,
			height: (width > 700? 700: width),
			margin: {t: 20},
			
		//	title: title
		};
		
		layout.yaxis = {
			showgrid: true,
			range: yRange,
			title: yTitle,			
			titlefont: {				
				size: 18				  
			},
			showline: true,
			showarrow: true
		};
			
		var defaultPlotlyConfiguration = { modeBarButtonsToRemove: ['sendDataToCloud', 'autoScale2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'lasso2d', 'select2d'], displaylogo: false, showTips: true };
		// display the plot in myPlot HTML element
		
		Plotly.plot(d3ObjId, [trace], layout, defaultPlotlyConfiguration);	   
		
	});
}

// /* hard code */
// function createBufferAreaChart(url, d3ObjId, title, yTitle, yRange) {
// 	var myPlot = document.getElementById(d3ObjId);
// 	var width = myPlot.clientWidth;
// 	if (!width) { // if it is hidden and no width can't be get, a workaround here is to show the element, and get the width and then hide it.
// 		var parentHiddenDiv = $('#' + d3ObjId ).parents('.toolWrap');
// 		if (parentHiddenDiv.length > 0) {
// 			if ($(parentHiddenDiv[0]).is(":hidden")){
// 				$(parentHiddenDiv[0]).show();
// 				width = myPlot.clientWidth;
// 				$(parentHiddenDiv[0]).hide();
// 			}
// 		}
// 	}
// 	var barColor = ['#066006fa', '#39a239fa', '#5ea65efa', '#a2d6a2fa'];
// 	var xValue = ['Oil Sands Region', 'Athabasca', 'Cold Lake', 'Peace River'];	
// 	var yLabel = ['0 m', '50 m', '200 m', '500 m'];
	
// 	var yValues = 	[[84.5,	91.3, 58.2, 78.7 ],
// 					[69.2, 78.4, 35.1, 61.0 ],
// 					[48.5, 55.7, 29.0, 37.2 ],
// 					[28.5, 32.6, 23.4, 18.3]];
// 	var traceList = [];
// 	var yMax = 100;
// 	var yMin = -100;
// 	if (yRange) {
// 		yMax = yRange[1];
// 		yMin = yRange[0];
// 	}
	
// 	yValues.forEach(function(row, index){
// 		var trace = {
// 		  x: xValue,
// 		  y: row,
// 		  type: 'bar',
// 		  name: yLabel[index],
// 		  text: row,
// 		  textposition: 'outside',
// 		  hoverinfo: 'none',
// 		  opacity: 0.6,
		  
// 		  marker: {
// 			color: barColor[index%barColor.length],
// 			line: {
// 			  color: '#ffffff',
// 			  width: 1.5
// 			}
// 		  }
// 		};
// 		traceList.push(trace);
// 	});
// 	var layout = {
// 		font: {
// 				family: '"Open Sans",sans-serif',
    
// 			},
// 		width: width,
// 		height: (width > 700? 560: width*0.8),
// 		margin: {t: 20},
// 		yaxis: {
// 			showgrid: true,
// 			range: yRange,
// 			title: yTitle,			
// 			titlefont: {				
// 			  size: 18				  
// 			}
// 		},
// 	//	title: title
// 	};

// 	 var defaultPlotlyConfiguration = { modeBarButtonsToRemove: ['sendDataToCloud', 'autoScale2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'lasso2d', 'select2d'], displaylogo: false, showTips: true };
// 		// display the plot in myPlot HTML element
		
// 	Plotly.plot(d3ObjId, traceList, layout, defaultPlotlyConfiguration);	   
// }