function generateConfig(layer, config) {
    let { id, settings, geom } = layer;

    if (!config) {

        try {
            geom = JSON.parse(geom);
        } catch (e) {
        }

        config = {
            'id': id + '',
            'type': 'line',
            'source': {
                'type': 'geojson',
                'data': {
                    'type': 'Feature',
                    'geometry': geom
                }
            },
            'layout': {},
            // 'paint': {
            // 'fill-color': '#088',
            // 'fill-opacity': 0,
            // 'fill-outline-color': 'rgba(169,169,169,1)'
            // }
            'layout': {
                'line-join': 'miter',
                'line-cap': 'square'
            },
            'paint': {
                'line-color': '#000000',
                'line-width': 2,
                'line-offset': -2
            }
        };
    }         

    return config;
}

// function switchToLayer(divId, map, layerIds, id) {
//     $('#' + divId + ' .lds-spinner').show();
//     layerIds.forEach(x => {
//         map.setLayoutProperty(x, 'visibility', 'none');
//     });
    
//      map.setLayoutProperty(id, 'visibility', 'visible');
// }
function removeLayer(divId, map, id) {
    
    changeLayerVisibility(divId, map, id, false);
}

function showLayer(divId, map, id) {    
    changeLayerVisibility(divId, map, id, true);
}

function changeLayerVisibility(divId, map, id, toVisible) {
    const visibility = map.getLayoutProperty(id, 'visibility'); // initial is is undefined
    if (visibility) {
        if (visibility === 'visible' && toVisible == false) {
            $('#' + divId + ' .lds-spinner').show();
            map.setLayoutProperty(id, 'visibility', 'none');        
        } else if (visibility !== 'visible' && toVisible == true) {        
            $('#' + divId + ' .lds-spinner').show();
            map.setLayoutProperty(id, 'visibility', 'visible');
        }
    } else { // initally visibility is undefined, so can't use that to check if layer is visible or not, just follow the instruction to hide/show it.
        $('#' + divId + ' .lds-spinner').show();
        map.setLayoutProperty(id, 'visibility', (toVisible ? 'visible': 'none'));
    }
}
function makeStepSegments(fillColorArr)  {
    let segments = [];
    /* skip first two elements since they describe the type of expression and its input
    * while the rest are pairs of a color and a respective step threshold
    * https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step */
    for (let i = 2; i < fillColorArr.length; i += 2) {
        let color = fillColorArr[i];
        let prevStep = fillColorArr[i - 1];
        let nextStep = fillColorArr[i + 1];

        let min = prevStep;
        if (i === 2 && fillColorArr.length > 5) {
            let increment = fillColorArr[i + 3] - nextStep;
            min = (nextStep - 1) - increment;
        }

        let max = nextStep - 1;
        if (i === fillColorArr.length - 1) {
            let increment = prevStep - fillColorArr[i - 3];
            max = (prevStep - 1) + increment;
        }

        segments.push({ color, min, max });
    }

    return segments;
};

function makeMatchSegments (fillColorArr)  {
    let segments = [];
    /* skip first two elements since they describe the type of expression and its input
    * skip the last element since it's just a fallback color
    * https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-match */
    for (let i = 2; i < fillColorArr.length - 1; i += 2) {
        let color = fillColorArr[i + 1];
        let category = fillColorArr[i];

        segments.push({ category, color });
    }

    return segments;
}

function createLegend(divIdMap, map, name, id, fill) {
    const divId = divIdMap + '_container';
    if (Array.isArray(fill)) {
         $('#' + divId + ' #hf-legend .legend-container').append('<div class="legends" id="hf-legend' + id + '"><h5>' + name + '</h5></div></div>'); //$('#hf-legend').append('<div class="legend-container"><div class="toggle-legend arrow-down"></div><div class="legends" id="hf-legend' + id + '"><h5>' + name + '</h5><button id="hf-hide' + id + '">Hide</button><button id="hf-show' + id + '">Show</button></div></div>');
   
        
        if(fill[0] == 'match') {                   

            for (let i = 2; i < fill.length - 1; i += 2) {

                $('#' + divId + ' #hf-legend' + id).append('<div class="legend-row"> <div class="legend" style="background-color:' + fill[i + 1] + ';" ></div><span>' + fill[i] + '</div></div>')
            }
        } else if (fill[0] == 'step') {
            const segment = makeStepSegments (fill);
            
            for (let i = 0; i < segment.length ; i ++) {
                $('#' + divId + ' #hf-legend' + id).append('<div class="legend-row"> <div class="legend" style="background-color:' + segment[i].color + ';" ></div><span>' + segment[i].min + '-' + segment[i].max + '</div></div>')
            }
        } else if (fill[0].type == 'fill') {// multiple layer
            if ($('#legendRow'+ id).length > 0) {
                $("#legendRow" + id ).append('<div class="legend" style="background-color:' + fill + ';" ></div><span>' + name + '</span>');
            } else {    
                $('#' + divId + ' #hf-legend .legend-container').append('<div><div class="legend-row" id="legendRow' + id + '"> </div></div>');
            }

            for (let i = 0; i < fill.length - 1; i ++) {
               
                $("#legendRow" + id ).append('<div class="legend" style="background-color:' + fill[i].paint['fill-color'] + ';" ></div><span>' + fill[i].id + '</span>');
            }
        }

    } else {
        if ($('#legendRow'+ id).length > 0) {
            $("#legendRow" + id ).append('<div class="legend" style="background-color:' + fill + ';" ></div><span>' + name + '</span>');
        } else {    
            $('#' + divId + ' #hf-legend .legend-container').append('<div><div class="legend-row" id="legendRow' + id + '"> <label class="checkbox-label"><input type="checkbox" id="ch_legend' + id + '" checked><span class="checkbox-custom rectangular"></span></label><div class="legend" style="background-color:' + fill + ';" ></div><span>' + name + '</span></div></div>');
        }
    }

  
    $('#' + divId + ' #ch_legend' + id).change(function () {
        if ($(this).is(":checked")) {
            showLayer(divId, map, id);
        } else {
            removeLayer(divId, map, id);
        }
    })
}

function loadLayerFromUrl(divId, map, headers,  mapLayers, url, base, showMask) {
    $('#' + divId + ' .lds-spinner').show();
    let mapLayer = {};
    
    axios.get(url, { headers }).then(function (response) {
        // handle success        
        const baseLayer = response.data.data;
        
        // map.on('load', function () {                
        mapLayer.full = baseLayer;
        if (base) {
            mapLayer.type = 'aoi';
            const newLayer = generateConfig(baseLayer);
            mapLayer.layer = newLayer;
            const layerIds = Object.keys(mapLayers);
            
            console.log('current layers ', layerIds);
            if (layerIds.length > 0) {
                map.addLayer(newLayer); //layerIds.length-1]);
            } else {
                console.log();
                map.addLayer(newLayer);
            }
            if (showMask) {                
                // drawMask(map, [baseLayer], layerIds[layerIds.length-1]);
                zoomToLayer(map, [baseLayer]);
                // $('#hf-zoom').click(function () {
                //     zoomToLayer(map, [baseLayer]);
                // });
                mapLayers[newLayer.id] = { id: newLayer.id, type: 'aoi', layer: [baseLayer] };
            }
            
           
        } else {
            mapLayer.type = 'data';
            let layer = baseLayer.style.layers[0];
            let source = baseLayer.style.sources.dataset
            mapLayer.layer = layer;
            mapLayer.source = source;
            mapLayer.id = baseLayer.id + '';
            layer.source = baseLayer.id + '';
            mapLayers[layer.source] = { source: source, layer: layer };            
          
            if ( baseLayer.style.layers.length <= 1) {
                map.addSource(baseLayer.id, source);
                map.addLayer(layer);
                createLegend(divId, map, baseLayer.name, layer.source, layer['paint']['fill-color']);
            } else { // multiple layers
                createLegend(divId, map, baseLayer.name, layer.source, baseLayer.style.layers);
                map.addSource(baseLayer.id, source);
                baseLayer.style.layers.forEach( x => {x.source = baseLayer.id + '';  map.addLayer(x);});
                
               
            }
        }
    })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
        .finally(function () {
            // always executed            
            return mapLayer;
        });
    
}

function zoomToLayer(map, layers) {
    return new Promise((resolve) => {
        
        let geoms = Object.values(layers).map(layer => ({ type: 'Feature', geometry: layer.geom }));
        
        const bbox = turf.bbox(geoms[0]);
        
        map.fitBounds(bbox);

    })
}

function drawMask(map, layers) {    
    
    return new Promise((resolve) => {
        
        let geoms = Object.values(layers).map(layer => ({ type: 'Feature', geometry: layer.geom }));

        let combined = turf.union(...geoms);
        const world = turf.bboxPolygon([-180, -90, 180, 90]);
        const diff = turf.difference(world, combined);

        let config = {
            'id': 'world',
            'source': {
                'type': 'geojson',
                'data': diff
            },
            'type': 'fill',
            'visibility': 'visible',
            'paint': {
                'fill-color': 'white',
                'fill-opacity': 0.9
            }
        };

        map.addLayer(config);
    })
}

function checkSourceLoaded (id, mapLayers) {
    return new Promise((resolve, reject) => {
      let time = 0;
      let timer = setInterval(() => {
        try {
          if (mapLayers.getSource(id) && mapLayers.isSourceLoaded(id)) {
            clearInterval(timer)
            resolve()
          }

          time += 500;

          // timeout
          if (!mapLayers.getSource(id) || time > 30000) {
            clearInterval(timer)
            reject();
          }
        } catch (e) {
          clearInterval(timer)
          reject(e)
        }
      }, 500)
    })
  }

function initMap (divId) {	 
    let mapLayers = {};         
    const bounds = [
        [-130, 40], // Southwest coordinates
        [-90, 70]  // Northeast coordinates
       ];

    let headers = {};
    headers['x-api-key'] = 'NSgyFqN2n31UQPd3uLA8o2w0A7TJ0Gk29JPVK041';

    mapboxgl.accessToken = 'pk.eyJ1IjoicWZhbmciLCJhIjoiY2p5NjJ3OHNqMGUydDNibXpvZzdodzNsaCJ9.HAK1XLZht400oO5Q_19LsQ';
     /* add legend expand compress action */
     $('#' + divId + '_container #hf-legend .icon').click(() => {
        const legendObj = $('#' + divId + '_container #hf-legend');
        legendObj.toggleClass( 'compress', !legendObj.hasClass('compress') );
        legendObj.toggleClass( 'expand', !legendObj.hasClass('expand') );
    });

    return new Promise((resolve) => {
        let map = new mapboxgl.Map({
            container: divId,
            // style: 'mapbox://styles/mapbox/light-v10',
            zoom: 5,
            center: [-118.447303, 53.753574],
            maxBounds: bounds,
            // style: 'mapbox://styles/mapbox/satellite-v9'
        });
        map.addControl(new mapboxgl.FullscreenControl());
        map.addControl(new mapboxgl.NavigationControl());
        
        map.on('data', function (data) {        
          
                if (data.dataType === 'source' && data.isSourceLoaded) {        
                    $('#' + divId + ' .lds-spinner').hide();
                // stop listening to map.on('data'), if applicable      
                
                
                const maskLayerId = Object.keys(mapLayers).find( x => mapLayers[x].type === 'aoi');
                if (maskLayerId) { 
                    drawMask(map, mapLayers[maskLayerId].layer);
                    delete mapLayers[maskLayerId];
                }
            }
            
            
        })
        
        // const axios = require('axios');
        const topographUrl = 'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3601?type=basemap&id=3601&grid_id=1';
        const dataUrls = [
        'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3667?type=dataset&id=3625&grid_id=1',
        'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3625?type=dataset&id=3667&grid_id=1',
        'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3692?type=dataset&id=3625&grid_id=1',
        'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3680?type=dataset&id=3625&grid_id=1',
        'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3694?type=dataset&id=3625&grid_id=1',
        'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/7772?type=dataset&id=7772&gridId=1',
        ]

        const alpacUrl = 'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/69?type=aoi&id=69&gridId=1';
        const EIAUrl = 'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/7609?type=aoi&id=7609&gridId=1'

        //https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/2253?type=dataset&id=2253&grid_id=1'
        // // Make a request for a user with a given ID
        // dataUrls.forEach((url) => loadLayerFromUrl(divId, map,  headers, mapLayers, url)); 
        // loadLayerFromUrl(divId, map, headers,  mapLayers, EIAUrl, true, true);
        // loadLayerFromUrl(divId, map, headers,  mapLayers, alpacUrl, true);
        
        axios.get(topographUrl, { headers }).then(function (styleResponse) {
            const style = styleResponse.data.data.style;
            map.setStyle(style);
            
            
            dataUrls.forEach((url) => loadLayerFromUrl(divId, map,  headers, mapLayers, url)); 
            checkSourceLoaded('7772', map).then(x=> {
                loadLayerFromUrl(divId, map, headers,  mapLayers, EIAUrl, true, true);
                loadLayerFromUrl(divId, map, headers,  mapLayers, alpacUrl, true);     
            });
            
        }); 
    });

    
    // https://upsckv7isg.execute-api.us-west-2.amazonaws.com/development/search?match=human+footprint+2017&gridId=1 search for human footprint
    // get all categories
    //https://upsckv7isg.execute-api.us-west-2.amazonaws.com/development/layers/?types=dataset&parentId=3664&gridId=1
    // one categories 
    // https://upsckv7isg.execute-api.us-west-2.amazonaws.com/development/layers/?types=dataset&parentId=3668&gridId=1
}

async function getHumanFootprint(divId, humanFootprintVersion) {
    const landCoverId = 5;
    const layerUrlPrefix = 'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/?types=dataset&gridId=1&parentId=';
    const datasetPrefix = 'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/';
    const datasetSuffix = '?type=aoi&gridId=1&id=';
    const hfLayerIds = [3665, 3668, 3691, 3677, 3681, 3687];
    hfLayerIds.forEach( hfLayer => {
        //<div class="legend" style="background-color:' + fill + ';" ></div><span>' + name + '
       
        $('#' + divId + '_container #hf-legend .legend-container').append('<div><div class="legend-row" id="legendRow' + hfLayer + '"> <label class="checkbox-label"><input type="checkbox" id="ch_legend' + hfLayer + '" checked><span class="checkbox-custom rectangular"></span></label></div></div>')
        
    })

    console.log(hfLayerIds.map(x => datasetPrefix + x + datasetSuffix + x ));
    return hfLayerIds.map(x => datasetPrefix + x + datasetSuffix + x );
    /* step 1: get all land cover layers, filter result to get matched human footprint layer data id */
    const landCoverList = await axios.get(layerUrlPrefix + landCoverId);
    const hfCollectionList = landCoverList.filter(x => x.name == humanFootprintVersion );
    if (!hfCollectionList.id) {
        alert ('Error: Can\'t find dataset ' +  humanFootprintVersion);
        return; 
    } 

    const hfList = await axios.get(layerUrlPrefix + hfCollectionList.id);
    /* collect layer information: name, url , put them into legend div, so legend displays in desired order, 
    and then call to load layers */
    
    hfList.forEach( hfLayer => {
        //<div class="legend" style="background-color:' + fill + ';" ></div><span>' + name + '
        if (hfLayer.hasData) {
            $('#' + divId + '_container #hf-legend .legend-container').append('<div><div class="legend-row" id="legendRow' + hfLayer.id + '"> <label class="checkbox-label"><input type="checkbox" id="ch_legend' + hfLayer.id + '" checked><span class="checkbox-custom rectangular"></span></label></div></div>')
        }
    })

    return hfList.map(x => layerUrlPrefix + x.id );

}

function initHfMap (divId, mapVersion) {	 
    let mapLayers = {};         
    const bounds = [
        [-130, 40], // Southwest coordinates
        [-90, 70]  // Northeast coordinates
       ];

    let headers = {};
    headers['x-api-key'] = 'NSgyFqN2n31UQPd3uLA8o2w0A7TJ0Gk29JPVK041';

    mapboxgl.accessToken = 'pk.eyJ1IjoicWZhbmciLCJhIjoiY2p5NjJ3OHNqMGUydDNibXpvZzdodzNsaCJ9.HAK1XLZht400oO5Q_19LsQ';
     /* add legend expand compress action */
     $('#' + divId + '_container #hf-legend .icon').click(() => {
        const legendObj = $('#' + divId + '_container #hf-legend');
        legendObj.toggleClass( 'compress', !legendObj.hasClass('compress') );
        legendObj.toggleClass( 'expand', !legendObj.hasClass('expand') );
    });

    return new Promise((resolve) => {
        let map = new mapboxgl.Map({
            container: divId,
            // style: 'mapbox://styles/mapbox/light-v10',
            zoom: 5,
            center: [-118.447303, 53.753574],
            maxBounds: bounds,
            // style: 'mapbox://styles/mapbox/satellite-v9'
        });
        map.addControl(new mapboxgl.FullscreenControl());
        map.addControl(new mapboxgl.NavigationControl());
        
        map.on('data', function (data) {                
            if (data.dataType === 'source' && data.isSourceLoaded) {        
                $('#' + divId + ' .lds-spinner').hide();
            // stop listening to map.on('data'), if applicable       
                const maskLayerId = Object.keys(mapLayers).find( x => mapLayers[x].type === 'aoi');
                if (maskLayerId) {
                    drawMask(map, mapLayers[maskLayerId].layer);
                    delete mapLayers[maskLayerId];
                }
            }
        })
        
        // const axios = require('axios');
        const topographUrl = 'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3601?type=basemap&id=3601&grid_id=1';
        

        const alpacUrl = 'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/69?type=aoi&id=69&gridId=1';
        const EIAUrl = 'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/7609?type=aoi&id=7609&gridId=1'
  
        
        axios.get(topographUrl, { headers }).then(function (styleResponse) {
            const style = styleResponse.data.data.style;
            map.setStyle(style);            
            
            getHumanFootprint(divId, mapVersion).then( (dataUrls) => dataUrls.forEach((url) => loadLayerFromUrl(divId, map,  headers, mapLayers, url))); 
 
            loadLayerFromUrl(divId, map, headers,  mapLayers, alpacUrl, true);
            loadLayerFromUrl(divId, map, headers,  mapLayers, EIAUrl, true, true);
            resolve({mapLayers, map});          
        }); 
    });

    
    // https://upsckv7isg.execute-api.us-west-2.amazonaws.com/development/search?match=human+footprint+2017&gridId=1 search for human footprint
    // get all categories
    //https://upsckv7isg.execute-api.us-west-2.amazonaws.com/development/layers/?types=dataset&parentId=3664&gridId=1
    // one categories 
    // https://upsckv7isg.execute-api.us-west-2.amazonaws.com/development/layers/?types=dataset&parentId=3668&gridId=1
}