/* db full name -> css style class name */
const hfCategory = [['Total Human Footprint', 'Total Human Footprint'],		
['Agriculture','agriculture'],
['Energy', 'energy'],
['Forestry','forestry'],
['Human-created Water Bodies','human_created_waterbodies'],
['Transportation','transportation'],
['Urban/Industrial','urban']];

const colorMap = {
'Agriculture': ['#E1D15D' , '#5E1E5D'], 
'Energy':  ['#836F9A','#6F839A'],
'Forestry': ['#77AA67', '#AA7767'], 
'Human-created Water Bodies': ['#08DBDD', '#DB08DD'], 
'Urban/Industrial': ['#4E4E4E', '#E4E4E4'], 
'Transportation': ['#EB7A7A', '#EB7A7A'], 
'Effective Forestry': ['#77AA67', '#77AA67'],
'Effective Human Footprint': ['#666', '#666']
};

const extenedHfCategory = [...hfCategory];
extenedHfCategory.push(['Effective Human Footprint', 'Effective Human Footprint'],);
extenedHfCategory.push(['Effective Forestry','Effective Forestry']);
const totalHFCategory = 'Total Human Footprint';


const linearCategory = [
	['Total Linear Features','total', 2],
	['Seismic Line (Conventional)', 'seismic_line_convention', 2],
	['Seismic Line (Low Impact)', 'seismic_line', 2],
	['Roads Minor','road_minor', 2],
	['Roads Major','road', 2],
	['Pipeline','pipeline', 2],
	['Transmission Line','transmission_line', 1],
	['Railways', 'rail', 1]
];

const credit  =  {
	enabled: true,
	href: '',
	style: { 'cursor': 'pointer', 'color': '#999999', 'fontSize': '10px' },
	text: ''
	};
	
/* use custom icons 
	* 1. insert icons into label
	* 2. hide the default icon
	* */
function useHFCustomIcon(category, size, inName, hideLabel){
		const iconSize = size || 25; 
		
		const name = inName || this.name;
		let id ='';
		for (let i=0; i < category.length; i++){
			if (category[i][0] == name){
				id = category[i][1];
			}
		}
		if (!id){
			return name;
			
		}
		//const img = '<img src = "/docroot/reports/oilsand/img/SVGicons/icon-' + id + '.png" class="icon" width = "'+ iconSize +'px" height = "'+ iconSize +'px">';
		return  ' <tag class="chart-legend-icon ' + id.replace(/[\s\/]/g, '_').toLowerCase() + ' '  + hideLabel + '"> ' + (hideLabel?'&nbsp;':name) + '</tag>';
		
}

function reloadOnWindowResize() {
	$(window).resize(function() {
		$.each(Highcharts.charts, function(indx, chart){
			const height = $(chart.renderTo ).height();
			const width = $(chart.renderTo ).width();
			chart.setSize(width, height, doAnimation = true);	
		});
		
	});	
}
function createStackBar(divId, regionId, startYear, endYear, version, xLabel, xBreak){
	let hfCategoryList = [];
	let regionList = [];

	
	const url = API_ROOT + '/report/getHFStackBarData';
	let params = {};
	params.endYear= endYear;
	if (regionId) {
		params.regionId= regionId;
	}
	params.version = version;
	$.get(url, params, function(data){
		$.each(data, function(key, row){
			
			regionList.push(key)
			$.each(row, function(subKey, subRow){
				if (subKey.indexOf('Effective') == -1 ) { // don't include Effective data.
					if (hfCategoryList.indexOf(subKey) == -1){
						hfCategoryList.push(subKey);
					}
				}
				
			});
		});
		
		displayHFSummaryTable(divId, data, regionList, hfCategoryList, xLabel, xBreak  );			
		reloadOnWindowResize();
		// $(window).resize(function() {
		// 	$.each(Highcharts.charts, function(indx, chart){
		// 		const height = $(chart.renderTo ).height();
		// 		const width = $(chart.renderTo ).width();
		// 		chart.setSize(width, height, doAnimation = true);	
		// 	});
			
		// });
		
	});
}
/* table of hf */
function displayHFSummaryTable(divId, data, regionList, hfCategoryList , xLabel, xBreak ){
	let stackBarData = [];
	
	$.each(regionList, function(index, value) {
		$('#' + divId + ' .hfSumamryTableHeader').append('<th class="percentHead">' + value + '</th>');
	});
	let regionTotal = {};
	$.each(hfCategoryList, function(index, category) {
		let seriesData = {};
		seriesData.name =  category;
		seriesData.total = 0; 
		let seriesList = [];
		let htmStr = '<tr  rel="total-human-footprint" class="hfContent shown">';
		htmStr += '<th class="nameBod"><tag class="chart-legend-icon ' + category.replace(/[\s\/]/g, '_').toLowerCase() + '"></th><th class="nameBod">' + category + '</th>'; // add icon and category name
		$.each(regionList, function(index, region) {
			htmStr += '<td class="percentBod">' + data[region][category] + '%</td>';
			if (category != 'Total Human Footprint'){				
				seriesList.push(parseFloat(data[region][category]));
			} else {
				seriesList.push({y : 0,
					manualTotal: (data[region][category])
				});
				regionTotal[region] = data[region][category];
			}
			
			if (category == 'Total Human Footprint'){ // create a fake data set, just to display the data label
				seriesData.total = data[region][category];
				seriesData.showInLegend = false;	
				seriesData.dataLabels = {
						// overflow: true,
						enabled: true,
						crop: false,
						formatter: function () {
							return this.point.options.manualTotal + '% | ' + this.point.category; // don't use auto calculated total, as we round up values at each category, and total sum may be off a little bit.
						},
						horizontalAlign: 'right'					
					};
			}
			else{
				seriesData.dataLabels = {			             
						enabled: false,
						crop: false,
						formatter: function () {
							return this.point.y + '%'
						}			                
					};
			}				
		});
		
		htmStr += '</tr>';
		$('#' + divId + ' .hfSumamryTableBody').append(htmStr);			
	
		seriesData.borderWidth = 1;
		seriesData.data = seriesList;	
		
		stackBarData.legendImage = 'https://www.highcharts.com/samples/graphics/sun.png';
		stackBarData.push(seriesData);
		
	});
	
	/* display chart */
	const bHideLegendSymbol = true;
	renderStackBarChart(divId, '', regionList, stackBarData, xLabel, xBreak, regionTotal, bHideLegendSymbol);
}
	
function renderStackBarChart(divId, stackBarTitle, categoryList, stackBarData, xLabel, xBreak, regionTotal, bHideLegendSymbol){

	
	$('#' + divId + ' .stackBarChart').highcharts( {
			chart: {
				backgroundColor:'rgba(255, 255, 255, 0.0)',
				//marginRight: 300,
				type: 'bar',
				height: 300,
				events: {
					render: function () {
						const chart = this;						
						$.each(chart.series[0].data, function (i, d) {
							// console.log('max is ', chart, d, chart.xAxis[0].toPixels(0), chart.xAxis[0].toPixels(1), chart.xAxis[0].toPixels(-1));
							if (d.dataLabel) {
								d.dataLabel.attr({
									x: chart.yAxis[0].toPixels(parseFloat(d.manualTotal)),
									/**
									 * using previous version, y is automatically calculated. 
									 * but not in new version. when not specified, -9999 will be applied.
									 * so I manually calcuated y pixel position
									 * plot height - current plot x - 0.25 * bar width (not sure why it is 0.25, I thought 0.5 make more sense)	
									 */
									y:  chart.plotHeight - d.plotX - 0.25* d.pointWidth, 
								});
							}
						});					
					
						// legend hover over
						legend = chart.legend;

						for (let i = 0, len = legend.allItems.length; i < len; i++) {
							(function(i, chart) {
								const item = legend.allItems[i].legendItem;
								item.on('mouseover', function (e) {
									//show custom tooltip here
									// 0: is the fake dataset, so add 1 to the index
									// chart.series[i+1].update({
									// 	dataLabels: {
									// 		enabled: true
									// 	}
									// }, false);
									for( let indx =0; indx < chart.series[i+1].data.length; indx++) {
										const data =  chart.series[i+1].data[indx];
										data.setState('hover');	
										chart.tooltip.refresh(data);
									}
									
									
								}).on('mouseout', function (e) {
									for( let indx =0; indx < chart.series[i+1].data.length; indx++) {
										const data =  chart.series[i+1].data[indx];
										data.setState('');
										
									}
								});
							})(i, chart);
						}
						
						// modify the legend symbol from a rect to a line
						if (bHideLegendSymbol) {
							$('#' + divId + ' .highcharts-legend-item rect').attr('width', '0');
						}
					}
				}
			}, // end of chart options
			credits: credit,
			colors: ['#CCCCCC','#E1D15D' , '#836F9A','#77AA67', '#08DBDD', '#4E4E4E', '#EB7A7A'],
			title: {
				text: stackBarTitle
			},
			
			xAxis: {
				categories: categoryList,
				visible: false,
				lineWidth: 0,
				tickWidth: 0,
				opposite:true,
				
			},
			yAxis: {
				min: 0,
				max: 100,
				title: {
					text: xLabel,
					style: {
						color: '#666',
						'font-weight': '900',						
						'font-size': '1.1em',
					}
				},
				gridLineWidth: 0,
				lineWidth: 1,
				reversedStacks:false,
				visible: true,
				breaks: [{
					from: xBreak,
					to: 100,
					breakSize: 2
				}],
				events: {
					pointBreak: pointBreakColumn
				},
				
			},
			tooltip: {
				useHTML: true,
				formatter: function () {
					return '<div class="tooltip">' +
						(this.y > 0?'<b>' + this.x + '</b><br/>' + this.series.name + ': ' + this.y + '%<br/>':'') +
						'Total: ' + regionTotal[this.x] +'%</div>'; //replace system stackTotal using custom data
				}
			},
			
			legend: {
					useHTML: true,
					reversed: false,
					labelFormatter: function(){ return useHFCustomIcon(extenedHfCategory, 15, this.name);} 
				},			    	
			
			plotOptions: {
				series: {
					stacking: 'normal',
					pointWidth: 50,
					borderWidth:4			        
				}
			},
			series: stackBarData,
		}
		);


} // end of creating stackBar charts
	
/**--------------------------------------------------
 * create trending bars
 ------------------------------------------*/
	/**
 * Extend the Axis.getLinePath method in order to visualize breaks with two parallel
 * slanted lines. For each break, the slanted lines are inserted into the line path.
 */
	Highcharts.wrap(Highcharts.Axis.prototype, 'getLinePath', function (proceed, lineWidth) {
		let axis = this,
			path = proceed.call(this, lineWidth),
			x = path[1],
			y = path[2];

		Highcharts.each(this.breakArray || [], function (brk) {
			if (axis.horiz) {
				x = axis.toPixels(brk.from);
				path.splice(3, 0,
					'L', x - 4, y, // stop
					'M', x - 9, y + 5, 'L', x + 1, y - 5, // left slanted line
					'M', x - 1, y + 5, 'L', x + 9, y - 5, // higher slanted line
					'M', x + 4, y
				);
			} else {
				y = axis.toPixels(brk.from);
				path.splice(3, 0,
					'L', x, y - 4, // stop
					'M', x + 5, y - 9, 'L', x - 5, y + 1, // lower slanted line
					'M', x + 5, y - 1, 'L', x - 5, y + 9, // higher slanted line
					'M', x, y + 4
				);
			}
		});
		return path;
	});

	/**
	 * On top of each column, draw a zigzag line where the axis break is.
	 */
	function pointBreakColumn(e) {
		let point = e.point,
			brk = e.brk,
			shapeArgs = point.shapeArgs,
			x = shapeArgs.x,
			y = this.translate(brk.from, 0, 1, 0, 1),
			w = shapeArgs.width,
			key = ['brk', brk.from, brk.to],
			path = ['M', x, y, 'L', x + w * 0.25, y + 4, 'L', x + w * 0.75, y - 4, 'L', x + w, y];

		if (!point[key]) {
			point[key] = this.chart.renderer.path(path)
				.attr({
					'stroke-width': 2,
					stroke: point.series.options.borderColor
				})
				.add(point.graphic.parentGroup);
		} else {
			point[key].attr({
				d: path
			});
		}
	}
	function createTrendingCharts(divId, regionId, startYear, endYear, version){
			const url =  API_ROOT + '/report/getAllHFTrendDataDetails';
			let params = {};
			params.startYear= startYear;
			params.endYear= endYear;
			params.regionId= regionId;
			params.version = version;
		
			$.get(url, params, function(data){ // get hf trending for all regions
			
				$.each(data, function(index, values){ // for each region
					let trendData = [];
					
					let columns = {}; // 
					
					columns.name = values.name;
					columns.type = 'column';
					columns.connectNulls = true;
					const yearTrend = values.trend;
					columns.data = [];
					columns.color='#cccccc';
					columns.showInLegend = false;
					
					let errorBarList = {}; // error bar for single regions, different hf
					
					let hfLineList = {};	// for single region different  hf						
				
					
					let yearCategory = [];
					let trendStartYear = endYear;
					$.each(yearTrend, function(key, val){
						if (trendStartYear > key){
							trendStartYear = key;
						}
					});
				
					let	maxHF = 0;
				
							
					for (let i= trendStartYear; i <= endYear; i++) { // go through all year data, collect bar height, and error 						
						const currentRow = yearTrend[i+'']; // get one year data. 
						if (currentRow){ // if there are values, find the total human footprint							
						
							$.each(currentRow, function(key, hfRow) { // go through each human footprint category
								
								if (key != totalHFCategory) {// add to spline, and error bar
									let errorBars = errorBarList[key];
									if (errorBars == null) {
										errorBars = {};
										errorBars.name = key;
										errorBars.type = 'errorbar';	
										errorBars.connectNulls = true;
										errorBars.color = colorMap[key][0];
										errorBars.data = [];	
										errorBars.whiskerLength = 5;
										errorBars.whiskerWidth = 1;						
									}
									let errorRange = [hfRow.lcl, hfRow.hcl];
									errorBars.data.push(errorRange);
									delete errorBarList[key];
									errorBarList[key] = errorBars;
									if (hfRow.hcl > maxHF){
										maxHF = hfRow.hcl;
									}
									let hfLine = hfLineList[key];
									if (hfLine == null) {
										hfLine = {};
										hfLine.name = key;
										hfLine.type = 'spline';	
										hfLine.connectNulls = true;
										hfLine.color = colorMap[key][0];
										hfLine.data = [];
										if (key.indexOf('Effective') != -1) {
											hfLine.dashStyle = 'Dot';
										}										
									}
									hfLine.data.push(hfRow.value);
									delete hfLineList[key];
									hfLineList[key] = hfLine;
								} else { // for total add the columns
									const currentValue = hfRow.value;
									if (currentValue > maxHF){
										maxHF = currentValue;
									}
									columns.data.push(currentValue);
								}
							});
						} else{
							columns.data.push(0);
							// add year when there are no values
							$.each(extenedHfCategory, function (index, row) {
								
								const key = row[0];
								if (key != totalHFCategory) {
									let hfLine = hfLineList[key];
									if (hfLine == null) {
										hfLine = {};
										hfLine.name = key ;
										hfLine.type = 'spline';	
										hfLine.connectNulls = true;
										hfLine.data = [];
										
										if (key.indexOf('Effective') != -1) {
											hfLine.dashStyle = 'Dot';
										}											
									}
									hfLine.data.push(null);
									delete hfLineList[key];
									hfLineList[key] = hfLine;
									
									let errorBars = errorBarList[key];
									if (errorBars == null) {
										errorBars = {};
										errorBars.name = key;
										errorBars.type = 'errorbar';	
										errorBars.connectNulls = true;
										errorBars.data = [];
										errorBars.whiskerLength = 2;
										errorBars.whiskerWidth = 1;
									}
									const errorRange = [null, null];
									errorBars.data.push(errorRange);
									delete errorBarList[key];
									errorBarList[key] = errorBars;
								}
							});
						}						
						yearCategory.push(i);
					}
					
					trendData.push(columns); // add bars for all years in range					
					$.each(errorBarList, function(key, errorBars) {trendData.push(errorBars);});
					$.each(hfLineList, function(key, hfLine) {trendData.push(hfLine);});
					//trendData.push(hfLine);
					const compareList = values.compare;
					const hasTable = (compareList.detail.length> 0 && compareList.detail[0]  && compareList.detail[0].pctDifference);
					
					renderTrendingBarChart(divId, values.name,  startYear, endYear, 'Human Footprint (%)', trendData,yearCategory, values.id, hasTable, Math.ceil(maxHF*1.1) , index); //(Math.ceil(maxHF/10))*10 );
					if (hasTable) {
						displayHFTrendTable(divId, values.name, startYear, endYear,  compareList, values.id, index);
					}
					
				}); // end of each data
				
				// const slider = $('#' + divId).lightSlider({
			    //     item: 1,
			    //     loop: true,
			    //     adaptiveHeight:true,
			    //     pager: false
			    // }).goToSlide(2);
				
				$('#' + divId).bxSlider({
					infiniteLoop: false,
					slidesToShow: 1,
					slidesToScroll: 1,
					hideControlOnEnd: true
				});
			

				// $.each($('#' + divId + ' .graph-table-tabs'), function(indx,  obj){
				// 	$(obj).find('.tabLink').click(function(event){	
				// 		console.log('click ', $(this));
				// 		event.preventDefault();   
				// 		if (!$(this).hasClass('active')) {
				// 			$(obj).find('.tabContent').toggle();
				// 			$(obj).find('.tabLink').removeClass('active');
				// 			// $($(this).attr('href')).show();
				// 			// $("#tab3t045c898e-50d1-43f1-99c7-2e69f15c24d83").show();
				// 			// console.log($($(this).attr('href')), ($($(this).attr('href')).is(":visible")));
				// 			$(this).addClass('active');
				// 		}
				// 	});
				// });	
				
				$('#' + divId + ' .tabList').simpleTabs();
			});
		}
		
	function displayHFTrendTable(divId, regionName, startYear, endYear,  compareData, subRegionId){
		// don't display table when the data is missing, or incomplete
		if (compareData.detail.length == 0){
			return;
		}
 	
		if (!compareData.detail[0] || !compareData.detail[0].pctDifference) {
			return;
		}
		
		const regionDivId = divId + subRegionId;
		let htmStr = '';
		htmStr = '<div class="column"><table class="table is-striped"><tr><th class="nameHead">Human Footprint Type</th><th class="percentHead">' + startYear + ' Area (%)</th><th class="percentHead">' + endYear + ' Area (%)</th><th class="percentHead">Change (%)</th></tr>';
		
			
		htmStr += '<tr>';
		$.each(compareData.detail, function(indx, row) {
					htmStr += '<th class="nameBod">' + row.category + '</th>';					
				
						htmStr += '<td class="percentBod">' + row.orgPercentage + '</td>';
						htmStr += '<td class="percentBod">' + row.percentage + '</td>';
						htmStr += '<td class="percentBod">' + (row.pctDifference.indexOf('-')!=-1 ? '': '+') + row.pctDifference + '</td>';
						htmStr += '</tr>';
			});
			
		htmStr += '</table></div>';
		
		$('#' + divId + ' #tab3' + regionDivId).append(htmStr);
		
	}
	
	function renderTrendingBarChart(divId, regionName, startYear, endYear, title, chartData, yearCategory, subRegionId, hasTable, breakStart, index){
		const regionDivId = divId + subRegionId;
		let html = '';
		if (hasTable){
			html = '<div class="graph-table-tabs ' + (index ==0?'active':'') + '"><h4 class="centered" style="margin: 10px 0 40px 0;">' + regionName + '</h4>' + 
				'<div class="columns is-centered content tabList chart">' +
				'  <div class="tab column is-one-third"><a href="#tab1' + regionDivId + '" class="tabLink btn is-default-tab is-active">View as Graph</a></div>'+
				/*'  <div class="tab col s4"><a href="#tab2' + regionDivId + '" class="tabLink btn">Table (km<sup>2</sup>)</a></div>'+*/
				'  <div class="tab column is-one-third"><a href="#tab3' + regionDivId + '" class="tabLink btn">View as Table</a></div>'+
				'</div>'+
				''+
				'<div id="tab1' + regionDivId + '" class="tabContent"  style="width:100%;height:600px"><div id="trendBarChart'+ regionDivId + '" style="width:100%; height:100%" ></div></div>'+
				/*'<div id="tab2' + regionDivId + '" class="col s12 carouselColumn tabContent" style="display:none; height:600px"></div>'+*/
				'<div id="tab3' + regionDivId + '" class="columns tabContent" style="display:none; height:600px"></div>'+
				'';
		}
		else{
			html =  '<div><h5 class="center-align center-align marginB30">' + regionName + '</h5><div class="columns content tabs">' +
			'  <div class="tab column is-one-third"><a href="#tab1' + regionDivId + '" class="tabLink">Graph</a></div>'+
			'</div>'+
			'<div class="column" > '+
			' <div id="tab1' + regionDivId + '" class="column" style=" height:600px"> <div id="trendBarChart' + regionDivId + '" class="column carouselColumn" style=" height:100"></div></div></div>'
			+ '</div>';
		}
					
		$('#' + divId).append(html);  
			
			
		const width =$('#' + divId).parent().width();
	
				
		Highcharts.chart('trendBarChart' + regionDivId, {
			colors: ['#E1D15D' , '#836F9A','#77AA67', '#08DBDD', '#4E4E4E', '#EB7A7A', '#f00', '#0f0'],
			credits: credit,
			chart: {
				 backgroundColor:'rgba(255, 255, 255, 0.0)',
				 width:width - 40,
				 marginRight: 50,
				 events: {
				  load: function(event) {
						// hide default icons
						$('#trendBarChart' + regionDivId + ' .highcharts-legend-item path').hide();
					}
				 }
			},
			title : {
				text : '' 
			},
			tooltip : {
				headerFormat : '<b>{point.x}</b><br/>',
				pointFormat : '{series.name}: {point.y}%'
			},
			plotOptions: {
				spline: {
					lineWidth: 1,
					states: {
						hover: {
							lineWidth: 3
						}
					},
					marker: {
						enabled: false
					}
				}
			},
			legend: {
				  useHTML: true,
				  reversed: false,
				  labelFormatter: function(){ return useHFCustomIcon(extenedHfCategory, 15, this.name);}
				},
			xAxis : {
				categories : yearCategory,
				lineColor:'#000000'
			},
			yAxis : {			
				lineColor:'#000000',
				lineWidth: 1,
				title : {
					 text: 'Human Footprint (%)',
					 style: {
						color: '#666666',
						'font-weight': '900',
						'font-family': '"PT Serif",serif',
						'font-size': '1.3em',
					}
				},
				max:100,
				tickInterval:(breakStart > 10? 5: breakStart>5 ? 2:1),

				breaks: [{
					from: ((breakStart<=5 ||breakStart >=10)?breakStart: 10.5),
					to: 100,
					breakSize: breakStart*0.05
				}],
				events: {
					pointBreak: pointBreakColumn
				},
			},
			labels : {
				items : [ {
					html : '',
					style : {
						left : '50px',
						top : '18px'
					
					}
				} ]
			},
			series :chartData

		}); // end of trend bar

		
		$('#trendBarChartTab' + regionDivId).append('<div class="figure-title"><u>Trend in the percentage area of total human footprint (grey bars) and by human footprint category (coloured lines) in the ' +
				regionName + ' between ' + startYear + ' and ' + endYear+'</u>. Click on the entries in the legend to turn human footprint categories on and off.</div>');

	} // end of creating trending charts

	/* linear density data */
	function createLinearStackBar(divId, regionId, endYear, version){
		let linearCategoryList = [];
		let regionList = [];	
		
		const url = API_ROOT + '/report/getLinearDensityBarData';
		let params = {};
		params.endYear= endYear;
		if (regionId) {
			params.regionId= regionId;
		}
		if (version) {
			params.version = version;
		}
		$.get(url, params, function(data){
			$.each(data, function(key, row){				
				regionList.push(key);				
				$.each(row, function(subKey, subRow){					
					linearCategoryList.push(subKey);
				});				
			});
			
			displayLinearDensitySummaryTable(divId, data, regionList, linearCategoryList );			
			reloadOnWindowResize();
			// $(window).resize(function() {
			// 	$.each(Highcharts.charts, function(indx, chart){
			// 		const height = $(chart.renderTo ).height();
			// 	    const width = $(chart.renderTo ).width();
			// 	    chart.setSize(width, height, doAnimation = true);	
			// 	});
			    
			// });
			
		});
	}

	function displayLinearDensitySummaryTable(divId, data, regionList, linearCategoryList){
		let stackBarData = [];
		
		$.each(regionList, function(index, value) {
			$('#' + divId + ' .hfSumamryTableHeader').append('<th class="percentHead percentBod">' + value + ' </th>');
		});
		
		$.each(linearCategoryList, function(index, category) {			
			let seriesData = {};
			seriesData.name =  category;
			seriesData.total = 0; 
			let seriesList = [];
			let htmStr = '<tr  rel="total-human-footprint" class="hfContent shown">';
			htmStr += '<th class="nameBod">' + category + '</th>';
			$.each(regionList, function(index, region) {
				htmStr += '<td class="percentBod">' + data[region][category] + ' km/km<sup>2</sup></td>';
				if (category!= 'Total'){
				
					seriesList.push(parseFloat(data[region][category]));
				}
				else{
					seriesList.push({y : 0,
						manualTotal: (data[region][category])
					});
				}
					
				if (category == 'Total'){ // create a fake data set, just to display the data label
					seriesData.total = 0; //data[region][category];
					seriesData.showInLegend = false;	
					seriesData.dataLabels = {
							overflow: false,
							enabled: true,
							crop: false,
							formatter: function () {
								return this.point.options.manualTotal + ' km/km<sup>2</sup> | '  + this.point.category; // don't use auto calculated total, as we round up values at each category, and total sum may be off a little bit.
							},
							horizontalAlign: 'right'
						};
				}
				else{
					seriesData.dataLabels = {
							
						enabled: false,
						crop: false,
						formatter: function () {
							return this.point.y.toFixed(2) + ' km/km<sup>2</sup>'
						}							
					};
				}
				
			});
		
			htmStr += '</tr>';
			$('#' + divId + ' #hfSumamryTableBody').append(htmStr);

			seriesData.borderWidth = 1;
			seriesData.data = seriesList;
		
			stackBarData.legendImage = 'https://www.highcharts.com/samples/graphics/sun.png';
			stackBarData.push(seriesData);
		
		});
		
		/* display chart */
		renderLinearDensityStackBarChart(divId, '', regionList, stackBarData);
	}

	function renderLinearDensityStackBarChart(divId, stackBarTitle, categoryList, stackBarData, bHideLegendSymbol){
		$('#' + divId + ' .stackBarChart').highcharts( {
			chart: {
				backgroundColor:'rgba(255, 255, 255, 0)',
				marginRight: 300,
				spacingTop: 0,
				marginTop: 10,
				height: 250,
				type: 'bar',			
				events: {
					render: function () {
						const chart = this;
						let max = [];
						if (chart.series && chart.series.length > 0) {
							$.each(chart.series[0].data, function (i, d) {
								max[i] = 0;
							});
						
							$.each(chart.series, function(inds,s){					        	
								
								$.each(s.data, function(ind,d){
									if (s.visible){
										max[ind] += s.data[ind].y;
									}
								});	
							});
								
							$.each(chart.series, function (idx, series) {
								if (series.name == 'Total') {
									$.each(series.data, function (i, d) {
										if (d.dataLabel) {
											d.dataLabel.attr({
												x: chart.yAxis[0].toPixels(max[i])				
											});
										}
									});								
								}
							});
						}
				  
					
						// legend hover over
						legend = chart.legend;

						for (let i = 0, len = legend.allItems.length; i < len; i++) {
							(function(i, chart) {
								const item = legend.allItems[i].legendItem;
								item.on('mouseover', function (e) {
									//show custom tooltip here
									// 0: is the fake dataset, so add 1 to the index
//			                        	  chart.series[i+1].update({
//			                        	        dataLabels: {
//			                        	            enabled: true
//			                        	        }
//			                        	    }, false);
									for( let indx =0; indx < chart.series[i+1].data.length; indx++) {
										const data =  chart.series[i+1].data[indx];
										data.setState('hover');	
										chart.tooltip.refresh(data);
									}
									
									
								}).on('mouseout', function (e) {
									for( let indx =0; indx < chart.series[i+1].data.length; indx++) {
										const data =  chart.series[i+1].data[indx];
										data.setState('');										
									}
								});
							})(i, chart);
						}
						
						// modify the legend symbol from a rect to a line
					
						$('#' + divId + ' .highcharts-legend-item rect').attr('width', '0');
					
					}
				}
			},
			colors: ['#0c6960', '#196b1c', '#e68514', '#795548','#4e4e4e','#e60000','#A900e6'],
			title: {
				text: stackBarTitle				
			},
		   
			xAxis: {
				categories: categoryList,
				visible: false,
				lineWidth: 0,
				tickWidth: 0,
				opposite:true,
				labels: {
					style: {
						color: '#666',
						'font-weight': '900',						
						'font-size': '1.1em',
					}
				}
			},
			yAxis: {
				min: 0,
				max: 3.5,
				title: {
					text: 'Density (km/km<sup>2</sup>)',
					useHTML: true,
					style: {
						color: '#666',
						'font-weight': '900',						
						'font-size': '1.1em',
					}
				},
				gridLineWidth: 0,
				lineWidth: 1,
				reversedStacks:false,
				visible: true
			   
			},
			tooltip: {
				backgroundColor: "rgba(255,255,255,1)",
				useHTML: true,
				formatter: function () {
					return '<div class="tooltip"><b>' + this.x + '</b><br/>' +
						this.series.name + ': ' + this.y.toFixed(2) + ' km/km<sup>2</sup><br/>' +
						'Total: ' + this.point.stackTotal.toFixed(2) +' km/km<sup>2</sup></div>'; // replacing system stackTotal using custom data
				}
			},
			
			legend: {
				useHTML: true,
				reversed: false,
				labelFormatter: function(){ return useHFCustomIcon(linearCategory, 15, this.name);} 
			},			    	
			
			plotOptions: {
				series: {
					stacking: 'normal',
					pointWidth: 50,
					borderWidth:4,
					dataLabels: {
						style: {
							fontWeight: 'bold',
							color: '#666'
						},
						useHTML: true
					}			        
				}
			},
			series: stackBarData,
		}
		);


} // end of creating stackBar charts
/* create native habitat grouped bars */
	function createNativeHabitatGroupedBars(divId, year,  yAxisLabel) {
		const params = {year: year};
		$.get(API_ROOT + '/report/getNativeHabitatGroupedBarData', params, function(data){
			Highcharts.chart(divId, {
				chart: {
					type: 'column'
				},
				title: {
					text: ''
				},
				xAxis: {
					categories: data.category,
					labels: {
						style: {
						color: '#666',
						'font-weight': '700',						
						'font-size': '1em',
					}
					}
				},
				yAxis: {
					min: 0,
					max: 100,
					title: {
						text: yAxisLabel,
						style: {
							color: '#666',
							'font-weight': '700',						
							'font-size': '1.4em',
						}
					},
					stackLabels: {
						enabled: true,
						style: {
							fontWeight: 'bold',
							color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
						}
					}
				},
				plotOptions: {
					column: {
						dataLabels: {
							enabled: true,
							crop: false,
							overflow: 'none',
							formatter: function () {
								return this.point.y + '%'
							}	
						}
					}
				},				
				colors: data.colors,		
				tooltip: {
					backgroundColor: '#FCFFC5',
					headerFormat: '<b>{point.x}</b><br/>',
					formatter: function () {				
						return '<div class="tooltip">' +
							'<b>' + this.series.name  + '</b><br/>Percentage: ' + this.point.y + '%<br/>Area: ' + Math.abs(this.point.area).toFixed(1) + 'Km<sup>2</sup><br/>' +
							'</div>'; //replace system stackTotal using custom data
					}
					// pointFormat: function () {
					// 	return this.point.y + '%'
					// }	//'{series.name}: {point.y}%<br/>Total: {point.area} Km<sup>2</sup>'
				},
				series: data.series
			});
			reloadOnWindowResize();
		});
}
/* simple bar */
	function createSectorEffectBarChart(url, divId, title, yAxisLabel, yRange) {
		$.get(url, function(data){
			/* first package data, as the API was originall created for plotly charts */
			let series = [];
		
			let seriesData = {  showInLegend:false, data: [],  pointWidth: 40};
			let shortCategory = [];
			$.each(data.value, function(index, row){
				if (data.category[index] != 'Agriculture') {
					let rowData = {};
					rowData.y = row;
					rowData.name = data.category[index];
					rowData.color = data.color[index];		
					// rowData.pointPadding = 0.2;
					// rowData.groupPadding = 0.05	;
					seriesData.data.push(rowData);

					const dataRow = {name: row, y: 0}; //,  visible: false}
					series.push(dataRow)	;
					shortCategory.push(data.category[index].replace('Transportation','Transport')
					.replace('Urban/Industrial','Urban'));
				}
			});

			series.push(seriesData)	;		
			
			let tmpHfCategory = [...hfCategory];
			tmpHfCategory.push(['Transport', 'transport'],);
			tmpHfCategory.push(['Urban','urban']);

			Highcharts.chart(divId, {
				credits: {
					text: ''			 
				},
				chart: {
					type: 'column',
					//spaceLeft: 10,
					//spaceRight: 10,
					events: {
						render: function () { // hide default legend
							$('#' + divId + ' .highcharts-legend-item rect').attr('width', '0');						
						}
					
					}			
				},
				title: {
					text: ''
				},
				xAxis: {
					visible: true,			
					categories: shortCategory,
					lineWidth: 2,
					tickLength: 0,
					labels: {					
						useHTML: true,
						formatter: function(){ return useHFCustomIcon(tmpHfCategory, 15, this.value);} 
					}
				},
				yAxis: {
					min: yRange[0]?yRange[0]:0,
					max: yRange[1]?yRange[1]:100,
					title: {
						text: yAxisLabel,
						style: {
							color: '#666',
							'font-family': '"PT Serif",serif',
							'font-weight': '800',						
							'font-size': '1.3em',
						}
					},
					visible: true,
					lineWidth: 2,
					gridLineWidth: 1,
					endOnTick: false,
					startOnTick: false,
					// offset: 5
				},
				plotOptions: {
					column: {
						grouping: false,
						dataLabels: {
							enabled: true,
							crop: false,
							overflow: 'none',
							// useHTML: true,
							formatter: function () {
								return (this.point.y > 0 ? '+':'') + this.point.y + '%';// + useHFCustomIcon(extenedHfCategory, 15, this.point.name)
							}	
						},
						//stacking: 'normal'					
					},				
					series: {
						groupPadding: 0.2,
						events: {
							legendItemClick: function (x) {
								const i = this.index  - 1;
								let series = this.chart.series[0];
								let point = series.points[i];   
		
								if(point.oldY == undefined)
								point.oldY = point.y;
		
								point.update({y: point.y != null ? null : point.oldY});
							}
						}
					}
				},				
				legend: {
					enabled: false,			
					useHTML: true,
					reversed: false,
					align: 'center',
					//width: '60%',
					borderWidth: 1,
					labelFormatter: function(){ return useHFCustomIcon(extenedHfCategory, 15, this.name);} 
				},		
				tooltip: {
					backgroundColor: '#FCFFC5',
					headerFormat: '',
					// pointFormatter: function(){ return useHFCustomIcon(extenedHfCategory, 15, this.name);} 
					pointFormat: '{point.name}: {point.y}%'
				},
				series: series
			});
		});
	}
/* dynamically update sector effect bar when spp name changed*/
	function loadSectorEffectBarChart(url, sppGroup, displayFieldName, divId, year, title, yLabel, yRange){
		/* first load species list */		
		$.get(API_ROOT + '/report/getSpeciesListForSectorEffect', {version: 'Sector Effect', year, sppGroup}, function(data){
			if (data.length > 0) {
				let nameField = 'scientificName';
			
				if (sppGroup == 'Birds' || sppGroup.includes('Mammal')) {
					nameField = 'commonName';
				}			
				displayFieldName = (displayFieldName? displayFieldName: nameField);
				data.sort((a, b) =>  a[displayFieldName] > b[displayFieldName] ? 1: -1);					
			
				data.forEach( spp => {
					const localNameField = displayFieldName ? displayFieldName: nameField; // when spp group mixed (spp.sppGroup.sgId == 1 || spp.sppGroup.sgId == 7) ? 'commonName' : 'scientificName';
					const circleImg = spp['scientificName'] ? '<img class="middle circle-img" src="http://abmi.ca/FileDownloadServlet?dir=WEB_GRAPH&amp;filename=/profiles/' + 
						spp['scientificName'].replace(/\s/g, '-') + '-small.jpg"' + 
						'onerror="this.src=\'/.resources/Alpac-report/webresources/img/species-list-standard.jpg\'"></img>': '';
					$('#sector-effect-spp').append('<li ><div>' + circleImg + '<a class="middle sppName ' + (localNameField == 'scientificName' ? 'italic': '') + '" data-value="' +  spp[nameField] + '">' + spp[localNameField] +'</a></div></li>');
				});

				$('#sector-effect-spp li').on('click', function() {				
					const speciesDiv =  $(this).html();
					const species =  $(this).find('a').data("value");
					_switchSectorBars (url, speciesDiv, species, divId, title, yLabel, yRange)
				});

				// load first graphs
				// const spp = data[0]
				// const localNameField = nameField?nameField: (spp.sppGroup.sgId == 1 || spp.sppGroup.sgId == 7) ? 'commonName' : 'scientificName';
				// const species = spp[localNameField];
				// _switchSectorBars (url, species, divId, title, yLabel, yRange)
				$('#sector-effect-spp li').first().click();
				$('.centerSelect .dropdown').click();
				reloadOnWindowResize();
			} else {
				$('#under' + divId).empty().html('No Species');
				$('#se' + divId).empty().html('No Species');;
			}
			

		});
	}
	function _switchSectorBars (url, speciesDiv, species, divId, title, yLabel, yRange) {
		$('#selected-spp').html(speciesDiv);
		/* clear out both sector effect and under sector effect div, and reload data to create bars */
		$('#under' + divId).empty();
		createSectorEffectBarChart(url + species + '&version=Sector Effect Under', 'under' + divId, title, yLabel, yRange);
		$('#se'+divId).empty();
		createSectorEffectBarChart(url + species + '&version=Sector Effect', 'se' + divId, title, yLabel, yRange);
	}
	/* species intactness bars  for multiple species multiple years
	*/
	async function createSpeciesIntactnessFigure(divId, sppGroup, displayFieldName, version, breakRange, tickInterval) {
		
		const url = API_ROOT + '/report/getSpeciesIntactnessFigure';
		/* data {category, data: {[name, intactness, deviation]}} */		
		const data = await $.get(url, {sppGroup, version, displayFieldName});
		const sppName = data.category;
		const intactnessData = data.data.map ( x => {return {type: 'bar', name: x.name, data: x.intactness}	});
		
		createSppIntactnessChart (divId, sppName, displayFieldName, intactnessData, breakRange, tickInterval);
		reloadOnWindowResize();
	}
	function _formatSppName (sppName, nameType) {
		switch (nameType) {
			case 'common':
				return sppName.commonName;			
			case 'both':
				return sppName.commonName ? sppName.commonName +  ' (<i>' + sppName.scientificName + '</i>)' : '<i>' + sppName.scientificName + '</i>';
			default:
				return '<i>' + sppName.scientificName + '</i>';
		}
	}
	function createSppIntactnessChart (divId, sppName, displayFieldName, intactnessData, breakRange, tickInterval) {
		const width =$('#' + divId).parent().width();
		Highcharts.chart(divId, {
			colors: [ '#A9B937', '#A9B937A0', '#f00', '#0f0'],
			credits: credit,
			chart: {
				 backgroundColor:'rgba(255, 255, 255, 0.0)',
				 width:width - 40,
				// marginRight: 50,	
				 height: sppName.length* 40,	
				 spacingTop: 0		 
			},
			title : {
				text : '' 
			},
			tooltip: {
				useHTML: true,
				formatter: function () {				
					return '<div class="tooltip">' +
						'<b>' + _formatSppName(this.x, displayFieldName) + '</b><br/>' +   this.series.name + ': ' + Math.abs(this.point.options.sppItem.intactness).toFixed(1) + '%<br/>' +
						'</div>'; //replace system stackTotal using custom data
				}
			},
			
			plotOptions: {
				bar: {
					dataLabels: {						
						enabled:true,
						crop: false,
						overflow:"none",
					//	align: 'left',
						formatter: function () {
							return  this.point.options.sppItem && Math.abs(this.point.options.sppItem.intactness).toFixed(1) + '%'; // don't use auto calculated total, as we round up values at each category, and total sum may be off a little bit.
						}
					}					
				},
				series: {
					groupPadding: 0 /* make bar wide, default is 0.2, so label can be displayed */
				}
			},
			legend: {
				// layout: 'vertical',
				// align: 'right',
				verticalAlign: 'top',
				itemMarginTop: 0,
				itemMarginBottom:10
			  },
			annotations: [{
				labelOptions: {
					backgroundColor: 'rgba(255,255,255,0.5)',
					verticalAlign: 'top',
					y: 15
				},
				labels: [{
					point: {
						xAxis: 0,
						yAxis: 0,
						x: 100,
						y: 10
					},
					text: 'Arbois'
				}, {
					point: {
						xAxis: 0,
						yAxis: 0,
						x: 45.5,
						y: -10
					},
					text: 'Montrond'
				}]
			}],
			yAxis: [{
				min: -100,
				max: 100,
				lineColor:'#000000',
				lineWidth: 1,
				tickInterval: tickInterval,
				breaks: breakRange ? breakRange: [],
				events: {
					pointBreak: pointBreakColumn
				},
				title: {
				 text: 'Species Intactness'
				},
				labels:{
					formatter:function(){						
						if (Math.abs(this.value) <= 100)
							return 100 - Math.abs(this.value);
					}
				},
				plotLines: [{
					color: '#333333',
					width: 2,
					value: 0					
				}]
				},				
				{
					title: {text: '  ', margin: 0},
					linkedTo: 0,
					opposite: true,
					tickInterval: 20,	
					gridLineWidth: 0,				
					labels:{
						autoRotation: [0],
						overflow: 'allow',
						formatter:function(){			
							if (this.value == -40)			
								return '<span> Decrease</span>';
							if (this.value == 0)
								return '|';
							if (this.value == 20)			
								return 'Increase ';

						}
					},
				}],
			xAxis : {
				categories : sppName,
				lineColor:'#000000',
				labels:{
					formatter: function() {						
						return _formatSppName(this.value, displayFieldName);
					}
				},				
			},
			series :intactnessData

		}); // end of trend bar
	}


	/* error bar + scatter for habitat element */
	async function createHabitataElement(divId, version, typeId , title, yAxis) {
		const url = API_ROOT + '/report/getHabitatElement';
		/* data {category, data: {[name, intactness, deviation]}} */		
		const response = await $.get(url, {version, type: typeId});
		const data = response.data;
		const type = response.type
		const categories = type.map( x => x.hecLabel);
		const categoryIndex = type.map( x => x.hecId)

		const offset = 0.1;
		let errorBar = {}, scatter = {};
		
		let years = Array.from(new Set(data.map(x => x.hevYear)));
		years.forEach ( (y, index) => {
			errorBar[y] = [];
			scatter[y] = [];
			const shift = ((index%2 ==0) ? -1 : 1) * offset;
			// using category index as guide find values.
			categoryIndex.forEach(
				categoryId => {
					console.log(categoryIndex , 'categoryIndex');
					let found = data.find(v => v.hevYear == y && v.categoryId == categoryId ) || {hevLower: 0, hevUpper: 0};					
					errorBar[y].push([categoryIndex.indexOf(categoryId) + shift, found.hevLower, found.hevUpper]);
					scatter[y].push({x: categoryIndex.indexOf(categoryId) + shift, 'y': found.hevMean, low: found.hevLower, up: found.hevUpper});
				});
		});
		
		
		let series = [];
		years.forEach ( (y, index) => {
			series.push({
				name: 'Year ' + y,
				type: "errorbar",
				whiskerWidth: 0,
				data: errorBar[y]			
				
			});

			series.push({
				name: 'Year ' + y,
				type: "scatter",
				marker: {
					radius: 8
				},
				data: scatter[y]
			});
		})
		const units = {1: '#/ha', 2: '#/ha', 3: 'm<sup>3</sup>/ha'};
		createHabitatElementChart (divId, series, categories, title, yAxis, units[typeId]);
		reloadOnWindowResize();
	}
	function createHabitatElementChart (divId, series, categories, title, yAxis, unit) {
		const width =$('#' + divId).parent().width();
		
		Highcharts.chart(divId, {
			colors: [ '#b6d245', '#7a8627',  '#f00', '#0f0'],
			credits: credit,
			chart: {
				 backgroundColor:'rgba(255, 255, 255, 0.0)',
				 width:width - 40,
				// marginRight: 50,	
			
				 spacingTop: 0		 
			},
			title : {
				text : title				
			},
			tooltip: {
				useHTML: true,
				formatter: function () {				
					return '<div class="tooltip">' +
						'<b>' + categories[Math.round(this.x)] + '</b><br/>' +   this.series.name + ': <br>Mean: ' +  this.y.toFixed(1) + ' ' + unit + '<br/>' +
						// ' low: ' + this.point.options.low + ' <br>upper:' + 
						//  this.point.options.up +'<br/>' +
						'</div>'; //replace system stackTotal using custom data
				}
			},
			
			plotOptions: {
				bar: {
					dataLabels: {						
						enabled:true,
						crop: false,
						overflow:"none",
					//	align: 'left',
						
					}					
				},
			},
			legend: {
				// layout: 'vertical',
				// align: 'right',
				verticalAlign: 'top',
				itemMarginTop: 0,
				itemMarginBottom:10
			  },
			annotations: [{
				labelOptions: {
					backgroundColor: 'rgba(255,255,255,0.5)',
					verticalAlign: 'top',
					y: 15
				},
				labels: [{
					point: {
						xAxis: 0,
						yAxis: 0,
						x: 100,
						y: 10
					},
					text: 'Arbois'
				}, {
					point: {
						xAxis: 0,
						yAxis: 0,
						x: 45.5,
						y: -10
					},
					text: 'Montrond'
				}]
			}],
			yAxis: {
				// min: -100,
				// max: 100,
				title: {
					text: yAxis,
					useHTML: true,
					style: {
						color: '#666',
						'font-weight': '900',						
						'font-size': '1.1em',
					}
				},
				lineColor:'#000000',
				lineWidth: 1,
				// tickInterval: tickInterval,
				// breaks: breakRange ? breakRange: [],
				// events: {
				// 	pointBreak: pointBreakColumn
				// },
			
				},				
				
			xAxis : {
				//categories,
				lineColor:'#000000',
				
				labels:{
					// rotation: 8,
					// autoRotation: false,
					useHTML: true,
					overflow: 'allow',
					padding: 0,
					y: 0.5,
					// maxStaggerLines: 1,
					formatter: function() {						
						if (this.value > categories.length || this.value < 0)
							return '';
						let display = categories[this.value.toFixed()];
						display = display.replace(/\s/g, '<br>')
						display = display.substring(0, display.lastIndexOf(' ')) + '<br>' + display.substring(display.lastIndexOf(' ')+1);
						return  display + '</span>'; //(this.value.toFixed()%2 == 1 ? '': '</br><span style="">') +
					},
					style: {
						color: '#666',
						'font-weight': '700',						
						// 'font-size': '1em',
					}
				},				
			},
			series 

		}); // end of trend bar
	}