
function initMap (divId) {	          
    const bounds = [
        [-130, 40], // Southwest coordinates
        [-90, 70]  // Northeast coordinates
       ];

    let headers = {};
    headers['x-api-key'] = 'NSgyFqN2n31UQPd3uLA8o2w0A7TJ0Gk29JPVK041';

    mapboxgl.accessToken = 'pk.eyJ1IjoicWZhbmciLCJhIjoiY2p5NjJ3OHNqMGUydDNibXpvZzdodzNsaCJ9.HAK1XLZht400oO5Q_19LsQ';
    let map = new mapboxgl.Map({
        container: divId,
        // style: 'mapbox://styles/mapbox/light-v10',
        zoom: 5,
        center: [-118.447303, 53.753574],
        maxBounds: bounds
    });
    map.addControl(new mapboxgl.FullscreenControl());
    map.addControl(new mapboxgl.NavigationControl());
    
    map.on('data', function (data) {
        
        if (data.dataType === 'source' && data.isSourceLoaded) {        
          $('#' + divId + ' .lds-spinner').hide();
          // stop listening to map.on('data'), if applicable
          const maskLayerId = Object.keys(mapLayers).find( x => mapLayers[x].type === 'aoi');
          if (maskLayerId) { 
              drawMask(map, mapLayers[maskLayerId].layer);
              delete mapLayers[maskLayerId];
          }
        }
      })
    let mapLayers = {};
    // const axios = require('axios');
    const topographUrl = 'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3601?type=basemap&id=3601&grid_id=1';
    const dataUrls = {
        boundary: {layers: [69, 7609], alwaysShow: true, default: true, mask: false}, // alpac + EIA
        ecosystem: {layers: [24, 36, 38, 39, 40, 42, 43, 25, 44, 26, 45 ], mask: 7609 },
        wetland: {layers: [3662, 3661, 3659, 3660, 3663, 3658], mask: 7609},
        file_history: {layers: [7772], mask: 7609},
        parks: { layers: [203, 202], mask: 7609}
    };
    'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3667?type=dataset&id=3625&grid_id=1',
    'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3625?type=dataset&id=3667&grid_id=1',
    'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3692?type=dataset&id=3625&grid_id=1',
    'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3680?type=dataset&id=3625&grid_id=1',
    'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3694?type=dataset&id=3625&grid_id=1',
    'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/3690?type=dataset&id=3625&grid_id=1']

    const alpacUrl = 'https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/69?type=aoi&id=69&gridId=1';

    //https://upsckv7isg.execute-api.us-west-2.amazonaws.com/dev2/layers/2253?type=dataset&id=2253&grid_id=1'
    // Make a request for a user with a given ID

    
    axios.get(topographUrl, { headers }).then(function (styleResponse) {
        const style = styleResponse.data.data.style;
        map.setStyle(style);
        const aoi = loadLayerFromUrl(divId, map, headers,  mapLayers, alpacUrl, true, true);
        const defaultObj = dataUrls.find(x => x.default);
        createLegendRow(defaultObj.layers);
        //const data2 = loadLayerFromUrl(map, headers, mapLayers, gridUrl);
        //const data3 = loadLayerFromUrl(map,headers,  mapLayers, landcoverLayerUrl);
        // loadLayerFromUrl(map, headers, mapLayers, baseTypeUrl, true);
        
    });
    
    /* add legend expand compress action */
    $('#' + divId + '_container #hf-legend .icon').click(() => {
        const legendObj = $('#' + divId + '_container #hf-legend');
        legendObj.toggleClass( 'compress', !legendObj.hasClass('compress') );
        legendObj.toggleClass( 'expand', !legendObj.hasClass('expand') );
    });
    // https://upsckv7isg.execute-api.us-west-2.amazonaws.com/development/search?match=human+footprint+2017&gridId=1 search for human footprint
    // get all categories
    //https://upsckv7isg.execute-api.us-west-2.amazonaws.com/development/layers/?types=dataset&parentId=3664&gridId=1
    // one categories 
    // https://upsckv7isg.execute-api.us-west-2.amazonaws.com/development/layers/?types=dataset&parentId=3668&gridId=1
}